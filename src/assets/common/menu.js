﻿/**
 * Created by again on 2016/6/21.
 */

$(function(){
    //初始化菜单html
    initMenu();

    requestDataTotal();
    //页面名称
    autoResizeMenuLeft();
    //initHeader();
});



//绑定菜单点击事件
$(document).on("click",".left-menu dt",function(){
    $(".left-menu dt").next().slideUp();
    //展开或收缩子菜单 避免反复点击bug
    $(this).next().filter(':not(:animated)').slideToggle("slow");
    //切换主菜单小图标
    var className=$(this).find("span").attr("class");
    $(".left-menu dt").find("span").attr("class","icon-circle-left");
    if(className=="icon-circle-left") className="icon-circle-down";
    else className="icon-circle-left"
    $(this).find("span").attr("class",className);
});


//获取url中的menuId后初始化选中菜单
function initSelectMenu()
{
    var query=new QueryString();
    //获取url菜单参数
    var menuId=query["menuId"];
    if(menuId==null||menuId==undefined) menuId="0.0";
    $("#left-menu .dd-list a").each(function(){
        //获取左侧菜单参数
        var href= $(this).attr("href");
        var curMenuId=href.split("=")[1];
        if(curMenuId==menuId){
            $(this).parents("dd").addClass("selected").parent().show();
            $(this).parents("dl").find("dt span").attr("class","icon-circle-down");
            if(curMenuId.length==5){
                $(this).parents("li").addClass("selected").parent().show();
                $(this).parents("dd").find("span").attr("class","icon-circle-down");
            }
        }
    });

}

//获取url参数值
function QueryString()
{
    var name,value,i;
    var str=location.href;
    var num=str.indexOf("?")
    str=str.substr(num+1);
    var arrtmp=str.split("&");
    for(i=0;i < arrtmp.length;i++)
    {
        num=arrtmp[i].indexOf("=");
        if(num>0)
        {
            name=arrtmp[i].substring(0,num);
            value=arrtmp[i].substr(num+1);
            this[name]=value;
        }
    }
}
//
////获取未处理安防条数、故障条数、审批条数
function requestDataTotal(){
    //请求安防条数
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c027", "", securityNumResultArrived, '');
    $.getJSON('/cwp/src/json/warning/warningEvent_c027.json',function(data){
        securityNumResultArrived(data);
    })
    //请求故障条数
    // PostForm("/cwp/front/sh/workflow!execute", "uid=getMytodoCount", "", approvalNumResultArrived, '');
    $.getJSON('/cwp/src/json/warning/warningEvent_getMytodoCount.json',function(data){
        approvalNumResultArrived(data);
    })
    //请求安防条数

}

//回调函数返回数据
function securityNumResultArrived(result){
    if(result.returnCode=="0"){
        if(parseInt(result.bean.SECURITY)>99){
            $("#securityNum").text("99+");
            $("#securityNum").attr("title",result.bean.SECURITY);
        }
        else{
            $("#securityNum").text(result.bean.SECURITY);
        }

        if(parseInt(result.bean.WARN)>99){
            $("#faultNum").text("99+");
            $("#faultNum").attr("title",result.bean.WARN);
        }
        else{
            $("#faultNum").text(result.bean.WARN);
        }
    }
    else{
        $("#securityNum").text(0);
        $("#faultNum").text(0);
    }
}

function approvalNumResultArrived(result){
    if(result.returnCode=="0") {
        var count=result.object.count!=undefined?result.object.count:0;
        if ( parseInt(count)> 99) {
            $("#approvalNum").text("99+");
            $("#approvalNum").attr("title", count);
        }else {
            $("#approvalNum").text(count);
        }
    }
    else{
        $("#approvalNum").text(0);
    }
}


//获取网页尺寸修改事件
window.onresize=function()
{
    autoResizeMenuLeft();
}

function autoResizeMenuLeft()
{
    var wHeight = $(window).height();
    $(".menu-list").css({"height":wHeight-200});
}



function initMenu(){
    var menuHtml=localStorage.getItem("menuHtml");
    setTimeout(function(){

        $(".menu-list").html(menuHtml);
        initSelectMenu();
    }, 0);
    //初始化菜单选择

}

