﻿$(document).ready(function () {
    $("#header").load("/cwp/src/module/common/header.html",initHeader);
    $("#left-menu").load("/cwp/src/module/common/leftMenu.html",initMenu);
    $("#copyright").load("/cwp/src/module/common/copyright.html");
    document.title="首航节能-智慧园区";
    requestDataTotal();

    //主页内容滚动条
    $('.right-scroll').slimScroll({
        width: 'auto', //可滚动区域宽度
        height: '100%', //可滚动区域高度
        size: '5px', //组件宽度
    });
});

//退出登录
$(document).on("click","#header_logout", function () {
    localStorage.clear();
    // window.location.href = "/cwp/front/sh/login!logout?uid=L001";
    window.location.href = "/cwp/login.html";
});


//设置
$(document).on("click","#header_setting", function () {
    if (window.location.href.indexOf("personSetting.html")>-1){
        window.location.reload();
    }else{
        top.location.href ="/cwp/src/static/accountService/personSetting/personSetting.html?menuId=4.0"
    }
});


//绑定菜单点击事件
//$(document).on("click",".left-menu dt",function(){
//    $(".left-menu dt").next().slideUp();
//    //展开或收缩子菜单 避免反复点击bug
//    $(this).next().filter(':not(:animated)').slideToggle("slow");
//    //切换主菜单小图标
//    var className=$(this).find("span").attr("class");
//    $(".left-menu dt").find("span").attr("class","icon-circle-left");
//    if(className=="icon-circle-left") className="icon-circle-down";
//    else className="icon-circle-left"
//    $(this).find("span").attr("class",className);
//});


$(document).on("click",".left-menu dt",function(){
    $(".left-menu dt").next().slideUp();
    //展开或收缩子菜单 避免反复点击bug
    $(this).next().filter(':not(:animated)').slideToggle("slow");
    //切换主菜单小图标
    var className=$(this).find("span").attr("class");
    $(".left-menu dt").find("span").attr("class","icon-circle-left");
    if(className=="icon-circle-left") className="icon-circle-down";
    else className="icon-circle-left";
    $(this).find("span").attr("class",className);
});

//三级菜单点击事件
$(document).on("click",".dd-list dd a",function(){
    var t_span=$(this).find("span");
    if(t_span.length>0){
        var className=$(this).find("span").attr("class");
        if(className=="icon-circle-left") className="icon-circle-down";
        else className="icon-circle-left"
        t_span.attr("class",className);
        $(this).next().filter(':not(:animated)').slideToggle("slow");
    }
});

//获取url中的menuId后初始化选中菜单
function initSelectMenu()
{
    var query=new QueryString();
    //获取url菜单参数
    var menuId=query["menuId"];
    if(menuId==null||menuId==undefined) menuId="0.0";

    $("#left-menu .dd-list a").each(function(){
        //获取左侧菜单参数
        var href= $(this).attr("href");
        var curMenuId=href.split("=")[1];
        if(curMenuId==menuId){
            $(this).parents("dd").addClass("selected").parent().parent().show();
            $(this).parents("dl").find("dt span").attr("class","icon-circle-down");
            $(this).parents("dl").addClass("navColor");
            if(curMenuId.length==5){
                $(this).parents("li").addClass("selected").parent().show();
                $(this).parents("dd").find("span").attr("class","icon-circle-down");
                $(this).parents("dl").addClass("cur");
            }
        }
        if(menuId == '0.0'){
            $('.indexLeftListBtn').parents("dl").addClass("navColor");
        }
    });

}


//获取url参数值
function QueryString()
{
    var name,value,i;
    var str=location.href;
    var num=str.indexOf("?")
    str=str.substr(num+1);
    var arrtmp=str.split("&");
    for(i=0;i < arrtmp.length;i++)
    {
        num=arrtmp[i].indexOf("=");
        if(num>0)
        {
            name=arrtmp[i].substring(0,num);
            value=arrtmp[i].substr(num+1);
            this[name]=value;
        }
    }
}
//
////获取未处理安防条数、故障条数、工单审批
function requestDataTotal(){
    //判断是否具有安防告警及故障告警权限
    //请求安防条数
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c027", "", securityNumResultArrived, '');
    $.getJSON('/cwp/src/json/warning/warningEvent_c027.json',function(data){
        securityNumResultArrived(data);
    })
    //请求故障条数
    // PostForm("/cwp/front/sh/workflow!execute", "uid=getMytodoCount", "", approvalNumResultArrived, '');
    $.getJSON('/cwp/src/json/warning/warningEvent_getMytodoCount.json',function(data){
        approvalNumResultArrived(data);
    })
    //请求安防条数

}

//回调函数返回数据
function securityNumResultArrived(result){
    var srcurityNum="";
    var faultNum="";
    if(result.returnCode=="0"){
        if(parseInt(result.bean.SECURITY)>99){
            srcurityNum="99+";
        }
        else{
            srcurityNum=result.bean.SECURITY;
        }

        if(parseInt(result.bean.WARN)>99){
            faultNum="99+";
        }
        else{
            faultNum=result.bean.WARN;
        }
    }
    else{
        srcurityNum="0"
        faultNum="0";
    }
    $(".dd-list:eq(0) dd a").each(function(){
        var href=$(this).attr("href");
        if(href.indexOf("eventManagement.html")>-1){
            $(this).append("<i class=t-badge-orange>"+srcurityNum+"</i>");
        }
        else if(href.indexOf("faultAlarm.html")>-1){
            $(this).append("<i class=t-badge-orange>"+faultNum+"</i>");
        }

    });


}

function approvalNumResultArrived(result){
    var approvalNum="";
    if(result.returnCode=="0") {
        var count=result.object.count!=undefined?result.object.count:0;
        if ( parseInt(count)> 99) {
            approvalNum="99+";
        }else {
            approvalNum=count;
        }
    }
    else{
        approvalNum="0";
    }

    $(".dd-list:eq(0) dd a").each(function(){
        var href=$(this).attr("href");
        if(href.indexOf("maintenanceOrder.html")>-1){
            $(this).append("<i class=t-badge-orange>"+approvalNum+"</i>");
        }
    });
}



//window.onresize=function()
//{
//    autoResizeMenuLeft();
//}
//获取网页尺寸修改事件
//$(window).resize(function(){
//    autoResizeMenuLeft();
//});

//根据屏幕分辨率绑定或解除导航点击事件
function autoResizeMenuLeft()
{
    var wWidth=$(window).width();
    if(wWidth >= 930){
        //绑定菜单点击事件
        $(document).on("click",".left-menu dt",function(){
            $(".left-menu dt").next().filter(':not(:animated)').slideUp();
            //展开或收缩子菜单 避免反复点击bug
            $(this).next().filter(':not(:animated)').slideToggle("slow");
            //切换主菜单小图标
            var className=$(this).find("span").attr("class");
            $(".left-menu dt").find("span").attr("class","icon-circle-left");
            if(className=="icon-circle-left") className="icon-circle-down";
            else className="icon-circle-left";
            $(this).find("span").attr("class",className);
        });
    }else{
        $(document).off("click",".left-menu dt");
    }

}



function initMenu(){
    var menuHtml=localStorage.getItem("menuHtml");
    $(".menu-list").html(menuHtml);
    //隐藏无链接菜单
    hideMenuByNoLinks();
    initSelectMenu();
    //菜单栏滚动条
    $('.menu-list').slimScroll({
        width: 'auto', //可滚动区域宽度
        height: '100%', //可滚动区域高度
        size: '5px' //组件宽度
    });

}


//隐藏菜单
function hideMenuByNoLinks(){

    $(".dd-list dd  a").each(function(){
        var href = $(this).attr("href");
        var next= $(this).next();
        if(href == "javascript:void(0);" && next.length==0  ){
            $(this).parent().hide();
        }
    });
}

//加载头部内容
function initHeader(){
    var userName = localStorage.getItem("username");
    var tureName = localStorage.getItem("trueName");
    var userRole = localStorage.getItem("roles");
    var userRoleArray= JSON.parse(userRole);
    // var userRoleName=userRoleArray[0].roleName;
    // $("#header_username").html(userRoleName+"："+tureName);//设置header 用户名
}