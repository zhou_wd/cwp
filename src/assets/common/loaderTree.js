var treeNodeId;
var addDiyDomType;//一级菜单还是二级菜单
var url;
var uid;

//清空树状图
function delTree(){
    $("#dataTree li").remove();
    $("#dataTreeTotal").html("(共0条记录)");
    $("#dataTree").append('<li id="dataTreeData" style="text-align: center;line-height: 30px;">暂无数据记录</li>')
}
//自定义DOM节点
function addDiyDom(treeId, treeNode) {
    var spaceWidth = 15;
    var liObj = $("#" + treeNode.tId);
    var aObj = $("#" + treeNode.tId + "_a");
    var switchObj = $("#" + treeNode.tId + "_switch");
    var icoObj = $("#" + treeNode.tId + "_ico");
    var spanObj = $("#" + treeNode.tId + "_span");
    aObj.attr('title','');
    aObj.append('<div class="diy swich"></div>');
    var div = $(liObj).find('div').eq(0);
    switchObj.remove();
    spanObj.remove();
    icoObj.remove();
    div.append(switchObj);
    div.append(spanObj);
    $("#" + treeNode.tId + "_switch").attr("data-value",treeNode.dictValue);
    //判断是否添加二级菜单
    if(addDiyDomType == 1){
        $("#" + treeNode.tId + "_span").text(treeNode.dictKey);
        var spaceStr = "<span style='height:1px;display: inline-block;width:" + (spaceWidth * treeNode.level) + "px'></span>";
        switchObj.before(spaceStr);
        var editStr = '';
        editStr += '<div class="diy">' + (treeNode.dictValue )+ '</div>';
        editStr += '<div class="diy">' + (treeNode.dictName )+ '</div>';
        editStr += '<div class="diy"><a class="t-btn t-btn-sm t-btn-red t-btn-deal dictdel tree-btnred" data-dictvalue="'+ treeNode.dictValue +'">删除</a><a class="t-btn t-btn-sm t-btn-blue t-btn-deal addDictData tree-btnblue" data-dictvalue="'+ treeNode.dictValue +'">添加字典</a></div>';
        aObj.append(editStr);
        //当单击下拉按钮请求子级列表
        $("#" + treeNode.tId + "_switch").click(function(){
            treeNodeId =  treeNode.tId;
            if(document.getElementById(treeNode.tId + "_ul")!=undefined){
                $("#detailDictValue").val("");
                    $("#" + treeNodeId + "_ul").remove();
                $("#" + treeNodeId + "_switch").css("background-position","85px 0px");
            }else{
                $("#detailDictValue").val(this.getAttribute( 'data-value'));
                PostForm(url,uid,"",dictListTwo,"","dictDataForm");
                $("#" + treeNodeId + "_switch").css("background-position","67px 0px");
                aObj.append("<ul class='level10' id="+ treeNodeId + "_ul></ul>");
            }
        })
    }else{
        $("#" + treeNode.tId + "_span").text(treeNode.dictdataKey);
        $("#" + treeNode.tId + "_switch").css("display","none");
        $("#" + treeNode.tId + "_span").css("margin-left","30px");
        var spaceStr = "<span style='height:1px;display: inline-block;width:" + (spaceWidth * treeNode.level) + "px'></span>";
        switchObj.before(spaceStr);
        var editStr = '';
        editStr += '<div class="diy">' + (treeNode.dictdataValue )+ '</div>';
        editStr += '<div class="diy">' + (treeNode.dictdataName )+ '</div>';
        editStr += '<div class="diy"><a class="t-btn t-btn-sm t-btn-red t-btn-deal dictDatadel tree-btnred" data-dictDataId="'+ treeNode.id +'">删除</a><a class="t-btn t-btn-sm t-btn-blue t-btn-deal editDictData tree-btnblue" data-dictDataId="'+ treeNode.id +'">编辑</a></div>';
        aObj.append(editStr);
    }
}
//封装渲染
//treeId树状结构的ID
//zTreeNodes需要渲染的数据
//addDiyDomType需要渲染的级别  1级菜单可以不用填入config configUid
function outpintSetting(treeId,zTreeNodes,DiyDomType,config,configUid){
    addDiyDomType = DiyDomType;
    if(addDiyDomType == 1){
        urlold = url;
        uidold = uid;
        url = config;
        uid = configUid;
    }
    var setting={
        view: {
            showLine: false,
            showIcon: false,
            addDiyDom:addDiyDom
        },
        data: {
            simpleData: {
                enable: true
            }
        }
    };
    $.fn.zTree.init(treeId, setting, zTreeNodes);
}

//获取二级菜单
function dictListTwo(result){
    if(result.returnCode ==0){
        if(result.beans.length>0){
            outpintSetting($("#" + treeNodeId + "_ul"), result.beans,0);
        }
    }
    else{
        tipModal("icon-cross", result.returnMessage);
        $("#" + treeNodeId + "_switch").css("background-position","85px 0px");
    }
}