﻿var serverError = "服务器通信失败，请重试。";

var ipg_url="";
//var httpUrl="http://10.88.77.247:8080";
//var videoUrl="http://10.88.77.247:";
//var ipg_url="http://120.194.44.254:20888";

//rtmp视频内网播放地址
var rtmpVideoIp ="192.168.121.126:1935";
//rtmp视频外网播放地址
var public_rtmpVideoIp ="120.194.44.254:1935";


function htmlEncode(value) {
    return $('<div/>').text(value).html();
}
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}
function replaceInvalidChars(source) {
//    while (source.indexOf('+') >= 0) {
//        source = source.replace('+', "%2B");
//    }
    return source;
}


function PostForm(url, parameters, action, successFunction, failedFunction,formId) {
    //添加域名及端口号
    //var host = "http://120.194.44.254:20888/";
    var host=window.location.protocol+"//"+window.location.host;
    //获取IPG地址
    ipg_url=host;
    url=host+url;
    //获取用户ID
    var userId=localStorage.getItem("parkId");
    if(userId==null||userId==undefined)
    {
        userId="";
    }
    parameters+="&userId="+userId;
    var queryString = window.location.search;
    if (queryString != "" && queryString != null) {
        url += queryString + "&" + parameters;
    }
    else {
        url += "?" + parameters;
    }
    var submitForm="";
    if(formId==""||formId==undefined)
    {
        submitForm=$(document.forms[0]).serialize();
    }
    else{
        submitForm=$("#"+formId).serialize();
    }
    var method = action!="post"?"get":"post";
    var request= $.ajax({
        timeout : 10000,
        type:method,
        url:url+"&rid=" + Math.random(),
        beforeSend:function(requests){
            requests.setRequestHeader("platform", "pc");
        },
        async:true,
        dataType: "json",
        data:submitForm,
        success:function (result) {
            if (successFunction != null && successFunction != "") {
                successFunction(result);
            }
        },
        error:function (result) {
            //超时提示
            if(result.statusText=='timeout'){
                alert("服务器通信超时，请稍后再试");
            }
            if (failedFunction != null && failedFunction != "") {
                failedFunction(result);
            }


        }
    });
}



function communicateFailed(result) {
    alert("服务器通信失败，请稍后再试");
}