﻿//$(function(){
//    var tipMolal
//    $("body").append(
//    <div class='modal fade bs-example-modal-sm' id='myTip" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
//        <div class="modal-dialog modal-sm" role="document">
//        <div class="modal-content">
//        <div class="modal-body" style="text-align:center;font-size:14px;">
//        <i id="icon-tip" style="margin-right:6px;"></i>
//        <span class="tip-info"></span>
//        </div>
//        </div>
//        </div>
//        </div>
//    });
//});

//提示图标，提示内容，提示后跳转链接
function tipModal(icon, content, linkUrl,timeout) {
    var tipIconColor = "";
    switch (icon) {
        case "icon-checkmark":
            tipIconColor = "t-tip-todo";
            break;
        case "icon-cross":
            tipIconColor = "t-tip-error";
            break;
        case "icon-info2":
            tipIconColor = "t-tip-warn";
            break;
    };
    //提示图标及颜色
    $("#myTip #icon-tip").attr("class",icon).addClass(tipIconColor);
    //提示内容
    $("#myTip .tip-info").html(content);
    $("#myTip").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    if (linkUrl !== "" && linkUrl !== undefined) {
        //定时跳转链接
        setTimeout(function () { window.location.href = linkUrl; }, timeout||2000);
        return;
    }
    else {
        //定时关闭调试窗
        setTimeout(function () {
            $("#myTip").modal("hide");
        }, timeout||2000);
    }
}

//////绑定点击事件
//$("#btnSaveInfo").on("click", function () {
//    ////成功
//    tipModal("icon-checkmark", "保存成功","linkUrl");
//    ////失败
//    //tipModal("icon-cross", "保存失败");
//    //提示
//    tipModal("icon-info2", "请输入用户名");

//});
