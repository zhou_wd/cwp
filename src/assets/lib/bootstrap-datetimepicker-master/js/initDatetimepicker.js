﻿//初始化日历控件
$(function(){
    $(".form_datetime").datetimepicker({
        language: 'zh-CN',//日历语言
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,//自动关闭
        todayHighlight: 1,//今天高亮
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        clearBtn: true
    });

    //选择日期
    $('.form_date').datetimepicker({
        language: 'zh-CN',
        format: "yyyy-mm-dd",
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        clearBtn: true
    });

    //选择时间
    $('.form_time').datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        clearBtn: true
    });

    //开始时间-结束时间
    $(".form_date_start").datetimepicker({
        language: 'zh-CN',
        format: "yyyy-mm-dd",
        autoclose: true,
        minView: "month",
        maxView: "decade",
        todayBtn: true,
        clearBtn: true
    }).on("click", function (ev) {
        $(".form_date_start").datetimepicker("setEndDate", $(".form_date_end").val());
    });

    $(".form_date_end").datetimepicker({
        language: 'zh-CN',
        format: "yyyy-mm-dd",
        autoclose: true,
        minView: "month",
        maxView: "decade",
        todayBtn: true,
        clearBtn: true
    }).on("click", function (ev) {
        $(".form_date_end").datetimepicker("setStartDate", $(".form_date_start").val());
    });
});
