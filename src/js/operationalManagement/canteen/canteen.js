$(function() {
    // getCanteenList(1);
});
//数据查询
$("#btnSearchFaultCanteen").on("click",function(){
    isFirst=true;
    // getCanteenList(1);
});

//获取列表数据
function getCanteenList(pageIndex){
    var queryDate = $("#startApplyTime").val();
    if(queryDate == ""){
        queryDate =getNowFormatDate();
        $("#startApplyTime").val(queryDate);
    }
    var loaderCanteenDataTab = new LoaderTableNew("consumptionList","/cwp/front/sh/pay!execute","uid=P025",pageIndex,10,outputCanteenList,"",CanteenDataPagination,"frmConsumption");
    loaderCanteenDataTab.Display();
}
//渲染列表数据
function outputCanteenList(result){
    if(result.returnCode ==0){
        //早餐交易量
        template.helper('breakfastCountFormat', function(inp) {
            if(inp == "null" || inp == "") {
                return  "一";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return  inp[1];
                }else{
                    return  inp[0];
                }
            }
        });
        //早餐交易额
        template.helper('breakfastMoneyAbs', function(inp) {
            if(inp == "null" || inp == "") {
                return  "0.00";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return   Math.abs(inp[1]).toFixed(2);
                }else{
                    return   Math.abs(inp[0]).toFixed(2);
                }
            }
        });
        //午餐交易量
        template.helper('lunchCountFormat', function(inp) {
            if(inp == "null" || inp == "") {
                return  "一";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return  inp[1];
                }else{
                    return  inp[0];
                }
            }
        });
        //午餐交易额
        template.helper('lunchMoneyAbs', function(inp) {
            if(inp == "null" || inp == "") {
                return  "0.00";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return   Math.abs(inp[1]).toFixed(2);
                }else{
                    return   Math.abs(inp[0]).toFixed(2);
                }
            }
        });
        //晚餐交易量
        template.helper('dinnerCountFormat', function(inp) {
            if(inp == "null" || inp == "") {
                return  "一";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return  inp[1];
                }else{
                    return  inp[0];
                }
            }
        });
        //晚餐交易额
        template.helper('dinnerMoneyAbs', function(inp) {
            if(inp == "null" || inp == "") {
                return  "0.00";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return   Math.abs(inp[1]).toFixed(2);
                }else{
                    return   Math.abs(inp[0]).toFixed(2);
                }
            }
        });
        //夜餐交易量
        template.helper('supperCountFormat', function(inp) {
            if(inp == "null" || inp == "") {
                return  "一";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return  inp[1];
                }else{
                    return  inp[0];
                }
            }
        });
        //夜餐交易额
        template.helper('supperMoneyAbs', function(inp) {
            if(inp == "null" || inp == "") {
                return  "0.00";
            }else{
                var inp =inp.split("-");
                if(inp.length > 1){
                    return   Math.abs(inp[1]).toFixed(2);
                }else{
                    return   Math.abs(inp[0]).toFixed(2);
                }
            }
        });
        //合计交易量
        template.helper('totalCountFormat', function(inp) {
            if(inp == "null" || inp == "") {
                return  "0";
            }
            else{
                return  inp;
            }
        });
        //合计金额
        template.helper('totalMoneyAbs', function(inp) {
            if(inp == "null" || inp == "") {
                return  "0.00";
            }
            else{
                if(inp.length > 1){
                    return   Math.abs(inp[1]).toFixed(2);
                }else{
                    return   Math.abs(inp[0]).toFixed(2);
                }
            }
        });
        var outHtml = template("dataTmpl", result);
        $("#consumptionList tbody").html(outHtml);
    }
    else{
        tipModal("icon-cross", "暂无数据");
    }
    $("#canteenListTotal").text("(共"+result.beans.length+"条记录)");
}
//列表数据分页回调
function CanteenDataPagination(page_index) {
    if (!isFirst) queryList(page_index + 1);
}
//获取当天日期
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate()-1;
    if(month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if(strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}