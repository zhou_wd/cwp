//切换分类
var echartColors = [];
$(function(){
    echartColors = ["#5b9bd5","#ed7d31","#ffc000"];
    $("#startApplyTime").val(getToday(-1));
    getBedUserInfo();
});

//查询

$("#btnSearchFaultCanteen").on("click",function(){
    getBedUserInfo();
});

//获取床位分配信息
function getBedUserInfo(){
    var time = $("#startApplyTime").val();
    PostForm("/cwp/front/sh/pay!execute","uid=P025&currentPage=1&pageSize=10&queryDate="+time,"",getinfo);
}
function getinfo(result){

    // result = {
    //     beans: [{
    //         breakfastCount: "0",
    //         breakfastMoney: "null",
    //         dinnerCount: "0",
    //         dinnerMoney: "null",
    //         lunchCount: "0",
    //         lunchMoney: "null",
    //         restaurantName: "班车",
    //         supperCount: "0",
    //         supperMoney: "null",
    //         totalCount: "0",
    //         totalMoney: "null"
    //     }, {
    //         breakfastCount: "0",
    //         breakfastMoney: "-44.0",
    //         dinnerCount: "0",
    //         dinnerMoney: "null",
    //         lunchCount: "0",
    //         lunchMoney: "-56.0",
    //         restaurantName: "员工之家",
    //         supperCount: "0",
    //         supperMoney: "null",
    //         totalCount: "0",
    //         totalMoney: "null"
    //     }, {
    //         breakfastCount: "0",
    //         breakfastMoney: "-1187.0",
    //         dinnerCount: "0",
    //         dinnerMoney: "-5478.0",
    //         lunchCount: "0",
    //         lunchMoney: "-10115.0",
    //         restaurantName: "中膳",
    //         supperCount: "0",
    //         supperMoney: "null",
    //         totalCount: "0",
    //         totalMoney: "null"
    //     }, {
    //         breakfastCount: "0",
    //         breakfastMoney: "-2447.1",
    //         dinnerCount: "0",
    //         dinnerMoney: "-1714.7",
    //         lunchCount: "0",
    //         lunchMoney: "-10061.3",
    //         restaurantName: "和兴隆",
    //         supperCount: "0",
    //         supperMoney: "-396.0",
    //         totalCount: "0",
    //         totalMoney: "null"
    //     }, {
    //         breakfastCount: "0",
    //         breakfastMoney: "null",
    //         dinnerCount: "0",
    //         dinnerMoney: "null",
    //         lunchCount: "0",
    //         lunchMoney: "null",
    //         restaurantName: "消费机",
    //         supperCount: "0",
    //         supperMoney: "null",
    //         totalCount: "0",
    //         totalMoney: "null"
    //     }],
    //     returnCode: "0",
    //     returnMessage: "获取餐厅消费情况完成"
    // };

    var allXName = [],

        morXData = [],

        noonXData = [],

        eveXName = [];

    function getMoney (num){
        var numData = Number(Math.abs(num).toFixed(2));
        if(isNaN(numData)){
            return 0;
        }else{
            return numData;
        }
    }

    if(result.returnCode == 0){
        for(i = 0;i < result.beans.length;i++){
            switch(result.beans[i].restaurantName)
            {
                case '员工之家':
                    allXName[2] = result.beans[i].restaurantName;
                    morXData[2] = getMoney(result.beans[i].breakfastMoney);
                    noonXData[2] = getMoney(result.beans[i].lunchMoney);
                    eveXName[2] = getMoney(result.beans[i].dinnerMoney) + getMoney(result.beans[i].supperMoney);

                    break;
                case '中膳':
                    allXName[0] = result.beans[i].restaurantName;
                    morXData[0] = getMoney(result.beans[i].breakfastMoney);
                    noonXData[0] = getMoney(result.beans[i].lunchMoney);
                    eveXName[0] = getMoney(result.beans[i].dinnerMoney) + getMoney(result.beans[i].supperMoney);

                    break;
                case '和兴隆':
                    allXName[1] = result.beans[i].restaurantName;
                    morXData[1] = getMoney(result.beans[i].breakfastMoney);
                    noonXData[1] = getMoney(result.beans[i].lunchMoney);
                    eveXName[1] = getMoney(result.beans[i].dinnerMoney) + getMoney(result.beans[i].supperMoney);

                    break;
            }
        }

        payEchartFn('morEcharts',['早上'],allXName,morXData,1);
        payEchartFn('noonEcharts',['中午'],allXName,noonXData,1);
        payEchartFn('eveEcharts',['晚上'],allXName,eveXName,1);
        // payEchartFn('nigEcharts',['夜间'],allXName,nigXData,nigXData2,1);

    }else{
        payEchartFn('morEcharts',['早上']);
        payEchartFn('noonEcharts',['中午']);
        payEchartFn('eveEcharts',['晚上']);
        // payEchartFn('nigEcharts');
    }
}

//获取当前日期
function getToday(timeD){
    var today = "";
    var date = new Date();
    date.setDate(date.getDate() + timeD);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
    return today;
}


function payEchartFn (divId,nameData,xAxisData,allData,allData2) {
    var gridLit = {
            borderWidth:0
        },
        barMaxW = 50;
    var winWidth;
    if (window.innerWidth){
        winWidth = window.innerWidth;
    }else if ((document.body) && (document.body.clientWidth)){
        winWidth = document.body.clientWidth;
    }
    if(winWidth<1400){
        barMaxW =40;
        gridLit = {
            x:60,
            x2:10,
            borderWidth:0
        };
    };

    var payEchart = echarts.init(document.getElementById(divId));
    var option = {
        title: {
            show : false
        },
        grid: gridLit,
        xAxis: [
            {
                type: 'category',
                splitLine: false,
                axisLabel: {
                    interval: 0
                },
                data: xAxisData
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '金额(元)',
                splitLine: false
            }
        ],
        series: [
            {
                name: nameData[0],
                type: 'bar',
                data: allData,
                barMaxWidth:barMaxW,
                itemStyle: {
                    normal: {
                        color: function(params) {
                            var colorList = [echartColors[2],echartColors[0],echartColors[1],'rgb(141,219,241)','rgb(243,181,149)','rgb(143,197,243)',echartColors[2]];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            show: true,
                            position: 'top',
                            formatter:'{b}\n{c}'
                            // formatter: function(params){
                            //     var nameLIst = [params.name+'\n'+params.value+'元',params.name+'\n'+params.value+'元',''];
                            //     for(i = 0;i<params.series.data.length;i++){
                            //         if(params.value == params.series.data[i]) return nameLIst[i]
                            //     }
                            // }
                            // formatter: '{c}'
                        }
                    }
                },
                markPoint: {
                    data: function(){
                        var xAxisArry = [];
                        for (var i = 0; i < xAxisData.length; i++) {
                            xAxisArry[i] = {
                                xAxis:i,
                                y: 350,
                                name:xAxisData[i],
                                symbolSize:0
                            };
                        }
                        return xAxisArry;
                    }
                }
            }
        ]
    };
    payEchart.setOption(option);
}