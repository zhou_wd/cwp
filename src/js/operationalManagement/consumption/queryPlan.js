$(function () {
    getQueryPlanList(1);
});

//提交
$("#btnSearch").on("click",function(){
    var startPlanTime = $("#startPlanTime").val();
    var endPlanTime = $("#endPlanTime").val();
    if(startPlanTime=="" || startPlanTime == null){
        tipModal("icon-cross", "请选择开始日期");
        return;
    }
    if(endPlanTime=="" || endPlanTime == null){
        tipModal("icon-cross", "请选择结束日期");
        return;
    }

    $(".startPlanTimeSp").html(startPlanTime);
    $(".endPlanTimeSp").html(endPlanTime);
    $("#dataTotal").html("100");
    $("#modalPlanSubmitConfirm").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
});


//确认提交
$("#btnConfirmSubmit").on("click",function(){
    $("#modalPlanSubmitConfirm").modal("hide");
    $("#myLoading").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    PostForm("/cwp/front/sh/pay!execute", "uid=P026","", postQueryPlanResultArrived, '',"frmQueryPlan");
});

function postQueryPlanResultArrived(result){
    $("#myLoading").modal("hide");
    if(result.returnCode=="0"){
        $("#dataTotal").html(result.bean.queryCount);
        $("#modalPlanSubmitTip").modal({//启用modal ID
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal', centerModal());
        $(window).on('resize', centerModal());
    }
    else if(result.returnCode=="-9999"){
        tipModal("icon-cross", "查询失败");
    }
    else{
        tipModal("icon-cross", result.returnMessage);
    }

}

//确定关闭提示框
$("#btnConfirm").on("click",function(){
    $("#modalPlanSubmitTip").modal("hide");
});


//详情查看异常数据
function detailSelect(id) {
    $("#hidQueryPlanId").val(id);
    isFirst=true;
    queryList(1);
}

//获取异常数据
function queryList(pageIndex){
    var queryPlanId=$("#hidQueryPlanId").val();
    var loaderAbnormalDataTab = new LoaderTableNew("abnormalData","/cwp/front/sh/pay!execute","uid=P028&consumStatisticsId="+queryPlanId,pageIndex,10,abnormalDataResultArrived,"",abnormalDataPagination);
    loaderAbnormalDataTab.Display();
}

function abnormalDataResultArrived(result){
    if(result.returnCode ==0){
        var outHtml = template("abnormalDataTmpl", result);
        $("#abnormalData tbody").html(outHtml);
        $("#detailSelect").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal');
        $(window).on('resize', centerModal());
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}
//获取计划数据
function getQueryPlanList(pageIndex){
    var loaderPlanDataTab = new LoaderTableNew("queryPlanList","/cwp/front/sh/pay!execute","uid=P027_2",pageIndex,10,outputQueryPlanList,"",palnDataPagination);
    loaderPlanDataTab.Display();
}
//渲染数据
function outputQueryPlanList(result){
    if(result.returnCode ==0 && result.beans.length>0){
        var outHtml = template("dataTmpl", result);
        $("#queryPlanList").html(outHtml);
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}
//异常数据分页回调
function abnormalDataPagination(page_index) {
    if (!isFirst) queryList(page_index + 1);
}
//列表数据分页回调
function palnDataPagination(page_index) {
    if (!isFirst) getQueryPlanList(page_index + 1);
}