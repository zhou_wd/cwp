
$(function() {
    //consumptionList(1);
    getDept();
    getDcontractType();
    getTimeList();
    getCompanyList();

});
//数据查询
$("#btnSearchFaultConsume").on("click",function(){
    var timelist = $("#timelist").val();
    var chinaMobile = $("#chinaMobile").val();
    var companyId = $("#hidCompanyId").val();

    if(chinaMobile==""){
        tipModal("icon-cross", "请选择公司类型");
        return;
    }
    $("#subsidy").html("00.00");
    $("#money").html("00.00");
    $("#moneyTotal").html("00.00");
    isFirst=true;
    consumptionList(1);
});
//获取列表数据
function consumptionList(pageIndex){
    var loaderTab = new LoaderTableNew("consumptionList","/cwp/front/sh/pay!execute","uid=P029",pageIndex,8,consumptionResultArrived,"",consumptionPagination,"frmSearch");
    loaderTab.Display();
}

//内外部部门选择，选择一个清空另一个
$("#companyList").on("change",function(){
    $("#deptList").selectpicker("val","");
    $("#hidCompanyId").val($(this).val());
});


$("#deptList").on("change",function(){
    $("#companyList").selectpicker("val","");
    $("#hidCompanyId").val($(this).val());
});


$("#canteen").on("change",function(){
    $("#hidGroupName").val($(this).val());
});


$("#chinaMobile").on("change",function(){
    $("#deptList").selectpicker("val","");
    $("#companyList").selectpicker("val","");
    $("#hidCompanyId").val("");
    if($(this).val()==0){
        $("#companyLi").hide();
        $("#deptLi").show();
    }
    else{
        $("#companyLi").show();
        $("#deptLi").hide();
    }
});

//合同类型选择
$("#contractType").on("change",function(){
    $("#hidContractType").val($(this).val());
});


//生成报表数据
function consumptionResultArrived(result){
    if(result.returnCode=="0"){
        //福利余额绝对值
        template.helper('subsidyAbs', function(inp) {
            return Math.abs(inp).toFixed(2);
        });
        //充值消费额绝对值
        template.helper('moneyAbs', function(inp) {
            return Math.abs(inp).toFixed(2);
        });
        //合计消费额绝对值
        template.helper('moneyTotalAbs', function(inp) {
            return Math.abs(inp).toFixed(2);
        });
        var outHtml = template("dataTmpl", result);
        $("#consumptionList tbody").html(outHtml);
        if(result.bean!=null){
            $("#subsidy").html(Math.abs(result.bean.subsidy).toFixed(2));
            $("#money").html(Math.abs(result.bean.money).toFixed(2));
            $("#moneyTotal").html(Math.abs(result.bean.moneyTotal).toFixed(2));
        }
        else{
            $("#subsidy").html("00.00");
            $("#money").html("00.00");
            $("#moneyTotal").html("00.00");
        }
    }

}
//数据分页
function consumptionPagination(page_index) {
    if (!isFirst) consumptionList(page_index + 1);
}

//获取所有部门
function getDept(){
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute", "uid=bedUserDept_003", "", outputDeptList, '',"");
}

//渲染部门下拉多选
function outputDeptList(result){
    if(result.returnCode=="0"){
        var option ="";
        var data=JSON.parse(result.object);
            for(var i=0; i<data.length; i++){
                option="<option value='"+data[i].id+"'>"+data[i].name+"</option>"
                $("#deptList").append(option);
            }
        $("#deptList").selectpicker('refresh');
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}

//获取合同类型
function getDcontractType(){
    PostForm("/cwp/front/sh/dict!execute", "uid=D012_3&dictKey=contract_type", "", outputDcontractTypeList, '');
}

//合同类型列表
function outputDcontractTypeList(result){
    if(result.returnCode=="0"){
        for(var i=0; i<result.beans.length; i++){
            option="<option value='"+result.beans[i].dictdataKey+"'>"+result.beans[i].dictdataName+"</option>"
            $("#contractType").append(option);
        }
        $("#contractType").selectpicker('refresh');
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}

//获取所有时间段
function getTimeList(){
    PostForm("/cwp/front/sh/pay!execute", "uid=P027&pageSize=12&currentPage=1", "", outputTimeList, '');
}

//渲染下拉时间段
function outputTimeList(result){
    //无计划时提示
    //tipModal("icon-info2", "<a class='jf-link' style='font-size:12px;' href='/cwp/src/static/operationalManagement/consumption/queryPlan.html?menuId=2.4.3'>暂无查询计划，点击跳转至添加查询计划页面</a>","","100000");
    if(result.returnCode ==0){
        template.helper('startTimeFormat', function(inp) {
            inp = inp.split(" ");
            return inp[0];
        });
        template.helper('endTimeFormat', function(inp) {
            inp = inp.split(" ");
            return inp[0];
        });
        var outHtml = template("timelistTmpl", result);
        $("#timelist").html(outHtml);
        $("#timelist").selectpicker('refresh');
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}

//获取公司列表
function getCompanyList(){
    PostForm("/cwp/front/sh/company!execute", "uid=c006_2", "", outputCompanyList, '');
}

function outputCompanyList(result){
    if(result.returnCode=="0"){
        for(var i=0; i<result.beans.length; i++){
            option="<option value='"+result.beans[i].companyId+"'>"+result.beans[i].companyName+"</option>"
            $("#companyList").append(option);
        }
        $("#companyList").selectpicker('refresh');
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}

