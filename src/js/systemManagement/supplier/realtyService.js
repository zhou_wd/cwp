﻿//页面加载显示供应商的所有信息

//搜索供应商列表
function searchRealtyList(pageIndex) {
    // var companyName=$("#keyword").val();
    // if(companyName==undefined)
    // {
    //     companyName="";
    // }
    $.getJSON('/cwp/src/json/supplier/company_c001.json',function(data){
        var loaderRealtyTab = new LoaderTable("loaderRealtyTab","tabRealty","/cwp/front/sh/company!execute","uid=c001",pageIndex,10,successResultArrivedByRealtyService,"","frmSearch1");
        loaderRealtyTab.loadSuccessed(data);
    });
    // loaderRealtyTab.Display();
}
//回调函数成功
function successResultArrivedByRealtyService(result) {
    //拼接表头html
    var thead="<thead>"+
        "<tr>"+
        "<th>公司名称</th>"+
        "<th>公司类型</th>"+
        "<th>公司地址</th>"+
        "<th>公司电话</th>"+
        "<th>公司负责人</th>"+
        "<th>负责人电话</th>"+
        "<th>状态</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html
    var tbodys = "<tbody>";
    for(var i=0;i<result.beans.length;i++){
        var company = result.beans[i];
        tbodys+=
            "<tr><td>"+company.companyName+"</td>"+
            "<td>"+company.companyType+"</td>"+
            "<td>"+company.companyAddress+"</td>"+
            "<td>"+company.companyPhone+"</td>"+
            "<td>"+company.companyArtificialPerson+"</td>"+
            "<td>"+company.artificialPersonPhone+"</td>"+
            "<td id='status"+company.companyId+"'>"+(company.status=="0"?"启用":"禁用")+"</td>"+
            "<td>" +
            "<a class='t-btn t-btn-sm t-btn-blue t-btn-edit' onclick=updateCompany('"+company.companyId+"')>修改</a>"+
            "<a id='operate"+company.companyId+"' class='t-btn t-btn-sm "+(company.status=="0"?"t-btn-red":"")+"' onclick=freezeCompany('"+company.companyId+"')>"+(company.status=="0"?"禁用":"启用")+"</a>"+
            "</td>" +
            "</tr>";
    }
    tbodys += "</tbody>";
    //显示表格html
    $("#tabRealty").html(thead+tbodys);
}

// 操作 开启/关闭 状态
function freezeCompany(companyId){
    var status1 = $("#tabRealty #status"+companyId).html();
    $("#companyId").val(companyId);
    if(status1 == "启用"){
        // PostForm("/cwp/front/sh/company!execute", "uid=c005&code="+1,"", successResultChange, '','frmRealtyService');
        $.getJSON('/cwp/src/json/supplier/company_c005.json',function(data){
            successResultChange(data);
        })
        $("#tabRealty #status"+companyId).html("禁用");
        $("#tabRealty #operate"+companyId).html("启用").removeClass("t-btn-red");
    }else{
        // PostForm("/cwp/front/sh/company!execute", "uid=c005&code="+0, "", successResultChange, '','frmRealtyService');
        $.getJSON('/cwp/src/json/supplier/company_c005.json',function(data){
            successResultChange(data);
        })
        $("#tabRealty #status"+companyId).html("启用");
        $("#tabRealty #operate"+companyId).html("禁用").addClass("t-btn-red");
    }
}




//请求成功回调函数
function successResultChange(result) {

    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "修改成功");
            break;
        case "2":
            tipModal("icon-cross", "修改失败");
            break;
        case "-9999":
            tipModal("icon-cross", "修改失败");
            break;
        default:
            break;
    }
}
//条件查询
$("#btn_query").on("click",function(){
    isPostBack=true;
    searchRealtyList(1);
});

