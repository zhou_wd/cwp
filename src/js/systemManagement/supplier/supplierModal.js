﻿////绑定点击事件 新增供应商
$("#btnAddSupplier").on("click",function(){
    $("#modalSupplier .modal-title").text("新增设备供应商");
    $("#modalSupplier :text").val("");
    displayModalSupplier();
});

//显示模态框
function  displayModalSupplier()
{    //清空表单
    $(".tipArea div").html("").attr("class","");
    $("#address").val("");
    $("#modalSupplier").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());

}

$(function(){
    supplierValidator();
});

function supplierValidator(){
    $.formValidator.initConfig({
        formid: "frmSupper",
        onerror: function (msg) {return false;},
        onsuccess: function () {return true;}
    });


    $("#vendorName").formValidator({
        onfocus: "请输入公司名称",
        oncorrect: " "
    }).InputValidator({
        min:2,
        max: 30,
        onempty: "请输入公司名称",
        onerror: "不得大于30字符"
    }).RegexValidator({
        regexp: "companyname",
        datatype: "enum",
        onerror: "不得大于30字符"
    }).FunctionValidator({
        fun: function (val, elem) {
            var oldVendorName=$("#oldVendorName").val();
            if (val==oldVendorName) {
                return true;
            }
            else{
                var htmlObj= $.ajax({
                    type:"post",
                    url:"/cwp/front/sh/vendor!execute",
                    beforeSend:function(requests){
                        requests.setRequestHeader("platform", "pc");
                    },
                    async:false,
                    dataType: "json",
                    data:{
                        uid:"v010",
                        vendorName:$("#vendorName").val()
                    }
                });
                var result=JSON.parse(htmlObj.responseText);
                if (result.returnCode == "0") {
                    return true;
                } else {
                    return "该公司名称已经存在";
                }

            }
        }
    });



    $("#name").formValidator({
        onfocus: "请输入供应商负责人姓名",
        oncorrect: " "
    }).InputValidator({
        min: 4,
        max: 8,
        onempty: "请输入供应商负责人",
        onerror: "为4-8个字符,英文名请填简称"
    }).RegexValidator({
        regexp: "realname",
        datatype: "enum",
        onerror: "最长8个字符，英文名请填简称"
    });

    $("#phone").formValidator({
        onfocus: "请输入负责人手机",
        oncorrect: " "
    }).InputValidator({
        min: 11,
        max: 11,
        onempty: "请输入负责人手机",
        onerror: "为11个字符长度,且为数字"
    }).RegexValidator({
        regexp: "mobile",
        datatype: "enum",
        onerror: "例:15972566585"
    });

    $("#liaison").formValidator({
        onfocus: "请输入供应商联接口人",
        oncorrect: " "
    }).InputValidator({
        min: 4,
        max: 8,
        onempty: "请输入供应商接口人",
        onerror: "为4-8个字符,英文名请填简称"
    }).RegexValidator({
        regexp: "realname",
        datatype: "enum",
        onerror: "最长8个字符，英文名请填简称"
    });

    $("#liaisonPhone").formValidator({
        onfocus: "请输入供应商接口人手机",
        oncorrect: " "
    }).InputValidator({
        min: 11,
        max: 11,
        onempty: "请输入供应商接口人手机",
        onerror: "为11个字符长度,且为数字"
    }).RegexValidator({
        regexp: "mobile",
        datatype: "enum",
        onerror: "例:15972566585"
    });
}


/*
 查看详情
 */
function updateVendorInfo(vendorId){
    $("#vendorId").val(vendorId);
    // PostForm("/cwp/front/sh/vendor!execute", "uid=v002", "", successResultUpdate, '',"frmSupper");
    $.getJSON('/cwp/src/json/supplier/vendor_v002.json',function(data){
        successResultUpdate(data);
    })
    //editSupplierValidator();
}

//请求成功回调函数 显示编辑信息
function successResultUpdate(result) {
    if (result.returnCode == '0') {

        displayModalSupplier();
        $("#modalSupplier .modal-title").text("编辑设备供应商信息");
        $("#vendorName").val(result.bean.vendorName);
        $("#oldVendorName").val(result.bean.vendorName);
        $("#name").val(result.bean.vendorAttn1);
        $("#phone").val(result.bean.vendorAttn1Phone);
        $("#tel").val(result.bean.vendorPhone);
        $("#liaison").val(result.bean.liaison);
        $("#liaisonPhone").val(result.bean.liaisonPhone);
        $("#address").val(result.bean.vendorAddress);
    }
}

/**
 * 添加设备供应商
 */
$("#btn_saveVendor").on("click",function(){
    //判断表单验证是否通过
    // var companyName=$("#vendorName").val();
    if ($.formValidator.PageIsValid()) {
        var vendorId = $("#vendorId").val();
        if(vendorId!="" && vendorId!=null && vendorId!=undefined)
        {
            //修改
            // PostForm("/cwp/front/sh/vendor!execute", "uid=v003", "post", successSaveVendor, '',"frmSupper");
            $.getJSON('/cwp/src/json/supplier/vendor_v003.json',function(data){
                successSaveVendor(data);
            })
        }
        else
        {
            //新增
            // PostForm("/cwp/front/sh/vendor!execute", "uid=v005","post", successSaveVendor, '',"frmSupper");
            $.getJSON('/cwp/src/json/supplier/vendor_v005.json',function(data){
                successSaveVendor(data);
            })
        }
        $("#modalSupplier").modal("hide");
    }
});



//请求成功回调函数
function successSaveVendor(result) {
    //关闭模态框
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "保存成功");
            isPostBack=true;
            searchSupplierList(1);
            break;
        case "2":
            tipModal("icon-cross", "保存失败");
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
        default:
            break;
    }
}
