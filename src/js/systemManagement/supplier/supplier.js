﻿//页面加载显示供应商的所有信息
$(function(){
    searchSupplierList(1);
});
var type=0;
$("#myTabs li").on("click",function(){
    type = $(this).index();
    isPostBack=true;
    $("#keyword1").val("");
    $("#keyword").val("");
    if($(this).index()==0) {
        searchSupplierList(1);
    } else {
        searchRealtyList(1);
    }
});

//搜索供应商列表
function searchSupplierList(pageIndex)
{
    // var keyword1=$("#keyword1").val();
    // if(keyword1==undefined)
    // {
    //     keyword1="";
    // }
    $.getJSON('/cwp/src/json/supplier/vendor_v001.json',function(data){
        var loaderVendorTab = new LoaderTable("loaderVendorTab","tabVendor","/cwp/front/sh/vendor!execute","uid=v001",pageIndex,10,successResultArrived,"","frmSearch");
        loaderVendorTab.loadSuccessed(data);
    });
    // loaderVendorTab.Display();
}


//回调函数成功 拼接列表
function successResultArrived(result) {
    if (result.returnCode == '1') {

        //拼接表头html
        var thead="<thead>"+
            "<tr>"+
            "<th>公司名称</th>"+
            "<th>公司负责人</th>"+
            "<th>负责人手机</th>"+
            "<th>地址</th>"+
            "<th>办公电话</th>"+
            "<th>状态</th>"+
            "<th>操作</th>"+
            "</tr>"+
            "</thead>";
        //拼接表格列表html
        var tbobys = "<tbody>";
        for (var i = 0; i < result.beans.length; i++) {
            var vendor = result.beans[i];
            var vendorName = vendor.vendorName;
            var vendorAttn1 = vendor.vendorAttn1;
            var phone = vendor.vendorPhone;
            var vendorAddress = vendor.vendorAddress;
            var vendorAttn1Phone = vendor.vendorAttn1Phone;
            var vendorId = vendor.vendorId;
            var status = vendor.status;
            var className="";
            var str_status = "";
            var str_operate = "";
            if (status == "0") {
                str_status = "启用";
                str_operate = "禁用";
                className="t-btn-red";
            } else if (status == "1") {
                str_status = "禁用";
                str_operate = "启用";
            }

            tbobys+="<tr><td>" + vendorName + "</td>"+
                "<td>" + vendorAttn1 + "</td>" +
                "<td>" + vendorAttn1Phone + "</td>" +
                "<td>" + vendorAddress + "</td>" +
                "<td>" + phone + "</td>" +
                "<td id='status" + vendorId + "'>" + str_status + "</td>" +
                "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-edit' onclick=updateVendorInfo('" + vendorId + "')>修改</a>" +
                "<a id='operate" +vendorId + "' class='t-btn t-btn-sm "+className+"' onclick=freezeVendor('" + vendorId + "')>" + str_operate + "</a></td>" +
                "</tr>";
        }
        tbobys += "</tbody>";
        $("#tabVendor").html(thead+tbobys);
    }
}






// 操作 开启/关闭 状态
function freezeVendor(vendorId){
    var status = $("#tabVendor #status"+vendorId).html();
    $("#vendorId").val(vendorId);
    if(status == "启用"){
        // PostForm("/cwp/front/sh/vendor!execute", "uid=v004&status="+1, "", successResultUpdateVendor, '');
        $.getJSON('/cwp/src/json/supplier/vendor_v004.json',function(data){
            successResultUpdateVendor(data);
        })
        $("#tabVendor #status"+vendorId).html("禁用");
        $("#tabVendor #operate"+vendorId).html("启用").removeClass("t-btn-red");
    }else{
        // PostForm("/cwp/front/sh/vendor!execute", "uid=v004&status="+0, "", successResultUpdateVendor, '');
        $.getJSON('/cwp/src/json/supplier/vendor_v004.json',function(data){
            successResultUpdateVendor(data);
        })
        $("#tabVendor #status"+vendorId).html("启用");
        $("#tabVendor #operate"+vendorId).html("禁用").addClass("t-btn-red");
    }
}





//请求成功回调函数
function successResultUpdateVendor(result) {
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "修改成功");
            break;
        case "2":
            tipModal("icon-cross", "修改失败");
            break;
        case "-9999":
            tipModal("icon-cross", "修改失败");
            break;
        default:
            break;
    }
}




/*
 条件查询
 */
$("#btnSearch").on("click",function(){
    isPostBack=true;
    searchSupplierList(1);
});

//当前页，加载容器
function paginationCallback(page_index){
    if(!isPostBack) {
        if($("#myTabs .active").index()==0)
        {
            searchSupplierList(page_index+1);
        }
        else{
            searchRealtyList(page_index+1);
        }

    }
}