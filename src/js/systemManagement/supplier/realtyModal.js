﻿////绑定新增点击事件
$("#btnAddRealty").on("click", function () {
    $("#modalRealty .modal-title").text("新增服务供应商");
    $("#modalRealty :text").val("");
    displayModalRealty();
});


//显示模态框
function displayModalRealty()
{
    $(".tipArea div").html("").attr("class","");
    $("#companyAddress").val("");
    $("#modalRealty").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
}

$(function(){
    companyValidator();
});

function companyValidator(){
    $.formValidator.initConfig({
        validatorGroup:"2",
        formid: "frmRealtyService",
        onerror: function (msg) {return false;},
        onsuccess: function () {return true;}
    });
    $("#companyName").formValidator({
        validatorGroup:"2",
        onfocus: "请输入公司名称",
        oncorrect: " "
    }).InputValidator({
        min: 2,
        max: 30,
        onempty: "请输入公司名称",
        onerror: "不得大于30字符"
    }).RegexValidator({
        regexp: "companyname",
        datatype: "enum",
        onerror: "不得大于30字符"
    }).FunctionValidator({
        fun: function (val, elem) {
            var oldCompanyName=$("#oldCompanyName").val();
            if (val==oldCompanyName) {
                return true;
            }
            else{
                var htmlObj= $.ajax({
                    type:"post",
                    url:"/cwp/front/sh/company!execute",
                    beforeSend:function(requests){
                        requests.setRequestHeader("platform", "pc");
                    },
                    dataType: "json",
                    async:false,
                    data:{
                        uid:"c007",
                        companyName:$("#companyName").val()
                    }
                });
                var result=JSON.parse(htmlObj.responseText);
                if (result.returnCode == "0") {
                    return true;
                } else {
                    return "该公司名称已经存在";
                }

            }
        }
    });
    $("#companyType").formValidator({
        validatorGroup:"2",
        onfocus: "请输入公司类型",
        oncorrect: " "
    }).InputValidator({
        min:4,
        max:20,
        onempty: "请输入公司类型",
        onerror: "不得大于20字符"
    });


    $("#companyPhone").formValidator({
        validatorGroup:"2",
        onfocus: "请输入公司电话号码",
        oncorrect: " "
    }).InputValidator({
        min: 8,
        max: 12,
        onempty: "请输入公司电话号码",
        onerror: "电话号码长度不正确"
    }).RegexValidator({
        regexp: "tel",
        datatype: "enum",
        onerror: "请填写公司电话"
    });

    $("#companyArtificialPerson").formValidator({
        validatorGroup:"2",
        onfocus: "请输入公司负责人姓名",
        oncorrect: " "
    }).InputValidator({
        min:4,
        max:12,
        onempty: "请输入公司负责人姓名",
        onerror: "字符长度不得小于4，不得大于8"
    }).RegexValidator({
        regexp: "realname",
        datatype: "enum",
        onerror: "字符长度不得小于4，不得大于8"
    });

    $("#artificialPersonPhone").formValidator({
        validatorGroup:"2",
        onfocus: "请输入负责人电话",
        oncorrect: " "
    }).InputValidator({
        min:11,
        max:11,
        onempty: "请输入负责人电话",
        onerror: "不得超过11个字符"
    }).RegexValidator({
        regexp: "mobile",
        datatype: "enum",
        onerror: "例：15972566585"
    });
}





//编辑按钮
function updateCompany(companyId) {
    $("#companyId").val(companyId);
    // PostForm("/cwp/front/sh/company!execute", "uid=c003", "", successResultupdate, '','frmRealtyService');
    $.getJSON('/cwp/src/json/supplier/company_c003.json',function(data){
        successResultupdate(data);
    })
}

//请求成功回调函数
function successResultupdate(result) {
    if (result.returnCode == '0') {

        displayModalRealty();
        $("#exampleModalLabel").html("编辑服务供应商信息");
        $("#companyName").val(result.object.companyName);
        $("#oldCompanyName").val(result.object.companyName);
        $("#companyType").val(result.object.companyType);
        $("#companyAddress").val(result.object.companyAddress);
        $("#companyPhone").val(result.object.companyPhone);
        $("#companyArtificialPerson").val(result.object.companyArtificialPerson);
        $("#artificialPersonPhone").val(result.object.artificialPersonPhone);

    }
}


//保存操作
$("#btn_saveRealty").on("click", function () {
    //判断表单验证是否通过
    if ($.formValidator.PageIsValid('2')) {
        var companyId = $("#companyId").val();  //当前记录标识UUID
        if (companyId != "" && companyId !=undefined) {
            // PostForm("/cwp/front/sh/company!execute", "uid=c004", "post", successResultSave, '',"frmRealtyService");
            $.getJSON('/cwp/src/json/supplier/company_c004.json',function(data){
                successResultSave(data);
            })
        } else {
            // PostForm("/cwp/front/sh/company!execute", "uid=c002", "post", successResultSave, '',"frmRealtyService");
            $.getJSON('/cwp/src/json/supplier/company_c002.json',function(data){
                successResultSave(data);
            })
        }
        //关闭模态框
        $("#modalRealty").modal("hide");

    }
});
//新增/修改请求成功回调函数
function successResultSave(result) {

    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "保存成功");
            isPostBack=true;
            searchRealtyList(1);
            break;
        case "1":
            tipModal("icon-cross", "保存失败");
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
        default:
            break;
    }
}
