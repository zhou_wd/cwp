﻿var vendorIds="";
var childrenInfo = new Array();

var numberChildren=0;
//获取供应商
$(function(){
    // vendorList();
    // equipmentValidtor();
    // warningDictionaryType();
    $(".cameraType").hide();
});



//查询供应商
function vendorList(){
    PostForm("/cwp/front/sh/vendor!execute", "uid=v006", "",vendorListResultArrived, '');
}

function vendorListResultArrived(result)
{
    var sel = $("#vendorList");
    var option = "";
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].vendorName).val(result.beans[i].vendorId);
        sel.append(option);
    }
}



//获取建筑结构列表
function getDeptTreeModal(zNodes){
    if(zNodes!=null){
        //配置组织结构树
        var setting = {
            view: {
                dblClickExpand: false,
                showIcon: false,                 //隐藏节点图标
                selectedMulti: false             //设置是否允许同时选中多个节点
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                beforeClick: beforeClickSelect
            }
        };
        $.fn.zTree.init($("#treeOrgModal"), setting, zNodes);
    }
}

function beforeClickSelect(treeId, treeNode) {
    var treeObj = $.fn.zTree.getZTreeObj("treeOrgModal");
    $("#buildingname").val(treeNode.name);
    $("#buildingname").blur();
    $("#hidBuildingId").val(treeNode.id);
    hideMenu();
    return false;
}


function getParentNodes(node,allNode){
    if(node!=null){
        allNode += ">"+node['text'];
        curNode = node.getParentNode();
        getParentNodes(curNode,allNode);
    }else{
        //根节点
        curLocation = allNode;
    }
}

$("#buildingname").on("focus",function(){
    showMenu();
});




function showMenu() {
    var deptIdObj = $("#buildingname");
    var deptIdOffset = $("#buildingname").offset();
    $("#buildingModal").css({left:deptIdOffset.left + "px", top:deptIdOffset.top + deptIdObj.outerHeight() + "px"}).slideDown("fast");
    $("body").bind("mousedown", onBodyDown);
}

function hideMenu() {
    $("#buildingModal").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);
}


function onBodyDown(event) {
    if (!(event.target.id == "buildingname" || event.target.id == "buildingModal" || $(event.target).parents("#buildingModal").length>0)) {
        hideMenu();
    }
}


//新增验证
function equipmentValidtor()
{
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });
    $("#deviceTypeId").formValidator({
        onfocus:"请选择设备类型",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择设备类型",
        onerror: "请选择设备类型"
    });


    $("#devicePositionCode").formValidator({
        onfocus: "设备编码不能重复",
        oncorrect: "&nbsp"
    }).InputValidator({
        min: 2,
        max: 60,
        onempty: "请输入设备编码",
        onerror: "设备编码长度为2-60个字符"
    }).FunctionValidator({
        fun: function (val, elem) {
            var oldCode=$("#oldDevicePositionCode").val();
            var result;
            if (val==oldCode) {
                return true;
            }
            else{

                var htmlObj= $.ajax({
                    type: "get",//发送请求方式
                    url: "/cwp/front/sh/device!execute?uid=d012",//发送请求路径
                    beforeSend:function(requests){
                        requests.setRequestHeader("platform", "pc");
                    },
                    datatype: "json",
                    async:false,
                    data:{
                        devicePositionCode:val
                    },
                    success: function (data) {
                        if (data.returnCode == "0") {
                            result = data.returnCode;
                            return true;
                        } else {
                            return false;
                        }
                    },
                    error: function () {
                        alert("服务器没有返回数据，可能服务器忙，请重试");
                    },
                });
                if (result == "0") {
                    return true;
                } else {
                    return "设备编码重复";
                }
            }
        }
    });
    $("#vendorList").formValidator({
        onfocus:"请选择供应商",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择供应商",
        onerror:"请选择供应商"
    });
    $("#deviceX").formValidator({
        tipid:"deviceCoordinateTip",
        onfocus: "请输X轴坐标",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 3,
        onempty: "请输X轴坐标",
        onerror: "长为1-3个字符"
    }).RegexValidator({
        regexp: "intege",
        datatype: "enum",
        onerror: "只能为整数"
    });

    $("#deviceY").formValidator({
        tipid:"deviceCoordinateTip",
        onfocus: "请输Y轴坐标",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 6,
        onempty: "请输Y轴坐标",
        onerror: "长为1-3个字符"
    }).RegexValidator({
        regexp: "intege",
        datatype: "enum",
        onerror: "只能为整数"
    });


    $("#buildingname").formValidator({
        onfocus:"请选择楼栋位置信息",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择楼栋位置信息",
        onerror:"请选择楼栋位置信息"
    });
}

//新增 显示模态框
$("#btnAddEquipment").on("click",function(){
    modalDistrict();
    $("#hidDeviceId").val("");
    $("#modalEquipment").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
});

//保存
$("#btnSave").on("click", function () {
    if($("#deviceTypeId").val() == 1010){
        isChdEquipmentValidatorVideo();
    }
    isChdEquipmentValidator();
    if ($.formValidator.PageIsValid()==true) {
        var hidDeviceId = $("#hidDeviceId").val();
        var requestIp = $("#controllerip").val();
        var isDeviceControl = $("input[name='isDeviceControl']:checked").val();
        var isDeviceInfo = $("input[name='isDeviceInfo']:checked").val();
        $("#requestIp").val(requestIp);
        chdTotal = $(".selectChd").length;
        var childrenDevice = "";
        for (var i = 0; i < chdTotal; i++) {
            var selectChildren = $('.selectChd').eq(i).val();
            var deviceCodeChildren = $('.deviceCodeChd').eq(i).val();
            if (typeof selectChildren != 'undefined' && typeof deviceCodeChildren != 'undefined') {
                childrenDevice += selectChildren + "$" + deviceCodeChildren + ","
            }
        }

        $("#childrenDevice").val(childrenDevice);
        if (hidDeviceId == "") {
            PostForm("/cwp/front/sh/device!execute", "uid=d009&isDeviceInfo"+isDeviceInfo+"&isDeviceControl"+isDeviceControl, "post", addDeviceResultArrived, '', "frmMain");
        } else {
            PostForm("/cwp/front/sh/device!execute", "uid=d011&isDeviceInfo"+isDeviceInfo+"&isDeviceControl"+isDeviceControl, "post", editDeviceResultArrived, '', "frmMain");
        }
    }
});


//添加 保存回调
function addDeviceResultArrived(result){
    switch (result.returnCode) {
        case "0":
            $("#modalEquipment").modal("hide");
            tipModal("icon-checkmark", "保存成功");
            isFirst=true;
            getDeviceByPage(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    }
}
function editDeviceResultArrived(result){
    switch (result.returnCode) {
        case "0":
            $("#modalEquipment").modal("hide");
            tipModal("icon-checkmark", "修改成功");
            isFirst=true;
            getDeviceByPage(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "修改失败");
            break;
    }
}

//设备类型类型选择
$('#deviceTypeId').on("change" ,function(){
    var parentId=$('#deviceTypeId').val();
    //判断是否有子设备
    if(parentId!="" && parentId!=null){
        getChildrenDevice(parentId);
    }
    else{
        clearChildrenDevice();
    }

});

//子设备清空
function clearChildrenDevice(){
    $("#addChildrenList").html("");
}

//获取子设备
function getChildrenDevice(parentId){
    if(parentId==1010){
        $(".cameraType").show();
    }else{
        $(".cameraType").hide();
    }
    PostForm("/cwp/front/sh/dict!execute", "uid=D006&dictValue="+parentId, "",getChildrenDeviceResultArrived, '');
}

function getChildrenDeviceResultArrived(result){
    if(result.returnCode="0"){
        var sel = $(".selectChd");
        var option = "";
        sel.empty();
        option = $("<option>").text("--请选择--").val("");
        sel.append(option);
        for (var i = 0; i < result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].dictdataName).val(result.beans[i].dictdataKey);
            sel.append(option);
        }
    }
    else{
        clearChildrenDevice();
    }
}

//添加子设备
function addChildren(){
    var chdCount=$(".selectChd").length+1;
    var liInfo="<ul class='t-columns-group clearfix children-item'>" +
                    "<li>"+
                    "<label><span class='t-tip-verification'>*</span>子设备名称</label>"+
                    "<div>"+
                    "<select class='selectChd'>"+
                    "<option value=''>--请选择--</option>"+
                    "</select>"+
                    "</div>"+
                    "</li>"+
                    "<li>"+
                    "<label><span class='t-tip-verification'>*</span>子设备编号</label>"+
                    "<div> <input class='deviceCodeChd' data-count="+chdCount+"></div>"+
                    "</li>"+
                    "<li class='addChildrenListDel'>"+
                    "<label><span class='t-tip-verification'><a class='t-btn t-btn-blue btn-delete-chd'>删除</a></span></label>"+
                    "</li>" +
                    "<li class=verify>"+
                    "<label></label>"+
                    "<div class=tipArea><div></div></div>"+
                    "</li>"+
                    "<li class=verify>"+
                    "<label></label>"+
                    "<div class=tipArea><div></div></div>"+
                    "</li>"+
            "</ul>";

    $('#addChildrenList').append(liInfo);

    var parentId=$('#deviceTypeId').val()
    if(parentId != "")
    getChildrenDevice(parentId);

}


//移除子设备录入文本框
$(document).on("click",".btn-delete-chd",function(){
    $(this).parent().parent().parent().parent().remove();
    $(".deviceCodeChd").each(function (index) {
        if(index>1){
            $(this).attr("data-count",index);
        }
    });
});


//子设备验证
var isChdEquipmentValid=true;
function isChdEquipmentValidator(){
    isChdEquipmentValid=true;
    $(".selectChd").each(function () {
        var selectChd_text=$(this).val();
        if(selectChd_text==""||selectChd_text==undefined){
            $(this).parents("ul").find(".tipArea:eq(0) div").attr("class","onError").html("请选择子设备名称");
            isChdEquipmentValid=false;
        }
    });
    $("#hidBuildingId").each(function () {
        var text=$(this).val();
        if(text==""||text==null||text==undefined){
            $("#buildingFirstTip").attr("class","onError").html("请输入子设备编码");
            isChdEquipmentValid=false;
        }else{
            $("#buildingFirstTip").attr("class","onSuccess").html(" ");
        }
    });
    $(".deviceCodeChd").each(function () {
        var text=$(this).val();
        if(text==""||text==null||text==undefined){
            $(this).parents("ul").find(".tipArea:eq(1) div").attr("class","onError").html("请输入子设备编码");
            isChdEquipmentValid=false;
        }
    });
    return isChdEquipmentValid;
}

//视频设备验证
var isChdEquipmentValidVideo=true;
function isChdEquipmentValidatorVideo(){
    isChdEquipmentValidVideo=true;
    $("#videoId").each(function () {
        var text=$(this).val();
        if(text==""||text==null||text==undefined){
            $("#videoIdTip").attr("class","onError").html("请输入ID");
            isChdEquipmentValidVideo=false;
        }else{
            $("#videoIdTip").attr("class","onSuccess").html(" ");
        }
    });
    $("#videoIp").each(function () {
        var text=$(this).val();
        if(text==""||text==null||text==undefined){
            $("#videoIpTip").attr("class","onError").html("请输入服务器ID");
            isChdEquipmentValidVideo=false;
        }else{
            $("#videoIpTip").attr("class","onSuccess").html(" ");
        }
    });
    $("#typeValue").each(function () {
        var text=$(this).val();
        if(text==""||text==null||text==undefined){
            $("#typeTip").attr("class","onError").html("请选择设备类型");
            isChdEquipmentValidVideo=false;
        }else{
            $("#typeTip").attr("class","onSuccess").html(" ");
        }
    });
    return isChdEquipmentValidVideo;
}


//子设备名称验证
$(document).on("change","#hidBuildingId",function(){
    var text=$(this).val();
    if(text!=""&&text!=undefined){
        $("#buildingFirstTip").attr("class","onSuccess").html(" ");
    }
    else{
        $("#buildingFirstTip").attr("class","onFocus").html("请选择子设备名称");
    }
});
//子设备名称验证
$(document).on("change",".selectChd",function(){
    var text=$(this).val();
    if(text!=""&&text!=undefined){
        $(this).parents("ul").find(".tipArea:eq(0) div").attr("class","onSuccess").html(" ");
    }
    else{
        $(this).parents("ul").find(".tipArea:eq(0) div").attr("class","onFocus").html("请选择子设备名称");
    }
});

//子设备编号验证
$(document).on("focus",".deviceCodeChd",function(){
    $(this).parents("ul").find(".tipArea:eq(1) div").attr("class","onFocus").html("请输入子设备编码");
    $("#assoldDevicePositionCode").val($(this).val());
}).on("blur",".deviceCodeChd",function(){
    var text=$(this).val();
    var oldtext = $("#assoldDevicePositionCode").val();
    var indexItem=$(this);
    var indexCount=$(this).attr("data-count");
    var result;
    if(text==""||text==null||text==undefined){
        $(this).parents("ul").find(".tipArea:eq(1) div").attr("class","onError").html("请输入子设备编码");
    }
    else if(text.length>=2 && text.length<=60){
        var isEqual=true;

        //如果表单中无重复设备编码则与数据库进行比对
        if(isEqual==true){
            if(text != oldtext){
                $.ajax({
                    type: "get",//发送请求方式
                    url: "/cwp/front/sh/device!execute?uid=d012",//发送请求路径
                    datatype: "json",
                    beforeSend:function(requests){
                        requests.setRequestHeader("platform", "pc");
                    },
                    async:false,
                    data:{
                        devicePositionCode:text
                    },
                    success: function (data) {
                        if (data.returnCode == "0") {
                            result = data.returnCode;
                            return true;
                        } else {
                            return false;
                        }
                    },
                    error: function () {
                        alert("服务器没有返回数据，可能服务器忙，请重试");
                    },
                });
                if (result == "0") {
                    indexItem.parents("ul").find(".tipArea:eq(1) div").attr("class","onSuccess").html(" ");
                } else {
                    indexItem.parents("ul").find(".tipArea:eq(1) div").attr("class","onError").html("该编码重复");
                }
            }else{
                indexItem.parents("ul").find(".tipArea:eq(1) div").attr("class","onSuccess").html(" ");
            }
        }
    }
    else if(text.length<2||text.length>60){
        $(this).parents("ul").find(".tipArea:eq(1) div").attr("class","onError").html("设备编码长度为2-60个字符");
    }
});


//获取设备详情
$(document).on("click",".btn-detail",function(){
    modalDistrict();
    numberChildren="";
    childrenInfo = new Array();
    var deviceId = this.getAttribute("data-id");
    PostForm("/cwp/front/sh/device!execute", "uid=d010&deviceId="+deviceId,"",deviceByIdResultArrived, '');
    $.getJSON('/cwp/src/json/device/device_list_info_d010.json', function(data) {
            /*optional stuff to do after success */
        deviceByIdResultArrived(data);
    });
    // warningDictionaryType();
});

//设备详情
function deviceByIdResultArrived(result){
    $("#modalEquipment .modal-title").html("设备详情");
    $('input,select',$('form[id="frmMain"]')).attr('disabled',true);
    $('#devicePositionCode').val(result.bean.devicePositionCode);
    $('#devicePositionDescribe').val(result.bean.devicePositionDescribe);
    $("#deviceBrand").val(result.bean.vendorName);
    $("#controllerip").val(result.bean.controllerip);
    $("#interfaceaddr").val(result.bean.interfaceaddr);
    $('#deviceX').val(result.bean.deviceX);
    $('#deviceY').val(result.bean.deviceY);
    $("#deviceTypeId").val(result.bean.deviceTypeId);
    $("#vendorList").val(result.bean.vendorId);
    $("#buildingname").val(result.bean.buildingname);
    $("#typeValue").val(result.bean.type);
    $(".modal-footer").hide();
    $('#installTime').val(result.bean.deviceInstallTime.split(' ')[0]);
    $('#guarantee').val(result.bean.guarantee);
    $("#addChildren").addClass("hide");
    if(result.bean.deviceTypeId==1010){
        $(".cameraType").show();
    }
    $('#videoId').val(result.bean.videoId);
    $('#videoIp').val(result.bean.videoIP);
    $('#devicePort').val(result.bean.devicePort);


    $("#deviceTypeId").val(result.bean.deviceTypeId);
    for(var i=0;i<result.beans.length;i++){
        childrenInfo.push(result.beans[i]);
        numberChildren++;
        var liInfo="<ul class='t-columns-group clearfix children-item'>" +
                        "<li id=li"+numberChildren+">"+
                            "<label class='necessary'>子设备名称</label>"+
                            "<div>"+
                            "<select id=selectChildren"+numberChildren+" class='selectChd' disabled>"+
                            "</select>"+
                            "</div>"+
                        "</li>"+
                        "<li id=li2"+numberChildren+">"+
                        "<label class='necessary'>子设备编号</label>"+
                        "<div><input class='deviceCodeChd' id=deviceCodeChildren"+numberChildren+" disabled /></div>"+
                        "</li>"+
                        "</ul>";
        $('#addChildrenList').append(liInfo);

    }



    if(result.beans.length>0){
        checkChildren();
    }
    vendorIds=result.bean.vendorId;

    $("#modalEquipment").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());

}
//打开模态框
function modalDistrict(){
    //$("#modalAddDistrict").html(modal);

    $(".tipArea div").html("").attr("class","");
    //$("#parentId").val("");
    $("#modalDistrict .modal-title").html("新增设备");
    $("#devicePositionCode").val("");
    $("#devicePositionDescribe").val("")
    $("#deviceBrand").val("");
    $("#controllerip").val("");
    $("#interfaceaddr").val("");
    $("#deviceX").val();
    $("#deviceY").val();
    $("#vendorList").val("");
    $("#buildingname").val("--请选择--");
    $('#deviceX').val("");
    $('#deviceY').val("");
    $("#deviceTypeId").val("");
    $('#addChildrenList').find("ul").remove();
    $(".modal-footer").show();
    $('input,select',$('form[id="frmMain"]')).removeAttr("disabled");
    $("#addChildren").removeClass("hide");
    $(".cameraType").hide();
    $("#typeValue").val("");
    $("#devicePort").val("");
    $("#videoId").val("");
    $("#videoIp").val("");

    $("#num").val("");
    $("#buildingDesc").val("");



}

function warningDictionaryType(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002&dictKey=intelligentType&dictdataKey='security','building_services'", "", warningDictionarytResultArrived, '');
}
function warningDictionarytResultArrived(result){
    var sel = $("#waringType");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
        sel.append(option);
    }

}

function warningDictionaryType(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002&dictKey=intelligentType&dictdataKey='security','building_services'", "", warningDictionarytResultArrived, '');
}
function warningDictionarytResultArrived(result){
    var sel = $("#waringType");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
        sel.append(option);
    }
}


//通过设备类型ID获取子设备列表
function checkChildren(){
    var parentId=$('#deviceTypeId').val();
    PostForm("/cwp/front/sh/dict!execute", "uid=D006&dictValue="+parentId, "",warningAddParentResultArrived, '');
}

function warningAddParentResultArrived(result){
    numberChildren=parseInt(numberChildren)+1;
    for(var j=0;j<numberChildren;j++){
        var sel=$('#selectChildren'+j);
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for (var i = 0; i < result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].dictdataName).val(result.beans[i].dictdataKey);
            sel.append(option);
        }
    }
    selectChildrenList();
}

//获取选中的子设备列表
function selectChildrenList(){
    for(var j=0;j<childrenInfo.length;j++){
        $(".selectChd").eq(j).val(childrenInfo[j].deviceTypeId);
        $(".deviceCodeChd").eq(j).val(childrenInfo[j].devicePositionCode);
    }
}

//修改
$(document).on("click",".t-btn-edit",function(){
    modalDistrict();
    numberChildren="";
    childrenInfo = new Array();
    var deviceId = this.getAttribute("data-id");
    $("#hidDeviceId").val(deviceId);
    PostForm("/cwp/front/sh/device!execute", "uid=d010&deviceId="+deviceId,"",deviceEdit, '');
    warningDictionaryType();
});

function deviceEdit(result){
    $("#modalEquipment .modal-title").html("修改设备");
    $("#oldDevicePositionCode").val(result.bean.devicePositionCode);
    $('#devicePositionCode').val(result.bean.devicePositionCode);
    $("#hidBuildingId").val(result.bean.buildingId);
    $('#devicePositionDescribe').val(result.bean.devicePositionDescribe);
    $("#deviceBrand").val(result.bean.vendorName);
    $("#controllerip").val(result.bean.controllerip);
    $("#requestIp").val(result.bean.controllerip);
    $("#interfaceaddr").val(result.bean.interfaceaddr);
    $('#deviceX').val(result.bean.deviceX);
    $('#deviceY').val(result.bean.deviceY);
    $("#deviceTypeId").val(result.bean.deviceTypeId);
    $("#vendorList").val(result.bean.vendorId);
    $("#buildingname").val(result.bean.buildingname);
    $('#installTime').val(result.bean.deviceInstallTime.split(' ')[0]);
    $('#guarantee').val(result.bean.guarantee);
    $("#typeValue").val(result.bean.type);
    $('#videoId').val(result.bean.videoId);
    $('#videoIp').val(result.bean.videoIP);
    $('#devicePort').val(result.bean.devicePort);
    if (result.bean.isDeviceControl == "0") {
        $("#isDeviceControlYes")[0].checked = true;
        $("#addChildren").removeClass("hide");
    } else {
        $("#isDeviceControlNo")[0].checked = true;
        $("#addChildren").addClass("hide");
    }
    if(result.bean.deviceTypeId==1010){
        $(".cameraType").show();
    }

    $("#deviceTypeId").val(result.bean.deviceTypeId);

    for(var i=0;i<result.beans.length;i++){
        var chdCount=$(".selectChd").length+1;
        childrenInfo.push(result.beans[i]);
        numberChildren++;
        var liInfo="<ul class='t-columns-group clearfix children-item'><li id=li"+numberChildren+">"+
            "<label class='necessary'>子设备名称</label>"+
            "<div>"+
            "<select id=selectChildren"+numberChildren+" class='selectChd' ></select>"+
            "</div>"+
            "</li>"+
            "<li id=li2"+numberChildren+">"+
            "<label class='necessary'>子设备编号</label>"+
            "<div> <input class='deviceCodeChd' id=deviceCodeChildren"+numberChildren+"  /></div>"+
            "</li>"+
            "<li class='addChildrenListDel'>"+
            "<label ><span class='t-tip-verification '><a class='t-btn t-btn-blue btn-delete-chd'>删除</a></span></label>"+
            "</li>" +
            "<li class=verify>"+
            "<label></label>"+
            "<div class=tipArea><div></div></div>"+
            "</li>"+
            "<li class=verify>"+
            "<label></label>"+
            "<div class=tipArea><div></div></div>"+
            "</li></ul>";
        $('#addChildrenList').append(liInfo);
    }

    if(result.beans.length>0){
        checkChildren();
    }
    vendorIds=result.bean.vendorId;

    $("#modalEquipment").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());

}

//删除提示框
$("#delDevice").click(function(){
    $("#modalDelDept").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
})
$("#btnDeleteDept").click(function(){
    $('#addChildrenList').find("ul").remove();
    $("#modalDelDept").modal("hide");
    $("#addChildren").addClass("hide");
})
$("#delBack").click(function(){
    $("#yesDevice")[0].checked = true;
})
$("#yesDevice").click(function(){
    $("#addChildren").removeClass("hide");
})