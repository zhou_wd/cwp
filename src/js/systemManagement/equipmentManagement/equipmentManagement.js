﻿$(function() {
    equipmentType();
    initBuildingTree();
    getDeviceByPage(1);
});

//获取所有设备类型
function equipmentType(){
    $.getJSON('/cwp/src/json/device/device_type_c002.json',function(data){
        equipmentTypeResultArrived(data);
    })
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002&dictKey=intelligentType&dictdataKey='security','building_services'", "", equipmentTypeResultArrived, '');
}

//设备类型列表
function equipmentTypeResultArrived(result){
     var sel = $("#searchDeviceTypeId");
   	 var option = "";
   	 sel.empty();
     option = $("<option>").text("--请选择--").val("");
     sel.append(option);
     for ( var i = 0; i <   result.beans.length; i++) {
   	      var option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
   		  sel.append(option);
   	}
    $("#deviceTypeId").html($("#searchDeviceTypeId").html());
}



//请求所有建筑结构数据
function initBuildingTree(){
    $.getJSON('/cwp/src/json/build.json',function(data){
        successResultBuilding(data);
    })
    // PostForm("/cwp/front/sh/intebuilding!execute", "uid=building004", "", successResultBuilding, '',"frmTree");
}

function successResultBuilding(result){
    switch (result.returnCode) {
        case "0":
            if(result.beans.length>0)
                constructTree(result.beans);
            break;
        case "1":
            tipModal("icon-cross", "建筑树获取失败");
            break;
        case "-9999":
            tipModal("icon-cross", "建筑树获取失败");
            break;
    }
}

//重构树对象数据
function constructTree(data){
    var zNodes=[];
    //数据排序
    for(var i=0; i<data.length; i++){
        var obj = new Object();
        obj.id=data[i].buildingId;
        obj.pId=data[i].parentId;
        obj.name=data[i].buildingDesc
        if(i==0)
        obj.open=true;
        zNodes.push(obj);
    }
    var setting = {
        view: {
            showIcon: false,
            selectedMulti: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            beforeClick:beforeClick
        }
    };
    $.fn.zTree.init($("#treeOrg"), setting, zNodes);
    var zTree = $.fn.zTree.getZTreeObj("treeOrg");
    getDeptTreeModal(zNodes);
}
//点击节点事件
function beforeClick(treeId, treeNode){
    var buildingId=treeNode.id;
    $("#buildingId").val(buildingId);
    isFirst=true;
    getDeviceByPage(1);
}


//搜索设备列表
$("#btnSearch").on("click",function(){
    // isFirst=true;
    // getDeviceByPage(1);
});

//请求设备列表数据
function getDeviceByPage(pageIndex)
{
    $.getJSON('/cwp/src/json/device/device_list_d001.json',function(data){
            var loaderDeviceTab = new LoaderTableNew("deviceTable","/cwp/front/sh/device!execute","uid=d001",pageIndex,10,deviceListResultArrived,"",paginationCallback,"frmSearch");
           
            deviceListResultArrived(data);
            loaderDeviceTab.loadSuccessed(data);
    })

}

//渲染设备列表
function deviceListResultArrived(result)
{
    if(result.returnCode=="0"){
        template.helper('deviceStateFormat', function (inp) {
            if (inp == "0") {
                return "正常";
            } else if (inp == "1") {
                return "故障";
            } else if (inp == "2") {
                return "严重故障";
            }
        });
        var outHtml = template("dataTmpl", result);
        $("#deviceTable tbody").html(outHtml);
    }
}

//当前页，加载容器
function paginationCallback(page_index){
    if(!isFirst) getDeviceByPage(page_index+1);
}