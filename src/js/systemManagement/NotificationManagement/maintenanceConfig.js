﻿var deviceTy="";
var updateOrAdd=1;
$(function() {
    formValidator();
    queryList(1);
});
//------列表开始----

//设备故障通知列表
function queryList(pageIndex){
    var faultName=$('#faultName').val();
    var loaderTableFault = new LoaderTable("loaderTableEvent","deviceFault","/cwp/front/sh/deviceFault!execute","uid=c001",pageIndex,5,deviceFaultResultArrived,"","frmSearch");
    loaderTableFault.Display();
}

//发送告警信息回调函数
function deviceFaultResultArrived(result){
    var sHead="<thead>"+
        "<tr>"+
        "<th>设备名称</th>"+
        "<th>告警级别</th>"+
        "<th>故障类型</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys="<tbody>";
    for(var i=0;i<result.beans.length;i++){
        tbodys+="<tr><td>"+result.beans[i].dictName+"</td>"+
            "<td>"+result.beans[i].alarmLevel+"</td>"+
            "<td>"+result.beans[i].faultName+"</td>"+
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal' onclick=detailDeviceFault('"+result.beans[i].id+"')>详情</a></td></tr>";
    }
    tbodys+="</tbody>";
    $("#deviceFault").html(sHead+tbodys);
}

//点击查询
$(".t-list-search .t-btn").click(function (){
    isPostBack=true;
    if($("#myTabs .active").index()==0){
        queryList(1);
    }else{
        queryListAlarm(1);
    }
});



//tab切换事件
function selectTab(selectId){
    isPostBack=true;
    $("#faultName").val("");
    if(selectId==1){
        queryList(1);
        getDictionaryByKey();
        $(".t-list-search").show();
    }else{
        queryListAlarm(1);
        getDictionaryBySecurity();
        $(".t-list-search").hide();
    }
}
//安防预警通知列表
function queryListAlarm(){
    var diName=$('#diName').val();
    var applyTime=$("#applyTime").val();
    var eventDescribe=$('#eventDescribe').val();
    if("undefined" == typeof applyTime){
        applyTime="";
    }
    if("undefined" == typeof eventDescribe){
        eventDescribe="";
    }
    if("undefined" == typeof diName){
        diName="";
    }
    var eventProcessStatus=""
    var loaderAlarmTab = new LoaderTable("loaderAlarmTab","deviceFault","/cwp/front/sh/warningEvent!execute","uid=c001&dictValue="+diName+"&eventProcessStatus="+eventProcessStatus+"&eventType=2&applyTime="+applyTime+"&eventDescribe="+eventDescribe,1,5,WarningEventAlarmListResultArrived,"");
    loaderAlarmTab.Display();
}

function WarningEventAlarmListResultArrived(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>类别</th>"+
        "<th>来源</th>"+
        "<th>原因</th>"+
        "<th>时间</th>"+
        "<th>状态</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html
    var tbodys = "<tbody>";
    for(var i=0;i<result.beans.length;i++){
        var orsystem="";
        var sate="";
        if(result.beans.eventFromSystem==1){
            orsystem="系统";
        }else{
            orsystem="手动";
        }
        if(result.beans[i].eventProcessStatus=="1"){
            state="未处理"
        }else if(result.beans[i].eventProcessStatus=="2"){
            state="处理中"
        }else if(result.beans[i].eventProcessStatus=="3"){
            state="已恢复"
        }

        tbodys+="<tr><td>"+result.beans[i].deviceName+"</td>"+
            "<td>"+orsystem+"</td>"+
            "<td>"+result.beans[i].eventDescribe+"</td>"+
            "<td>"+result.beans[i].applyTime+"</td>"+
            "<td>"+state+"</td>"+
            "<td>" +
            "<a class='t-btn t-btn-sm t-btn-blue t-btn-deal' onclick=detailSelect('"+result.beans[i].warningEventId+"')>详情</a>" +
            "</td></tr>";
    }
    tbodys += "</tbody>";
    //显示表格html
    $("#deviceFault").html(thead+tbodys);

}

//当前页，加载容器
function paginationCallback(page_index){
    if(!isPostBack) {
        if($("#myTabs1 .active").index()==0){
            queryList(page_index+1);
        }else{
            queryListAlarm(page_index+1);
        }
    }
}
//新增验证
function formValidator(){
    $.formValidator.initConfig({
        formid: "frmAlert", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });
    $("#deviceTypeFirst").formValidator({
        onfocus:"请选择设备类型",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择设备类型",
        onerror: "请选择设备类型"
    });
    $("#deviceSecond").formValidator({
        onfocus:"请选择设备名称",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择设备名称",
        onerror: "请选择设备名称"
    });
    $("#alarmLevel").formValidator({
        onfocus:"请选择告警级别",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择告警级别",
        onerror: "请选择告警级别"
    });
    $("#faultType").formValidator({
        onfocus:"请选择故障类型",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择故障类型",
        onerror: "请选择故障类型"
    });

}


//新增打开弹出框
$(document).on("click","#btnAddAlertLevel", function () {
    var eventType= $("#myTabs1  .active ").attr("id");
    eventType=eventType.substring(4,eventType.length);
    if(eventType==1){
        $(".modal-title").html("新增维修配置");
        $("#alarmLevel option:first").prop("selected", 'selected');
        updateOrAdd=1;

        selectDeviceFirstList();
        faultTypeList();
        $("#deviceTypeFirst")[0].disabled=false;
        document.getElementById("deviceSecond").disabled=false;
        document.getElementById("alarmLevel").disabled=false;
        deviceTy="";
        var sel = $("#deviceSecond");
        var option=$("<option>").text("请选择").val("");
        sel.empty();
        sel.append(option);
        sel.append(option);
        $(".modal-footer").html(" <button type='button' class='btn btn-default' data-dismiss='modal'>取消</button>"+
            "<button type='button'' class='btn btn-primary' onclick='saveDeviceFault()''>保存</button>");
        $("#modalAlertLevel").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal', centerModal());
        $(window).on('resize', centerModal());
    }else{
        addWarningDictionaryType();
        buildingQuary();
        $(".modal-title").html("新增安防预警通知");
        $("#modalMaintenanceNew").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal', centerModal());
        $(window).on('resize', centerModal());
    }


});

//新增安防预警通知
function saveWarningEvent() {
    var applierId = localStorage.getItem("parkId");
    var addWarinngType = $('#addWaringType').val();
    var addWarningLevel = $('#addLevel').val();
    if (addWarningLevel.length == 0) {
        addWarningLevel = 0;
    }
    var addEventPostion = $('#buildingChild').val() + $('#addDetailPostion').val();
    var addDetailDesc = $('#addDetailDesc').val();
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c006&warningType=" + addWarinngType + "&warningLevel=" + addWarningLevel + "&eventDetail=" + addEventPostion + "&eventDescribe=" + addDetailDesc + "&applierId=" + applierId+"&eventProgress=1&eventType=2", "", saveWarningEventResultArrived, '');
    arr = new Array();
    //关闭模态框
    $("#modalMaintenanceNew").modal("hide");
}
function saveWarningEventResultArrived(result){
    if(result.returnCode=='0'){
        tipModal("icon-checkmark", "添加成功");
        queryListAlarm();
    }else{
        tipModal("icon-cross", "添加失败");
        queryListAlarm();
    }
}
//---------------------新增安防预警通知选项字段查询
function addWarningDictionaryType(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_2&dictKey=intelligentType&dictdataKey='security','building_services'", "", AddWarningDictionarytResultArrived, '');
}

function AddWarningDictionarytResultArrived(result){
    var sel = $("#addWaringType");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        if(result.beans[i].dictdataValue=="1004001")
        {
            option = $("<option>").text(result.beans[i].deviceName).val(result.beans[i].dictdataValue);
            sel.append(option);
        }
    }
}
//查找所有楼栋 chenqiuxu
function buildingQuary(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c004", "", buildingResultArrived, '');
}

function buildingResultArrived(result){
    var sel = $("#buildingNo");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
        sel.append(option);
    }
}
//根据楼，显示层
function selectbuildChild(){
    var parentId=$("#buildingNo").val();
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c005_3&parentId="+parentId, "", bParentResultArrived, '');
}

function bParentResultArrived(result){
    var sel = $("#buildingChild");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingDesc);
        sel.append(option);
    }
}

//------------------------新增安防预警通知结束--------------------------

//安防预警通知详情
function detailSelect(warningEventId){
    detailWarningEvent(warningEventId);
    $("#modalMaintenanceDetails").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
}
function detailWarningEvent(warningEventId){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c003&warningEventId="+warningEventId, "", detailWarningById, '');
}

function detailWarningById(result){
    $('#warningLevel').html();
    var state="";
    if(result.bean.eventProcessStatus=="1"){
        state="未处理"
    }else if(result.bean.eventProcessStatus=="2"){
        state="处理中"
    }else if(result.bean.eventProcessStatus=="3"){
        state="已恢复"
    }
    $('#deviceType').html(result.bean.deviceName)
    $('#deviceState').html(state);
    if(result.bean.warningLevel==1){
        $('#warningLevel').append(" <i class='con-star-full'></i> <i class='icon-star-full'></i> <i class='icon-star-full'></i> <i class='icon-star-full'></i>")
    }
    if(result.bean.warningLevel==2){
        $('#warningLevel').append(" <i class='con-star-full'></i> <i class='icon-star-full'></i> <i class='icon-star-full'></i> <i class='icon-star-full disabled '></i>")

    }
    if(result.bean.warningLevel==3){
        $('#warningLevel').append(" <i class='con-star-full'></i> <i class='icon-star-full'></i> <i class='icon-star-full disabled'></i> <i class='icon-star-full disabled '></i>")
    }
    if(result.bean.warningLevel==4){
        $('#warningLevel').append(" <i class='con-star-full'></i> <i class='icon-star-full disabled'></i> <i class='icon-star-full disabled'></i> <i class='icon-star-full disabled '></i>")
    }
    $('#devicePostion').html(result.bean.eventDetail);

    $('#warningDescribe').html(result.bean.eventDescribe);
    if(result.bean.handle.length!=0){
        $('#handle').html(result.bean.handle);
    }else{
        $('#handle').html("暂时未处理");
    }
    if(result.bean.handleTime.length!=0){
        $('#handleTime').html(result.bean.handle);
    }else{
        $('#handleTime').html("暂时未处理");
    }
}

//------------------新增设备故障通知字段查询--------------
//查询设备类型
function selectDeviceFirstList(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_2&dictKey=intelligentType&dictdataKey='security','building_services'", "", deviceListResultArrived, '');

}
//设备类型返回
function deviceListResultArrived(result){
    if(result.returnCode=="0"){
        var sel = $("#deviceTypeFirst");
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for ( var i = 0; i <result.beans.length; i++) {
            if(result.beans[i].dictdataValue!="1004001") {
                option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
                sel.append(option);
            }
        }
        //if(updateOrAdd==1){
        //    //document.getElementById("deviceType").disabled=false;
        //}
    }
}
//设备类型监听事件
$(document).on("change","#deviceTypeFirst", function () {
    selectDeviceSecondList();
});
function selectDeviceSecondList(){
    var sDictValue=$('#deviceTypeFirst').val();
    PostForm("/cwp/front/sh/dict!execute", "uid=D006_2&dictValue="+sDictValue, "",deviceListSecondResultArrived, '');
}

function deviceListSecondResultArrived(result){
    var sel = $("#deviceSecond");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].dictdataName).val(result.beans[i].dictdataKey);
        sel.append(option);
    }

    if(deviceTy.length!=0){
        $("#deviceSecond").val(deviceTy);
        document.getElementById("deviceSecond").disabled=true;
        //var kk = document.getElementById("deviceSecond").options;
        //for (var j=0; j<kk.length; j++) {
        //    if (kk[j].value==deviceTy) {
        //        //document.getElementById("deviceSecond").disabled=true;
        //        kk[j].selected=true;
        //        break;
        //    }
        //}
    }
}

//设备故障类型
function faultTypeList(){
    PostForm("/cwp/front/sh/deviceFault!execute", "uid=c002", "",faultListResultArrived, '');
}

function faultListResultArrived(result){
    var sel = $("#faultType");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].name).val(result.beans[i].code);
        sel.append(option);
    }
    if(updateOrAdd==1){
        document.getElementById("faultType").disabled=false;
    }
}
//设备故障通知保存
function saveDeviceFault(){
    if ($.formValidator.PageIsValid()) {
        PostForm("/cwp/front/sh/deviceFault!execute", "uid=c003", "",addDeviceFaultResultArrived, '',"frmAlert");
    }
}
function addDeviceFaultResultArrived(result){
    if(result.returnCode=='0'){
        tipModal("icon-checkmark", "添加成功");
        isPostBack=true;
        queryList(1);
    }else{
        tipModal("icon-cross", "添加失败");
    }
    $("#modalAlertLevel").modal("hide");
}

//---------------弹出设备故障通知查看详情列表
function detailDeviceFault(deviceFaultId){
    updateOrAdd=2;
    $(".modal-title").html("设备故障通知详情");
    $(".modal-footer").html("");
    deviceTy="";
    selectDeviceFirstList();
    faultTypeList();
    $("#modalAlertLevel").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    PostForm("/cwp/front/sh/deviceFault!execute", "uid=c004&id="+deviceFaultId, "",updateDeviceFaultResultArrived, '');
}

function updateDeviceFaultResultArrived(result){
    detailDeviceType(result.bean.deviceId);
    $("#alarmLevel").val(result.bean.alarmLevel);
    $("#faultType").val(result.bean.faultTypeId);
    document.getElementById("alarmLevel").disabled=true
    document.getElementById("faultType").disabled=true;
    //var kk = document.getElementById("alarmLevel").options;
    //for (var i=0; i<kk.length; i++) {
    //    if (kk[i].value==result.bean.alarmLevel) {
    //        //document.getElementById("alarmLevel").disabled=true
    //        kk[i].selected=true;
    //    }
    //}
    //var kk = document.getElementById("faultType").options;
    //for (var i=0; i<kk.length; i++) {
    //    if (kk[i].value==result.bean.faultTypeId) {
    //        //document.getElementById("faultType").disabled=true;
    //        kk[i].selected=true;
    //    }
    //}
    //var kk = document.getElementById("deviceTypeFirst");
    //for(var i=0;i<kk.length;i++){
    //    for (var j=0; j<kk.length; j++) {
    //        if (kk[j].value==result.bean.deviceId) {
    //            kk[j].selected=true;
    //            //document.getElementById("deviceTypeFirst").disabled=true;
    //            break;
    //        }
    //    }
    //}
}

function detailDeviceType(deviceTypeId){
    PostForm("/cwp/front/sh/deviceFault!execute", "uid=c005_3&dictdataValue="+deviceTypeId, "",detailDeviceListSecondResultArrived, '');
}

function detailDeviceListSecondResultArrived(result){
    var deviceTypeId="";

    deviceTypeId=result.beans[0].dictdataValue;
    document.getElementById("deviceTypeFirst").disabled=true;
    for(var i=0;i<result.beans.length;i++){
        var kk = document.getElementById("deviceTypeFirst").options;
        for (var j=0; j<kk.length; j++) {
            if (kk[j].value==result.beans[i].dictValue) {
                deviceTypeId=result.beans[i].dictdataValue;
                kk[j].selected=true;
                //document.getElementById("deviceTypeFirst").disabled=true;
                break;
            }
        }
    }
    var sAddDeviceId =$('#deviceTypeFirst').val();
    if(sAddDeviceId.length!=0){
        selectDeviceSecondList(sAddDeviceId);
        deviceTy=deviceTypeId;
    }
}
//-----------------------------------------------------




