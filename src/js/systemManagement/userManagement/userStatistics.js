﻿var echartColors;
var dateMonth;
$(function () {
    var loader = "<div class='loading' style='margin:0 auto;text-align:center; padding-top:20px;'><img src='/cwp/src/assets/img/loading/cmcc_loading_lg.gif' style='height:35px;width:35px;display:inline;' alt=''/><p style='font-size:12px;display:block;margin-top: 10px'>载入中，请稍候...</p></div>";
    //$("#faultEcharts").html(loader);
    //$("#securityEcharts").html(loader);
    //$("#orderEcharts").html(loader);
    //$("#line-chart").html(loader);
    dateMonth = getToday();
    echartColors = ["#ed7d31","#ffc000","#5b9bd5"];
    requestEventNum();
//    displayOrderEcharts();
//    displayEnergyEcharts();
});

function getToday(){
    var today = "";
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
    return today;
}

function requestEventNum(){
    // PostForm("/cwp/front/sh/bedapplyAnalysis!execute", "uid=bedapplyAnalysis016&applyTime=" + dateMonth, "", dormitoryEchart, '');
    // PostForm("/cwp/front/sh/device!execute", "uid=d017&deviceTypeId=1004&buildingId=4fe6a6ba-72bd-46ef-b15b-f13045737823", "", entranceEchart, '');
    // PostForm("/cwp/front/sh/cardConsumInfo!execute", "uid=cardConsumInfo001&applyTime=" + dateMonth, "", consumptionEchart, '');
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c028", "", buildingEchart, '');
    entranceEchart();
    dormitoryEchart();
    // consumptionEchart();
    // buildingEchart();
}

//宿舍入住
function dormitoryEchart(){
    var dEcharts = echarts.init(document.getElementById("dormitoryEcharts"));
    var option = {
        title : {
            text: '用户人数',
            subtext: '',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: []
        },
        series : [
            {
                name: '性别',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5335, name:'男'},
                    {value:2310, name:'女'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    // 为echarts对象加载数据
    dEcharts.setOption(option);
}

// 门禁监控
function entranceEchart(){
    var eEcharts = echarts.init(document.getElementById('entranceEcharts'));
    var option = {
        title : {
            text: '部门人数',
            subtext: '',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: []
        },
        series : [
            {
                name: '部门人数：',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:335, name:'工程部'},
                    {value:310, name:'运维部'},
                    {value:234, name:'行政部'},
                    {value:135, name:'后勤部'},
                    {value:1548, name:'监管部'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    eEcharts.setOption(option);
}

// 消费统计
function consumptionEchart(){
    var option;
    if(true){
        var dEcharts = echarts.init(document.getElementById('consumptionEcharts'));
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();
        var monthDays = new Date(year,month,0).getDate();

        var dataNum = [];
        // for (var i = 0; i < monthDays; i++) {
        //     if(i<result.beans.length){
        //         dataNum[i] = Math.abs(result.beans[i].currDayCount)
        //     }else{
        //         dataNum[i] = 0
        //     }
        // }

        for (var i = 0; i < monthDays; i++) {
            if(i<30){
                dataNum[i] = parseInt(Math.random()*30000) +25000
            }else{
                dataNum[i] = 0
            }
        }

        option = {
            tooltip : {                                                         //  气泡提示框
                trigger: 'axis'                                                //直角坐标系中的一个坐标轴，坐标轴可分为类目型、数值型或时间型
            },
            color:echartColors,                                                //设置图表颜色
            legend: {                                                          //图例，表述数据和图形的关联
                data:['消费总额']
            },
            calculable : true,
            xAxis : [                                                           //直角坐标系中的横轴，通常并默认为类目型
                {
                    type : 'category',
                    boundaryGap : false,
                    data : function (){                                        //加载横坐标数据
                        var list = [];
                        for (var i = 1; i <= 31; i++) {
                            list.push(year+'-'+month+'-' + i);
                        }
                        return list;
                    }()
                }
            ],
            yAxis : [                                                            // 直角坐标系中的纵轴，通常并默认为数值型
                {
                    name:'(元)',
                    type : 'value',
                    max : '100000',
                    axisLabel : {                                              //设置纵坐标的单位
                        formatter: '{value}'
                    }
                }
            ],
            series : [                                                         //数据系列，一个图表可能包含多个系列，每一个系列可能包含多个数据
                {
                    name:'消费总额',                                       //所表示的类型
                    type:'line',
                    smooth:true,                                       //图标类型
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: 'default'
                            }
                        }
                    },
                    data:dataNum
                }
            ]
        }
    }else{
        option = {
            legend: {
                data:[]
            },
            series : [
                {
                    name:'',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[]
                }
            ]
        };
    }
    // 为echarts对象加载数据
    dEcharts.setOption(option);
}

// 楼宇
function buildingEchart(result){
    var bEchart = echarts.init(document.getElementById('buildingEcharts'));
    var option= {
        color:echartColors,
        legend: {
            data:['正常44个','故障2个']
        },
        calculable : true,
        series : [
            {
                name:'',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {name:'正常40个',value:'40'},
                    {name:'故障2个',value:'2'}
                ]
            }
        ]
    };
    bEchart.setOption(option);
}