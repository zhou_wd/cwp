﻿
//点击触发上传控件
var userImportLogId="";
//初始化文件上传控件
$("#file_upload").uploadify({
    'formData'      : {'regexEnum' : JSON.stringify(regexEnum),userId:localStorage.getItem("parkId")},
    'swf'           : '../../../assets/lib/uploadify/uploadify.swf?ver='+Math.random(),
    'uploader'      : '/cwp/front/sh/user!execute?uid=u023&ver='+Math.random(),
    'height'        : 34,
    'width'         : 177,
    'fileTypeExts': '*.xls; *.xlsx', //文件后缀限制 默认：'*.*'
    'fileSizeLimit': '10240KB', //文件大小限制 0为无限制 默认KB
    'fileTypeDesc' : '.xls; *.xlsx文件格式',//文件类型描述
    'method': 'post', //提交方式Post 或Get 默认为Post
    'buttonText': '', //按钮文字
    'multi': false, //多文件上传
    'fileObjName': 'files[]', //设置一个名字，在服务器处理程序中根据该名字来取上传文件的数据。默认为Filedata
    'queueID': 'I am dont need',  //默认队列ID
    'removeCompleted': true, //上传成功后的文件，是否在队列中自动删除
    'overrideEvents': ['onDialogClose', 'onSelectError','onUploadStart', 'onUploadSuccess', 'onUploadError'],//重写事件
    //返回一个错误，选择文件的时候触发          
    'onSelectError': function (file, errorCode, errorMsg) {
        var msgText = "";
        switch (errorCode) {
            case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                //this.queueData.errorMsg = "每次最多上传 " + this.settings.queueSizeLimit + "个文件";
                msgText += "每次最多上传 " + this.settings.queueSizeLimit + "个文件";
                break;
            case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                msgText += "文件大小超过限制10M";
                break;
            case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                msgText += "文件大小为0";
                break;
            case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
                msgText += "文件格式不正确，仅限 " + this.settings.fileTypeExts;
                break;
            default:
                msgText += "错误代码：" + errorCode + "\n" + errorMsg;
        }
        tipModal('icon-cross',msgText);
    },
    // 当选中文件时的回调函数
    'onUploadStart': function (file) {
        //显示上传状态
        $("#tipword").hide();
        $("#uploadProcess").show();
    },

    //检测FLASH失败调用
    'onFallback': function () {
        alert("您未安装FLASH控件，无法上传图片！请安装FLASH控件后再试。");
        $("#tipword").show();
        $("#uploadProcess").hide();
    },

    //文件上传成功后的回掉方法
    'onUploadSuccess': function (file, data, response) {
        if(data == ""){
            tipModal('icon-cross','导入失败');
        }
        else{
            var data=JSON.parse(data)
            if(data.returnCode == '0'){
                tipModal('icon-checkmark','导入完成');
                //同步数据 正在与一卡通系统同步用户数据
                //同步用户数据

                userImportLogId=data.bean.userImportLogId;
                if(userImportLogId != ""){
                    $("#modalLoading").modal({//启用modal ID
                        keyboard: true,
                        backdrop: 'static'
                    }).on('show.bs.modal', centerModal());
                    //同步一卡通系统
                    PostForm("/cwp/front/sh/user!execute", "uid=importUser003&userImportLogId="+userImportLogId,"", postAyncResultArrived, '');
                    //同步门禁系统
                    var groupNames = getAccessArry();
                    if(groupNames!=""){
                        PostForm("/cwp/front/sh/ipg!execute", "uid=ipg003&userImportLogId="+userImportLogId+"&groupNames="+groupNames,"", accessAyncResultArrived, '');
                    }

                }
            }
            else if(data.returnCode == '-9999'){
                tipModal('icon-cross','导入失败');
            }
            else{
                tipModal('icon-cross',data.returnMessage);
            }
            $("#tipword").show();
            $("#uploadProcess").hide();
        }

    },

    //文件上传出错时触发，参数由服务端程序返回。
    'onUploadError': function (file, errorCode, errorMsg, errorString) {
        tipModal('icon-cross',"上传出现异常，请重新上传");
        $("#tipword").show();
        $("#uploadProcess").hide();
    }
});



//同步数据回调
function postAyncResultArrived(result){
    $("#modalLoading").modal("hide");
    if(result.returnCode == "0"){
        isFirst=true;
        detailSelect(userImportLogId);
        queryList(1);
    }
    else if(result.returnCode == "-9999"){
        tipModal('icon-cross','同步失败');
    }
    else{
        tipModal('icon-cross',result.returnMessage);
    }
}

//门禁同步回调
function accessAyncResultArrived(result){
    $("#modalLoading").modal("hide");
    if(result.returnCode == "0"){
        isFirst=true;
        detailSelect(userImportLogId);
        queryList(1);
    }
    else if(result.returnCode == "-9999"){
        tipModal('icon-cross','同步失败');
    }
    else{
        tipModal('icon-cross',result.returnMessage);
    }
}

//--------------------------------------------导入日志-------------------------------
$(function () {
    queryList(1);
    //获取门禁列表
    getAccessList();
});

//门禁权限选择
$(document).on("click",".access .t-tag-category", function () {
    if ($(this).hasClass("selected")) {
        $(this).removeClass("selected");
    }
    else {
        $(this).addClass("selected");
    }

    //var selected = $(".access .selected");
    //if(selected.length > 0){
    //    $(".upload-box").show();
    //}
    //else{
    //    $(".upload-box").hide();
    //}
});



//获取计划数据
function queryList(pageIndex){
    var loaderTab = new LoaderTableNew("importLogTab","/cwp/front/sh/user!execute","uid=importUser001",pageIndex,10,importLogResultArrived,"",importLogPagination,"frmSearch");
    loaderTab.Display();
}

function importLogResultArrived(result){
    if(result.returnCode ==0){
        template.helper('isSynchronizationFormat', function (inp) {
            if (inp == "0") {
                return '是';
            } else  {
                return '否';
            }
        });
        var outHtml = template("importLogDataTmpl", result);
        $("#importLogTab tbody").html(outHtml);
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}

//异常数据分页回调
function importLogPagination(page_index) {
    if (!isFirst) queryList(page_index + 1);
}

//获取异常数据
//详情查看异常数据
function detailSelect(id) {
    $("#hidUserImportLogId").val(id);
    isFirst=true;
    abnormalDataList(1);
}

function abnormalDataList(pageIndex){
    var userImportLogId=$("#hidUserImportLogId").val();
    var loaderAbnormalDataTab = new LoaderTableNew("abnormalData","/cwp/front/sh/user!execute","uid=importUser002&userImportLogId="+userImportLogId,pageIndex,10,abnormalDataResultArrived,"",abnormalDataPagination);
    loaderAbnormalDataTab.Display();
}
//渲染异常数据
function abnormalDataResultArrived(result){
    if(result.returnCode ==0){
        template.helper('cardAddStateFormat', function (inp) {
            if (inp == "0") {
                return '未同步';
            }else if(inp == "1") {
                return '失败';
            }else if(inp == "2") {
                return '同步成功';
            }
        });
        var outHtml = template("abnormalDataTmpl", result);
        $("#abnormalData tbody").html(outHtml);
        $("#abnormalDataModal").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal', centerModal());
        $(window).on('resize', centerModal());
    }else{
        tipModal("icon-cross", result.returnMessage);
    }
}

//列表数据分页回调
function abnormalDataPagination(page_index) {
    if (!isFirst) abnormalDataList(page_index + 1);
}


//获取门禁权限列表
function getAccessList(){
    PostForm("/cwp/front/sh/ipg!execute", "uid=ipg001", "", successResultAccessList, "");
}

function successResultAccessList(result){
    if(result.returnCode=="0"){
        if(result.object.length > 0 ){
            var outHtml = template("accessTmpl", result);
            $(".access").html(outHtml);
        }
        else{
            $(".access").html("<div class='tip'>无门禁组数据</div>");
        }
    }
    else{
        $(".access").html("<div class='tip'>无门禁组数据</div>");
    }
}


var getAccessArry = function(){
    var accessArry=[];
    $(".access .t-tag-category.selected").each(function(){
        var id = $(this).attr("data-id");
        accessArry.push(id);
    });
    return accessArry.toString();
}

