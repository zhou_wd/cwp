//初始化楼栋树

//树形节点点击添加
var countTreeId;
$(function(){
    initMenuTree();
});

//验证
$(document).ready(function () {
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });
    $("#id").formValidator({
        onfocus: "请输入菜单编号",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请输入菜单编号"
    });

    $("#sortNumber").formValidator({
        onfocus: "请输入排序编号",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请输入排序编号"
    });

    $("#description").formValidator({
        onfocus: "请输入菜单名称",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请输入菜单名称"
    });
    $("#ismenu").formValidator({
        onfocus: "请选择建筑类型",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请选择建筑类型",
        onerror: "请选择建筑类型"
    });
});

//获取菜单树数据
function initMenuTree(){
    PostForm("/cwp/front/sh/menu!execute", "uid=menu008", "", successResultMenu, '',"frmTree");
}
function successResultMenu(result){
    switch (result.returnCode) {
        case "0":
            $("#dataTreeTotal").text( "共"+ result.beans.length +"条记录");
            if(result.beans.length>0)
                constructTree(result.beans);
            break;
        case "1":
            tipModal("icon-cross", "菜单树获取失败");
            break;
        case "-9999":
            tipModal("icon-cross", "菜单树获取失败");
            break;
    }
}


//1、判断建筑类型为区域
//2、根据父ID归类
//3、
//重构树对象数据
function constructTree(data){
    // 增加排序父ID
    for(var i=0; i<data.length; i++){
        if(data[i].sortNumber.length == 4){
            data[i].sortNumberParent = '0';
        }else{
            data[i].sortNumberParent = String(data[i].sortNumber.slice(0,(data[i].sortNumber.length-2)));
        }
    }
    var zNodes=[];
    for(var i=0; i<data.length; i++){
        var obj = new Object();
        // obj.id=data[i].id;
        // obj.pId=data[i].parentId;
        // // obj.url=data[i].url;
        // obj.description=data[i].description;

        obj.id=data[i].sortNumber;
        obj.pId=data[i].sortNumberParent;
        obj.powerPId=data[i].parentId;
        // obj.url=data[i].url;
        obj.description=data[i].description;
        obj.powerId = data[i].id;
        switch (data[i].ismenu){
            case 1:
                obj.ismenu = "菜单";
                break;
            case 2:
                obj.ismenu = "按钮";
                break;
        }
        zNodes.push(obj);
    }
    var setting = {
        view: {
            showIcon: false,
            selectedMulti: false,
            addDiyDom:addDiyDom
        },
        data: {
            simpleData: {
                enable: true
            }
        },
    };
    $.fn.zTree.init($("#treeOrg"), setting, zNodes);
    var zTree = $.fn.zTree.getZTreeObj("treeOrg");
    var countTreeId = "treeOrg_2";
    if(countTreeId!=undefined && countTreeId!=""){
        var getTreeNode=zTree.getNodeByTId(countTreeId);
        //默认选中节点
        zTree.selectNode(getTreeNode);
        //默认展开节点
        zTree.expandNode(getTreeNode,true);
    }
    //添加表头
    var li_head=' <li class="head"><a><div class="diy">菜单名称</div><div class="diy">编号</div><div class="diy">排序号</div>' +
        '<div class="diy">操作</div></a></li>';
    var rows = $("#treeOrg").find('li');
    if(rows.length>0){
        rows.eq(0).before(li_head);
        $("#dataTreeData").remove();
    }else{
        $("#treeOrg").append(li_head);
        $("#treeOrg").append('<li><td style="text-align: center;line-height: 30px;" >无符合条件数据</li>')
    }
};

//新增保存
$("#btn_saveUser").on("click",function(){
    //判断表单验证是否通过
    if($.formValidator.PageIsValid()){
         var id=$("#editId").val();
         if(id==""){
         PostForm("/cwp/front/sh/menu!execute", "uid=menu006", "post", successResultSaveMenu, '',"frmMain");
         }else{
             PostForm("/cwp/front/sh/menu!execute", "uid=menu004", "post", successResultEditMenu, '',"frmMain");
         }
    }
});

//新增请求成功回调函数
function successResultSaveMenu(result) {
    switch (result.returnCode) {
        case "0":
            $("#modalDistrict").modal("hide");
            tipModal("icon-checkmark", "保存成功");
            initMenuTree();
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    }
}

//编辑菜单信息回调
function successResultEditMenu(result){
    switch (result.returnCode) {
        case "0":
            $("#modalDistrict").modal("hide");
            tipModal("icon-checkmark", "保存成功");
            initMenuTree();
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    }
}
//删除菜单
function beforeRemove(id) {
    $("#hidId").val(id.getAttribute("data-id"));
    $("#modalDelMenu").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    return false;
}

//删除节点
$("#btnDelete").on("click",function(){
    PostForm("/cwp/front/sh/menu!execute", "uid=menu005", "", delMenubyId, "","frmTree");
});
function delMenubyId(result){
    $("#modalDelMenu").modal("hide");
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "删除成功");
            initMenuTree();
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "删除失败");
            break;
    }
}
//编辑
function beforeEditName(id) {
    addMenuTreeModal();
    setTimeout(function() {
        $("#hidId").val(id.getAttribute("data-id"));
        PostForm("/cwp/front/sh/menu!execute", "uid=menu007", "", getMenuById, '',"frmTree");
    }, 0);
    return false;
}
//显示菜单详情
function getMenuById(result){
    modalDistrict();
    $("#modalDistrict .modal-title").html("编辑菜单");
    switch (result.returnCode) {
        case "0":
            $("#id")[0].readOnly=true;
            $("#parentId").val(result.object.parentId);
            $("#ismenu").val(result.object.ismenu);
            $("#id").val(result.object.id);
            $("#sortNumber").val(result.object.sortNumber);
            $("#editId").val(result.object.id);
            $("#description").val(result.object.description);
            $("#url").val(result.object.url);
            if(result.object.parentId != 0){
                // $("#parentText").val(result.object.parentName);
                $("#parentText").val(result.object.parentId);
            }else{
                $("#parentText").val("")
            }
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "获取详情失败");
            break;
    }
}


//打开模态框
function modalDistrict(){
    $(".tipArea div").html("").attr("class","");
    //$("#parentId").val("");
    $("#editId").val("");
    $("#id")[0].readOnly=false;
    $("#modalDistrict .modal-title").html("新增菜单");
    $("#id").val("");
    $("#sortNumber").val("");
    $("#parentId").val("");
    $("#parentText").val("");
    $("#description").val("");
    $("#url").val("");
    $("#ismenu").val("");
    $("#modalDistrict").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
}

//点击节点事件
function beforeClick(treeId, treeNode){
    var id=treeNode.id;
    $("#id").val(id);
    isFirst=true;
}
//新增
$('#btnAddMenu').on("click",function(){
    modalDistrict();
    addMenuTreeModal();
});


//自定义DOM节点
function addDiyDom(treeId, treeNode) {
    countTreeId = treeNode.tId;
    var spaceWidth = 15;
    var liObj = $("#" + treeNode.tId);
    var aObj = $("#" + treeNode.tId + "_a");
    var switchObj = $("#" + treeNode.tId + "_switch");
    var icoObj = $("#" + treeNode.tId + "_ico");
    var spanObj = $("#" + treeNode.tId + "_span");
    aObj.attr('title','');
    aObj.append('<div class="diy swich"></div>');
    var div = $(liObj).find('div').eq(0);
    switchObj.remove();
    spanObj.remove();
    icoObj.remove();
    div.append(switchObj);
    div.append(spanObj);
    $("#" + treeNode.tId + "_span").text(treeNode.description);
    var spaceStr = "<span style='height:1px;display: inline-block;width:" + (spaceWidth * treeNode.level) + "px'></span>";
    switchObj.before(spaceStr);
    var editStr = '';
    editStr += '<div class="diy">' + (treeNode.powerId )+ '</div>';
    editStr += '<div class="diy">' + (treeNode.id )+ '</div>';
    //editStr += '<div class="diy">' + (treeNode.url)+ '</div>';
    editStr += '<div class="diy"><a class="t-btn t-btn-sm t-btn-red t-btn-deal btnDelete tree-btnred" onclick="beforeRemove(this)" data-tid="'+ treeNode.tId +'"   data-id="'+ treeNode.powerId +'">删除</a><a class="t-btn t-btn-sm t-btn-blue t-btn-deal editDictData tree-btnblue" onclick="beforeEditName(this)"  data-tid="'+ treeNode.tId +'"   data-id="'+ treeNode.powerId +'" >编辑</a></div>';
    aObj.append(editStr);
    $("#" + treeNode.tId + "_a").on("click",function(){
        $("#parentId").val(treeNode.powerId);
    })
}
//新增初始化菜单数
function addMenuTreeModal(){
    PostForm("/cwp/front/sh/menu!execute", "uid=menu008", "", getMenuTreeModal, '',"frmTree");

}
//获取数据初始化菜单树
function getMenuTreeModal(result){
    for(var i=0; i<result.beans.length; i++){
        if(result.beans[i].sortNumber.length == 4){
            result.beans[i].sortNumberParent = '0';
        }else{
            result.beans[i].sortNumberParent = String(result.beans[i].sortNumber.slice(0,(result.beans[i].sortNumber.length-2)));
        }
    }
    var zNodes=[];
    for(var i=0; i<result.beans.length; i++){
        var obj = new Object();
        obj.id=result.beans[i].sortNumber;
        obj.pId=result.beans[i].parentId;
        obj.name=result.beans[i].description;
        obj.powerId=result.beans[i].id;
        // obj.sortNumber=result.beans[i].sortNumber;
        // obj.url=result.beans[i].url;
        zNodes.push(obj);
    }
    if(result.returnCode=="0"){
        //配置组织结构树
        var setting = {
            view: {
                dblClickExpand: false,
                showIcon: false,                 //隐藏节点图标
                selectedMulti: false             //设置是否允许同时选中多个节点
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                beforeClick: beforeClickSelect
            }
        };

        $.fn.zTree.init($("#treeOrgModal"), setting, zNodes);
        var zTree = $.fn.zTree.getZTreeObj("treeOrgModal");
        if(countTreeId!=undefined && countTreeId!=""){
            var getTreeNode=zTree.getNodeByTId(countTreeId);
            //默认选中节点
            zTree.selectNode(getTreeNode);
            //默认展开节点
            zTree.expandNode(getTreeNode,true);
        }
    }
}
//渲染新增部门树
function beforeClickSelect(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeOrgModal");
    // $("#parentText").val(treeNode.name);
    $("#parentText").val(treeNode.powerId);
    $("#parentText").blur();
    $("#parentId").val(treeNode.powerId);
    hideMenu();
    return false;
}

$("#parentText").on("focus",function(){
    showMenu();
});

//显示部门树
function showMenu() {
    var deptIdObj = $("#parentText");
    var deptIdOffset = $("#parentText").offset();
    $("#menuContent").slideDown("fast");
    $("#menuContent").css({left:deptIdOffset.left + "px", top:deptIdOffset.top + deptIdObj.outerHeight() + "px"}).slideDown("fast");
    $("body").bind("mousedown", onBodyDown);
}
//隐藏部门树
function hideMenu() {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);
}
function onBodyDown(event) {
    if (!(event.target.id == "parentText" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length>0)) {
        hideMenu();
    }
}
