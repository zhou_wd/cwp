﻿////绑定点击事件
//详情
$(".t-btn-details").on("click", function () {
    $("#modalDetailsLog").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    //定时关闭
});

$(function () {
    searchLogList(1);
});

//加载页面列表
function searchLogList(pageIndex) {

    $.getJSON('/cwp/src/json/system/systemLog/log_l001.json',function(data){
        var loaderUserTab = new LoaderTable("loaderLogTab","logList","/cwp/front/sh/log!execute","uid=l001",pageIndex,10,successResultArrived,"");
        // loaderUserTab.Display();
        loaderUserTab.loadSuccessed(data);
    });
}


//请求成功，执行回调函数拼接表格信息
function successResultArrived(result) {
    //拼接表头html
    var thead="<thead>"+
        "<tr>"+
        "<th>序号</th>"+
        "<th>请求描述</th>"+
        "<th>请求人</th>"+
        "<th>请求IP</th>"+
        "<th>请求时间</th>"+
        "<th>类型</th>"+
        //"<th>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html

    var tbodys = "<tbody>";
    for(var i=0;i<result.beans.length;i++){
        var log = result.beans[i];
        tbodys+="<tr>" +
            "<td>"+(i+1)+"</td>"+
            "<td>"+log.Description+"</td>"+
            "<td>"+log.username+"</td>"+
            "<td>"+log.RequestIp+"</td>"+
            "<td>"+log.createtime+"</td>"+
            "<td>"+(log.type=="0"?"正常":"异常")+"</td>"+
            "<td>" +
            //"<a class='t-btn t-btn-sm t-btn-blue t-btn-details'>详情</a>"
            "</td>" +
            "</tr>";
    }
    tbodys += "</tbody>";
    //显示表格html
    $("#logList").html(thead+tbodys);
}


////分页回调函数
//当前页，加载容器
function paginationCallback(page_index){
    if(!isPostBack) {
        searchLogList(page_index+1);
    }
}
