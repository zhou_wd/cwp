﻿$(document).ready(function () {
    agentValidtor();
});
//验证
function agentValidtor()
{
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });

    $("#agentName").formValidator({
        onfocus: "请输代理名称",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 10,
        onempty: "请输代理名称",
        onerror: "长为1-10个字符"
    });
//    $("#agentType").formValidator({
//        onfocus:"请选择代理类型",
//        oncorrect:"&nbsp;"
//    }).InputValidator({
//        min:1,
//        onempty:"请选择代理类型",
//        onerror: "请选择代理类型"
//    });
    $("#ip").formValidator({
        onfocus: "请输代理ip",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 15,
        onempty: "请输代理ip",
        onerror: "长为1-15个字符"
    });
    $("#num").formValidator({
    	onfocus: "请输代理编号",
    	oncorrect: " "
    }).InputValidator({
    	min: 1,
    	max: 5,
    	onempty: "请输代理编号",
    	onerror: "长为1-5个字符"
    }).RegexValidator({
        regexp: "intege",
        datatype: "enum",
        onerror: "只能为整数"
    });
    $("#conProtocol").formValidator({
        onfocus: "请输协议类型",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 15,
        onempty: "请输协议类型",
        onerror: "长为1-15个字符"
    });
    $("#contable").formValidator({
    	onfocus: "请输请求间隔",
    	oncorrect: " "
    }).InputValidator({
    	min: 1,
    	max: 15,
    	onempty: "请输请求间隔",
    	onerror: "长为1-15个字符"
    }).RegexValidator({
        regexp: "intege",
        datatype: "enum",
        onerror: "只能为整数"
    });
    $("#parkCode").formValidator({
    	onfocus: "请输园区编号",
    	oncorrect: " "
    }).InputValidator({
    	min: 1,
    	max: 5,
    	onempty: "请输园区编号",
    	onerror: "长为1-5个字符"
    });
}
function saveAgent()
{
	if ($.formValidator.PageIsValid()){
		var agentName = $('#agentName').val();
		var agentType = $('#agentType').val();
		var num = $('#num').val();
		var ip = $('#ip').val();
		var port = parseInt(num*100) + parseInt(agentType);
		var contable = $('#contable').val();
		var conProtocol = $('#conProtocol').val();
		var parkCode = $('#parkCode').val();
		var description = $('#description').val();
		var agentOnline = 0;
		
		PostForm("/cwp/front/sh/agent!execute", "uid=c003&agentName=" + agentName +
				"&agentType=" + agentType +
				"&num=" + num +
				"&ip=" + ip +
				"&port=" + port +
				"&contable=" + contable +
				"&conProtocol=" + conProtocol +
				"&parkCode=" + parkCode +
				"&agentOnline=" + agentOnline +
				"&description=" + description
				, "", saveAgentResultArrived, '');
	}else{
		
	}
}


function saveAgentResultArrived(result)
{
   switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "添加成功","agentManagement.html?menuId=4.7");
            break;
        case "2":
            tipModal("icon-cross", "添加失败","agentManagement.html?menuId=4.7");
            break;
        case "-9999":
            tipModal("icon-cross", "添加失败","agentManagement.html?menuId=4.7");
            break;
    }
}


