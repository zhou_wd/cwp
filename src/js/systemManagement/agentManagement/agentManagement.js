﻿$(function() {
    Agent.init()

});

var agentIdDel;
window.dataList = [];

var Agent = {
    init: function() {

        var self = this;
        this.initEventBind();
        this.getAgentByPage(1);


        // 修改 Agent 配置
        window.updateAgent = function(index) {

            var detail = window.dataList[index];

            detail.isModify = true

            var html = template('dialogTmp', detail);
            $('#dialog').find('.modal-content').html(html).end().modal('show');
            $('#agentType').val(detail.agentType);
            self.agentValidtor();
        };

        // 删除配置
        window.deleteAgent = function (id){
            // if(confirm("确定要删除Agent信息吗？")){
            // PostForm("/cwp/front/sh/agent!execute", "uid=c004&id=" + id
            //     , "", delAgentResultArrived, '');
            // }
            agentIdDel = id;
            $("#delList").modal({//启用modal ID
                keyboard: true,
                backdrop: 'static'
            }).on('show.bs.modal', centerModal());
            $(window).on('resize', centerModal());
            return false;
        };

        //当前页，加载容器
        window.paginationCallback = function (page_index){
            if(!isPostBack) {
                self.getAgentByPage(page_index+1);
            }
        }
    },
    initEventBind: function() {
        var self = this
        //搜索Agent列表
        $("#btnSearch").on("click",function(){
            isPostBack=true;
            self.getAgentByPage(1);
        });

        //删除节点
        $("#btnDelList").on("click",function(){
            // PostForm("/cwp/front/sh/agent!execute", "uid=c004&id=" + agentIdDel, "", self.delAgentResultArrived, '');
            $.getJSON('/cwp/src/json/system/agent/agent_c004.json',function(data){
                self.delAgentResultArrived(data);
            });
            $("#delList").modal('hide');
        });

        // 新增Agent 配置
        $('#newAgent').on('click', function() {

            var html = template('dialogTmp');
            $('#dialog').find('.modal-content').html(html).end().modal('show');
            self.agentValidtor();

        });

        // 新增 Agent 配置 确认
        $("body").on('click', '#addBtn', function() {
            self.addAgent();
        });

        // 修改 Agent 配置 确认
        $('body').on('click', '#modifyBtn', function() {

            self.editAgent();
        })

    },
    getAgentByPage: function(pageIndex) {
        var self = this

        var agentName=$('#agentName').val();
        if(agentName==null||agentName==undefined) {
            agentName="";
        }
        var num=$('#num').val();
        if(num==null||num==undefined) {
            num="";
        }

        $.getJSON('/cwp/src/json/system/agent/agent_c001.json',function(data){
            var loaderAgentTab = new LoaderTable("loaderAgentTab","agentTable","/cwp/front/sh/agent!execute","uid=c001&agentName="+agentName+"&num="+num,pageIndex, 10,self.agentListResultArrived,"");
            // loaderAgentTab.Display();
            loaderAgentTab.loadSuccessed(data);
        });
    },
    addAgent: function() {
        if ($.formValidator.PageIsValid()){
            var self = this;

            var agentName = $('#agentNameCheck').val();
            var agentType = $('#agentType').val();
            var num = $('#numCheck').val();
            var ip = $('#ip').val();
            var port = parseInt(num*100) + parseInt(agentType);
            var contable = $('#contable').val();
            var conProtocol = $('#conProtocol').val();
            var parkCode = $('#parkCode').val();
            var description = $('#description').val();
            var agentOnline = 0;

            // PostForm("/cwp/front/sh/agent!execute", "uid=c003&agentName=" + agentName +
            //     "&agentType=" + agentType +
            //     "&num=" + num +
            //     "&ip=" + ip +
            //     "&port=" + port +
            //     "&contable=" + contable +
            //     "&conProtocol=" + conProtocol +
            //     "&parkCode=" + parkCode +
            //     "&agentOnline=" + agentOnline +
            //     "&description=" + description
            //     , "", self.addAgentResultArrived, '');
            $.getJSON('/cwp/src/json/system/agent/agent_c003.json',function(data){
                self.addAgentResultArrived(data);
            });
        }
    },
    editAgent: function() {
        if ($.formValidator.PageIsValid()==true){

            var self = this;

            var id = $('#modifyId').val();
            var agentName = $('#agentNameCheck').val();
            var agentType = $('#agentType').val();
            var num = $('#numCheck').val();
            var ip = $('#ip').val();
            var port = parseInt(num*100) + parseInt(agentType);
            var contable = $('#contable').val();
            var conProtocol = $('#conProtocol').val();
            var parkCode = $('#parkCode').val();
            var description = $('#description').val();
            var agentOnline = 0;

            // PostForm("/cwp/front/sh/agent!execute", "uid=c002"+
            //     "&id=" + id +
            //     "&agentName=" + agentName +
            //     "&agentType=" + agentType +
            //     "&num=" + num +
            //     "&ip=" + ip +
            //     "&port=" + port +
            //     "&contable=" + contable +
            //     "&conProtocol=" + conProtocol +
            //     "&parkCode=" + parkCode +
            //     "&agentOnline=" + agentOnline +
            //     "&description=" + description
            //     , "", self.editAgentResultArrived, '');
            $.getJSON('/cwp/src/json/system/agent/agent_c002.json',function(data){
                self.editAgentResultArrived(data);
            });
        }
    },
    // 表单验证
    agentValidtor: function () {
        $.formValidator.initConfig({
            formid: "frmMain", onerror: function (msg) {
                return false;
            }, onsuccess: function () {
                return true;
            }
        });

        $("#agentNameCheck").formValidator({
            onfocus: "请输代理名称",
            oncorrect: " "
        }).InputValidator({
            min: 1,
            max: 10,
            onempty: "请输代理名称",
            onerror: "长为1-10个字符"
        });
//    $("#agentType").formValidator({
//        onfocus:"请选择代理类型",
//        oncorrect:"&nbsp;"
//    }).InputValidator({
//        min:1,
//        onempty:"请选择代理类型",
//        onerror: "请选择代理类型"
//    });
        $("#ip").formValidator({
            onfocus: "请输代理ip",
            oncorrect: " "
        }).InputValidator({
            min: 1,
            max: 15,
            onempty: "请输代理ip",
            onerror: "长为1-15个字符"
        });
        $('#numCheck').formValidator({
            onfocus: "请输代理编号",
            oncorrect: " "
        }).InputValidator({
            min: 1,
            max: 5,
            onempty: "请输代理编号",
            onerror: "长为1-5个字符"
        }).RegexValidator({
            regexp: "intege",
            datatype: "enum",
            onerror: "只能为整数"
        });
        $("#conProtocol").formValidator({
            onfocus: "请输协议类型",
            oncorrect: " "
        }).InputValidator({
            min: 1,
            max: 15,
            onempty: "请输协议类型",
            onerror: "长为1-15个字符"
        });
        $("#contable").formValidator({
            onfocus: "请输请求间隔",
            oncorrect: " "
        }).InputValidator({
            min: 1,
            max: 15,
            onempty: "请输请求间隔",
            onerror: "长为1-15个字符"
        }).RegexValidator({
            regexp: "intege",
            datatype: "enum",
            onerror: "只能为整数"
        });
        $("#parkCode").formValidator({
            onfocus: "请输园区编号",
            oncorrect: " "
        }).InputValidator({
            min: 1,
            max: 5,
            onempty: "请输园区编号",
            onerror: "长为1-5个字符"
        });
    },
    agentListResultArrived: function(result) {

        var thead= "<thead>"+
            " <tr>"+
            "<th>名称</th>"+
            "<th>类型</th>"+
            "<th>编号</th>"+
            "<th>IP</th>"+
            "<th>端口</th>"+
            "<th>支持协议</th>"+
            "<th>请求间隔</th>"+
            "<th>在线状态</th>"+
            "<th>园区编号</th>"+
            "<th>操作</th>"+
            "</tr>"+
            "</thead>";
        var tbodys = "<tbody>";
        for(var i=0;i<result.beans.length;i++){
            var agentOnline="";
            var agentType="";
            if(result.beans[i].agentOnline==1){
                agentOnline="在线";
            }else{
                agentOnline="离线";
            }
            if(result.beans[i].agentType==1){
                agentType="门禁";
            }else if(result.beans[i].agentType==2){
                agentType="楼宇";
            }else if(result.beans[i].agentType==3){
                agentType="视频监控";
            }else if(result.beans[i].agentType==4){
                agentType="能耗";
            }

            tbodys+="<tr title="+result.beans[i].description+"><td>"+result.beans[i].agentName+"</td>"+
                "<td>"+agentType+"</td>"+
                "<td>"+result.beans[i].num+"</td>"+
                "<td>"+result.beans[i].ip+"</td>"+
                "<td>"+result.beans[i].port+"</td>"+
                "<td>"+result.beans[i].conProtocol+"</td>"+
                "<td>"+result.beans[i].contable+"</td>"+
                "<td>"+agentOnline+"</td>"+
                "<td>"+result.beans[i].parkCode+"</td>"+
                "<td>"+
                "<a class='t-btn t-btn-sm t-btn-green t-btn-edit' onclick='updateAgent(" + i +")'>修改</a>"+
                "<a class='t-btn t-btn-sm t-btn-red t-btn-del' onclick='deleteAgent("+result.beans[i].id+")'>删除</a></td>"+
                "</tr>"
        }
        tbodys += "</tbody>";
        //显示表格html
        $("#agentTable").html(thead+tbodys);

        window.dataList = result.beans;

    },
    delAgentResultArrived: function (result) {
        switch (result.returnCode) {
            case "0":
                tipModal("icon-checkmark", "删除成功","agentManagement.html?menuId=5.2");
                break;
            case "2":
                tipModal("icon-cross", "删除失败","agentManagement.html?menuId=5.2");
                break;
            case "-9999":
                tipModal("icon-cross", "删除失败","agentManagement.html?menuId=5.2");
                break;
            default:
                break;
        }
    },
    addAgentResultArrived: function (result) {
        $('#dialog').modal('hide');

        switch (result.returnCode) {
            case "0":
                tipModal("icon-checkmark", "添加成功");

                window.isPostBack = true;
                Agent.getAgentByPage(1);
                break;
            case "2":
                tipModal("icon-cross", "添加失败");
                break;
            case "-9999":
                tipModal("icon-cross", "添加失败");
                break;
            default:
                break;
        }
    },
    editAgentResultArrived: function (result) {
        $('#dialog').modal('hide');

        switch (result.returnCode) {
            case "0":
                tipModal("icon-checkmark", "修改成功");
                window.isPostBack = true;
                Agent.getAgentByPage(1);
                break;
            case "2":

                tipModal("icon-cross", "修改失败");
                break;
            case "-9999":

                tipModal("icon-cross", "修改失败");
                break;
            default:
                break;
        }
    }

}