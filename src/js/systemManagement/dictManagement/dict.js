$('#dictDataTab').slimScroll({
    height: '400px', //可滚动区域高度
    size: '5px', //组件宽度
});

$(function(){
	searchDictList(1);
});

//查询
$("#btnSearchDict").on("click",function(){
    isPostBack=true;
    searchDictList(1);
});
//搜索列表
function searchDictList(pageIndex)
{
    var dictTabLoader = new LoaderTable("dictTabLoader","dictTab","/cwp/front/sh/dict!execute","uid=D003",pageIndex,10,dict,"","dictForm");
    dictTabLoader.Display();
}
function paginationCallback(page_index){
    if(!isPostBack) searchDictList(page_index+1);
}
//加载分类列表
function dict(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>分类标识</th>"+
        "<th>分类值</th>"+
        "<th>分类名称</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        tbodys += "<tr>" +
        	"<td>" +  result.beans[i].dictKey + "</td>" +
            "<td>" +  result.beans[i].dictValue + "</td>" +
            "<td>" +  result.beans[i].dictName + "</td>" +
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal details' data-dictValue="+result.beans[i].dictValue+" data-dictName="+result.beans[i].dictName+">详情</a><a class='t-btn t-btn-sm t-btn-red t-btn-deal dictdel' data-dictValue="+result.beans[i].dictValue+" >删除</a><a class='t-btn t-btn-sm t-btn-blue t-btn-deal addDictData' data-dictValue="+result.beans[i].dictValue+">添加字典</a></td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#dictTab").html(thead+tbodys);
}
//删除分类
$(document).on("click",".dictdel", function () {
        $("#modalDelDict").modal("show");
        $("#delDictValue").val($(this).attr('data-dictValue'));
});
$("#btnDeleteDict").on("click",function(){
    PostForm("/cwp/front/sh/dict!execute","uid=D004","",delectDict,"","delDictForm");
    $("#modalDelDict").modal("hide");
});
function delectDict(result){

    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", result.returnMessage);
            isPostBack=true;
            searchDictList(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", result.returnMessage);
            break;
    }

}

//新建分类
$("#btnDictNew").on("click", function () {
    //验证
	addDictValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalDictNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    $(".modal-title").text("新增分类");
    $("#dictKey").val("");
    $("#dictValue").val("");
    $("#dictName").val("");
});
//保存分类
$("#saveDict").on("click",function(){
	
    if ($.formValidator.PageIsValid()) {
        //保存
		$.ajax({
			type : "post",
			url : "/cwp/front/sh/dict!execute",
            beforeSend:function(requests){
                requests.setRequestHeader("platform", "pc");
            },
			async : false,
			dataType : "json",
			data : {
				uid : "D001",
				dictKey : $("#dictKey").val(),
				dictValue : $("#dictValue").val(),
				dictName : $("#dictName").val()
			},
			success : function(result) {
				if (result.returnCode == '0') {
					tipModal("icon-checkmark", result.returnMessage);
                    isPostBack=true;
                    searchDictList(1);
				} else {
					tipModal("icon-cross", result.returnMessage);
				}
			},
			error : function() {
				_self.removeAttr('disabled');
				_self.text('保存')
			}
		});
        $("#modalDictNew").modal("hide");

    }
});
// 新增分类验证
function addDictValidator(){
    $.formValidator.initConfig({
    	formid: "dictNewForm", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });

    $("#dictKey").formValidator({
        onfocus: "请输入分类标识",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类标识"
    }).RegexValidator({
        regexp: "username",
        datatype: "enum",
        onerror: "分类标识只能包含字母、数字或下划线"
    }).AjaxValidator({
        type: "get",//发送请求方式
        url: "/cwp/front/sh/dict!execute?uid=D005",//发送请求路径
        datatype: "json",
        success: function (data) {
            if (data.returnCode == "0") {
                return true;
            } else {
                return false;
            }
        },
        error: function () {
            alert("服务器没有返回数据，可能服务器忙，请重试");
        },
        onerror: "分类标识已存在",
        onwait: ""
    });
    $("#dictValue").formValidator({
        onfocus: "请输入分类值",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类值"
    }).AjaxValidator({
        type: "get",//发送请求方式
        url: "/cwp/front/sh/dict!execute?uid=D005",//发送请求路径
        datatype: "json",
        success: function (data) {
            if (data.returnCode == "0") {
                return true;
            } else {
                return false;
            }
        },
        error: function () {
            alert("服务器没有返回数据，可能服务器忙，请重试");
        },
        onerror: "分类值已存在",
        onwait: ""
    });
    $("#dictName").formValidator({
        onfocus: "请输入分类名称",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类名称"
    });
}

// 获取字典详情
$(document).on("click",".details", function () {
    var dictValue =$(this).attr('data-dictValue');
    var dictName =$(this).attr('data-dictName');
    $("#detailDictValue").val(dictValue);
    $("#dictDataModalLabel").text(dictName+"字典详情");
    PostForm("/cwp/front/sh/dict!execute","uid=D006_3","",getDictDataList,"","dictDataForm");
});
//加载字典列表
function getDictDataList(result){
	if(result.returnCode=="0"){
		//显示模态框
		$("#modalDetailsDictData").modal({
	        keyboard: true,
	        backdrop: 'static'
	    }).on('show.bs.modal');
		//加载数据
	    var thead="<thead>"+
	        "<tr>"+
	        "<th>字典标识</th>"+
	        "<th>字典值</th>"+
	        "<th>字典名称</th>"+
	        "<th>操作</th>"+
	        "</tr>"+
	        "</thead>";
	    var tbodys= "<tbody>";
	    for(var i=0;i<result.beans.length;i++) {
	            tbodys += "<tr>" +
	                "<td>" + result.beans[i].dictdataKey + "</td>" +
	                "<td>" + result.beans[i].dictdataValue + "</td>" +
	                "<td>" + result.beans[i].dictdataName + "</td>" +
	                "<td><a class='t-btn t-btn-sm t-btn-red t-btn-deal dictDatadel' data-dictDataId="+result.beans[i].id+" >删除</a><a class='t-btn t-btn-sm t-btn-blue t-btn-deal editDictData' data-dictDataId="+result.beans[i].id+" >编辑</a></td>" +
	                "</tr>"
	    }
	    tbodys+= "</tbody>";
	    $("#dictData").html(thead+tbodys);
	}else{
		tipModal("icon-cross", result.returnMessage);
	}
}

//新建字典
$(document).on("click",".addDictData", function () {
    //验证
	addDictDataValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalDictDataNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    $(".modal-title").text("新增字典");
    $("#dictdataId").val("");
    $("#dictValueExt").val($(this).attr('data-dictValue'));
    $("#dictdataKey").val("");
    $("#dictdataValue").val("");
    $("#dictdataName").val("");
});

//编辑字典
$(document).on("click",".editDictData", function () {
	var dictDataId =$(this).attr('data-dictDataId');
	$("#modalDetailsDictData").modal("hide");
	//验证
	addDictDataValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalDictDataNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    PostForm("/cwp/front/sh/dict!execute","uid=D010&dictDataId="+dictDataId,"",getDictData,"");
});
//获取字典详情
function getDictData(result){
	 $(".modal-title").text("编辑字典");
	 $("#dictdataId").val(result.bean.id);
	 $("#dictValueExt").val(result.bean.dictValue);
	 $("#dictdataKey").val(result.bean.dictdataKey);
	 $("#hiddictdataKey").val(result.bean.dictdataKey);
	 $("#dictdataValue").val(result.bean.dictdataValue);
	 $("#dictdataName").val(result.bean.dictdataName);
}
//字典验证
function addDictDataValidator(){
    $.formValidator.initConfig({
    	validatorGroup:"2", formid: "dictDataNewForm", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });
    $("#dictValueExt").formValidator({
    	validatorGroup:"2",
        onfocus: "请输入分类值",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类值"
    });
    $("#dictdataKey").formValidator({
    	validatorGroup:"2",
        onfocus: "请输入字典标识",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入字典标识"
    }).RegexValidator({
        regexp: "username",
        datatype: "enum",
        onerror: "字典标识只能包含字母、数字或下划线"
    }).FunctionValidator({
        fun:function(val,elem) {
            var val = $("#hiddictdataKey").val();
            var dictdataKey = $("#dictdataKey").val();
            if(dictdataKey != val){
                var htmlObj = $.ajax({
                    type: "post",
                    url: "/cwp/front/sh/dict!execute",
                    beforeSend:function(requests){
                        requests.setRequestHeader("platform", "pc");
                    },
                    async: false,
                    dataType: "json",
                    data: {
                        uid: "D011",
                        dictdataKey:dictdataKey
                    }
                });
                var result = JSON.parse(htmlObj.responseText);
                if (result.returnCode == "0") {
                    return true;
                } else {
                    return "字典标识已存在";
                }
            }
        }
    });
    $("#dictdataName").formValidator({
    	validatorGroup:"2",
        onfocus: "请输入字典名称",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入字典名称"
    });
}
//保存字典
$("#saveDictData").on("click",function(){
    if ($.formValidator.PageIsValid(2)) {
    	var id = $("#dictdataId").val();
    	if(id == ""){
	        //新增
			$.ajax({
				type : "post",
				url : "/cwp/front/sh/dict!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
				async : false,
				dataType : "json",
				data : {
					uid : "D007",
					dictValue : $("#dictValueExt").val(),
					dictdataKey : $("#dictdataKey").val(),
					dictdataValue : $("#dictdataValue").val(),
					dictdataName : $("#dictdataName").val()
				},
				success : function(result) {
					if (result.returnCode == '0') {
						tipModal("icon-checkmark", result.returnMessage);
					} else {
						tipModal("icon-cross", result.returnMessage);
					}
				},
				error : function() {
					_self.removeAttr('disabled');
					_self.text('保存')
				}
			});
	        $("#modalDictDataNew").modal("hide");
	    }else{
	        //更新
			$.ajax({
				type : "post",
				url : "/cwp/front/sh/dict!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
				async : false,
				dataType : "json",
				data : {
					uid : "D008",
					id : $("#dictdataId").val(),
					dictValue : $("#dictValueExt").val(),
					dictdataKey : $("#dictdataKey").val(),
					dictdataValue : $("#dictdataValue").val(),
					dictdataName : $("#dictdataName").val()
				},
				success : function(result) {
					if (result.returnCode == '0') {
						tipModal("icon-checkmark", result.returnMessage);
					} else {
						tipModal("icon-cross", result.returnMessage);
					}
				},
				error : function() {
					_self.removeAttr('disabled');
					_self.text('保存')
				}
			});
	        $("#modalDictDataNew").modal("hide");
	    }
	}
});

//删除字典
var line;
$(document).on("click",".dictDatadel", function () {
	line = this;
    $("#delDictDataId").val($(this).attr('data-dictDataId'));
    PostForm("/cwp/front/sh/dict!execute","uid=D009","",delectDictData,"","delDictDataForm");
});
function delectDictData(result){
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", result.returnMessage);
            removeLine(line);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", result.returnMessage);
            break;
    }
}
//删除table里的某行数据
function removeLine(object){
	var trObj = object.parentNode.parentNode;
	if(trObj.tagName.toLowerCase() == "tr"){
		trObj.parentNode.removeChild(trObj);
	}
}