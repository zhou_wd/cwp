$('#dictDataTab').slimScroll({
    height: '400px', //可滚动区域高度
    size: '5px', //组件宽度
});

$(function(){
    dictListOne(1);
});

//查询
$("#btnSearchDict").on("click",function(){
    isFirst=true;
    dictListOne(1);
});
//删除分类
$(document).on("click",".dictdel", function () {
    $("#modalDelDict").modal("show");
    $("#delDictValue").val($(this).attr('data-dictValue'));
});
$("#btnDeleteDict").on("click",function(){
    PostForm("/cwp/front/sh/dict!execute","uid=D004","",delectDict,"","delDictForm");
    $("#modalDelDict").modal("hide");
});
function delectDict(result){

    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", result.returnMessage);
            isFirst=true;
            dictListOne(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", result.returnMessage);
            break;
    }

}

//新建分类
$("#btnDictNew").on("click", function () {
    //验证
    addDictValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalDictNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    $(".modal-title").text("新增分类");
    $("#dictKey").val("");
    $("#dictValue").val("");
    $("#dictName").val("");
});
//保存分类
$("#saveDict").on("click",function(){

    if ($.formValidator.PageIsValid()) {
        //保存
        $.ajax({
            type : "post",
            url : "/cwp/front/sh/dict!execute",
            beforeSend:function(requests){
                requests.setRequestHeader("platform", "pc");
            },
            async : false,
            dataType : "json",
            data : {
                uid : "D001",
                dictKey : $("#dictKey").val(),
                dictValue : $("#dictValue").val(),
                dictName : $("#dictName").val()
            },
            success : function(result) {
                if (result.returnCode == '0') {
                    tipModal("icon-checkmark", result.returnMessage);
                    isFirst=true;
                    dictListOne(1);
                } else {
                    tipModal("icon-cross", result.returnMessage);
                }
            },
            error : function() {
                _self.removeAttr('disabled');
                _self.text('保存')
            }
        });
        $("#modalDictNew").modal("hide");

    }
});
// 新增分类验证
function addDictValidator(){
    $.formValidator.initConfig({
        formid: "dictNewForm", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });

    $("#dictKey").formValidator({
        onfocus: "请输入分类标识",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类标识"
    }).RegexValidator({
        regexp: "username",
        datatype: "enum",
        onerror: "分类标识只能包含字母、数字或下划线"
    }).AjaxValidator({
        type: "get",//发送请求方式
        url: "/cwp/front/sh/dict!execute?uid=D005",//发送请求路径
        datatype: "json",
        success: function (data) {
            if (data.returnCode == "0") {
                return true;
            } else {
                return false;
            }
        },
        error: function () {
            alert("服务器没有返回数据，可能服务器忙，请重试");
        },
        onerror: "分类标识已存在",
        onwait: ""
    });
    $("#dictValue").formValidator({
        onfocus: "请输入分类值",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类值"
    }).AjaxValidator({
        type: "get",//发送请求方式
        url: "/cwp/front/sh/dict!execute?uid=D005",//发送请求路径
        datatype: "json",
        success: function (data) {
            if (data.returnCode == "0") {
                return true;
            } else {
                return false;
            }
        },
        error: function () {
            alert("服务器没有返回数据，可能服务器忙，请重试");
        },
        onerror: "分类值已存在",
        onwait: ""
    });
    $("#dictName").formValidator({
        onfocus: "请输入分类名称",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类名称"
    });
}

//新建字典
$(document).on("click",".addDictData", function () {
    //验证
    addDictDataValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalDictDataNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    $(".modal-title").text("新增字典");
    $("#dictdataId").val("");
    $("#dictValueExt").val($(this).attr('data-dictValue'));
    $("#dictdataKey").val("");
    $("#dictdataValue").val("");
    $("#dictdataName").val("");
});

//编辑字典
$(document).on("click",".editDictData", function () {
    var dictDataId =$(this).attr('data-dictDataId');
    $("#modalDetailsDictData").modal("hide");
    //验证
    addDictDataValidator();
    $(".tipArea div").html("").attr("class","");

    PostForm("/cwp/front/sh/dict!execute","uid=D010&dictDataId="+dictDataId,"",getDictData,"");
});
//获取字典详情
function getDictData(result){
    $(".modal-title").text("编辑字典");
    $("#dictdataId").val(result.bean.id);
    $("#dictValueExt").val(result.bean.dictValue);
    $("#dictdataKey").val(result.bean.dictdataKey);
    $("#hiddictdataKey").val(result.bean.dictdataKey);
    $("#dictdataValue").val(result.bean.dictdataValue);
    $("#dictdataName").val(result.bean.dictdataName);

    $("#modalDictDataNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
}
//字典验证
function addDictDataValidator(){
    $.formValidator.initConfig({
        validatorGroup:"2", formid: "dictDataNewForm", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });
    $("#dictValueExt").formValidator({
        validatorGroup:"2",
        onfocus: "请输入分类值",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入分类值"
    });
    $("#dictdataKey").formValidator({
        validatorGroup:"2",
        onfocus: "请输入字典标识",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入字典标识"
    }).RegexValidator({
        regexp: "username",
        datatype: "enum",
        onerror: "字典标识只能包含字母、数字或下划线"
    }).FunctionValidator({
        fun:function(val,elem) {
            var hiddictdataKey = $("#hiddictdataKey").val();
            if(hiddictdataKey == val){
                return true;
            }
            else{
                var htmlObj = $.ajax({
                    type: "post",
                    url: "/cwp/front/sh/dict!execute",
                    beforeSend : function(requests) {
                        requests.setRequestHeader("platform", "pc");
                    },
                    async: false,
                    dataType: "json",
                    data: {
                        uid: "D011",
                        dictdataKey:val
                    }
                });
                var result = JSON.parse(htmlObj.responseText);
                if (result.returnCode == "0") {
                    return true;
                } else {
                    return "字典标识已存在";
                }
            }
        }
    });
    $("#dictdataName").formValidator({
        validatorGroup:"2",
        onfocus: "请输入字典名称",
        oncorrect: "&nbsp;"
    }).InputValidator({
        min: 1,
        onempty: "请输入字典名称"
    });
}
//保存字典
$("#saveDictData").on("click",function(){
    if ($.formValidator.PageIsValid(2)) {
        var id = $("#dictdataId").val();
        if(id == ""){
            //新增
            $.ajax({
                type : "post",
                url : "/cwp/front/sh/dict!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                async : false,
                dataType : "json",
                data : {
                    uid : "D007",
                    dictValue : $("#dictValueExt").val(),
                    dictdataKey : $("#dictdataKey").val(),
                    dictdataValue : $("#dictdataValue").val(),
                    dictdataName : $("#dictdataName").val()
                },
                success : function(result) {
                    if (result.returnCode == '0') {
                        dictListOne(1);
                        isFirst=true;
                        tipModal("icon-checkmark", result.returnMessage);
                    } else {
                        tipModal("icon-cross", result.returnMessage);
                    }
                },
                error : function() {
                    _self.removeAttr('disabled');
                    _self.text('保存')
                }
            });
            $("#modalDictDataNew").modal("hide");
        }else{
            //更新
            $.ajax({
                type : "post",
                url : "/cwp/front/sh/dict!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                async : false,
                dataType : "json",
                data : {
                    uid : "D008",
                    id : $("#dictdataId").val(),
                    dictValue : $("#dictValueExt").val(),
                    dictdataKey : $("#dictdataKey").val(),
                    dictdataValue : $("#dictdataValue").val(),
                    dictdataName : $("#dictdataName").val()
                },
                success : function(result) {
                    if (result.returnCode == '0') {
                        tipModal("icon-checkmark", result.returnMessage);
                        dictListOne(1);
                        isFirst=true;
                    } else {
                        tipModal("icon-cross", result.returnMessage);
                    }
                },
                error : function() {
                    _self.removeAttr('disabled');
                    _self.text('保存')
                }
            });
            $("#modalDictDataNew").modal("hide");
        }
    }
});

//删除字典
var line;
$(document).on("click",".dictDatadel", function () {
    line = this;
    $("#delDictDataId").val($(this).attr('data-dictDataId'));
    PostForm("/cwp/front/sh/dict!execute","uid=D009","",delectDictData,"","delDictDataForm");
});
function delectDictData(result){
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", result.returnMessage);
            isFirst=true;
            dictListOne(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", result.returnMessage);
            break;
    }
}
//删除table里的某行数据
function removeLine(object){
    var trObj = object.parentNode.parentNode;
    if(trObj.tagName.toLowerCase() == "tr"){
        trObj.parentNode.removeChild(trObj);
    }
}



var zTreeNodes;
// 获取一级菜单数据
function dictListOne(pageIndex) {
    if(isFirst)delTree();
    var loaderCanteenDataTab = new LoaderTableNew("dataTree","/cwp/front/sh/dict!execute","uid=D003",pageIndex,10,outputDictListOne,"",dictListOnePagination,"dictForm");
    loaderCanteenDataTab.Display();

}
//列表数据分页回调
function dictListOnePagination(page_index) {
    if (!isFirst) dictListOne(page_index + 1);
}
// 渲染一级菜单数据
function outputDictListOne(data){
    //初始化列表
    if(data.returnCode ==0){
        zTreeNodes=data.beans;
        outpintSetting($("#dataTree"), data.beans,1,"/cwp/front/sh/dict!execute","uid=D006_3");
        //添加表头
        var li_head=' <li class="head"><a><div class="diy">分类标识</div><div class="diy">分类值</div><div class="diy">分类名称</div>' +
            '<div class="diy">操作</div></a></li>';
        var rows = $("#dataTree").find('li');
        if(rows.length>0){
            rows.eq(0).before(li_head)
            $("#dataTreeData").remove();
        }else{
            $("#dataTree").append(li_head);
            $("#dataTree").append('<li><td style="text-align: center;line-height: 30px;" >无符合条件数据</li>')
        }
    }
    else{
        $("#dataTree").append(li_head);
        $("#dataTree").append('<tr><td style="text-align: center;line-height: 30px;" >无符合条件数据</td></tr>')
    }
}

