﻿//初始化楼栋树
$(function(){
    initBuildingTree();

    //getAllBuilding(1);
});

//验证
$(document).ready(function () {
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });
    $("#buildingType").formValidator({
        onfocus: "请选择建筑类型",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请选择建筑类型",
        onerror: "请选择建筑类型"
    });

    $("#num").formValidator({
        onfocus: "请输入排序编号",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 10,
        onempty: "请输入排序编号",
        onerror: "请输入排序编号"
    }).RegexValidator({
        regexp: "intege1",
        datatype: "enum",
        onerror: "排序编号只能为正整数"
    });

    $("#buildingDesc").formValidator({
        onfocus: "请输入名称描述",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        max: 20,
        onempty: "请输入名称描述",
        onerror: "不能为空，不得大于20个字符长度"
    });
});


//请求所有建筑结构数据
function initBuildingTree(){
    PostForm("/cwp/front/sh/intebuilding!execute", "uid=building004", "", successResultBuilding, '',"frmTree");
}

function successResultBuilding(result){
    switch (result.returnCode) {
        case "0":
            if(result.beans.length>0)
            constructTree(result.beans);
            break;
        case "1":
            tipModal("icon-cross", "建筑树获取失败");
            break;
        case "-9999":
            tipModal("icon-cross", "建筑树获取失败");
            break;
    }
}
//1、判断建筑类型为区域
//2、根据父ID归类
//3、

//重构树对象数据
function constructTree(data){
    var zNodes=[];
    //数据排序
    //data.sort(sortObj("num"));
    for(var i=0; i<data.length; i++){
        var obj = new Object();
        obj.id=data[i].buildingId;
        obj.pId=data[i].parentId;
        obj.name=data[i].buildingDesc+"-"+data[i].num;
        //if(i==0)
        //obj.open=true;
        //if(data[i].buildingId.length<4){
        //    obj.iconSkin="pIcon01";
        //}else{
        //    obj.iconSkin="icon-home2";
        //}
        zNodes.push(obj);
    }

    var setting = {
        view: {
            addHoverDom: addHoverDom,
            removeHoverDom: removeHoverDom,
            showIcon: false,
            selectedMulti: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        edit: {
            enable: true
        },
        callback: {
            beforeEditName: beforeEditName,   //编辑回调事件
            beforeRemove: beforeRemove,       //删除回调事件
            beforeClick:beforeClick
        }
    };
    $.fn.zTree.init($("#treeOrg"), setting, zNodes);
    var zTree = $.fn.zTree.getZTreeObj("treeOrg");
    zTree.setting.edit.removeTitle = "删除";
    zTree.setting.edit.renameTitle = "编辑";
    if(countTreeId!=undefined && countTreeId!=""){
        var getTreeNode=zTree.getNodeByTId(countTreeId);
        //默认选中节点
        zTree.selectNode(getTreeNode);
        //默认展开节点
        zTree.expandNode(getTreeNode,true);
    }
}




//树形节点点击添加
var countTreeId;
function addHoverDom(treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
        + "' title='新增' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    var btn = $("#addBtn_"+treeNode.tId);
    if (btn) btn.bind("click", function(){
        var zTree = $.fn.zTree.getZTreeObj("treeOrg");
        zTree.selectNode(treeNode);
        countTreeId=treeNode.tId;
        modalDistrict();
        $("#modalDistrict .modal-title").html("新增建筑");
        $("#parentId").val(treeNode.id);
        return false;
    });
}

//新增保存
$("#btn_saveUser").on("click",function(){
    //判断表单验证是否通过
    if($.formValidator.PageIsValid()){
        var buildingId=$("#hidBuildingId").val();
        if(buildingId==""){
            PostForm("/cwp/front/sh/intebuilding!execute", "uid=i006", "post", successResultSaveBuilding, '',"frmMain");
        }
        else{
            PostForm("/cwp/front/sh/intebuilding!execute", "uid=building002", "post", successResultEditBuilding, '',"frmMain");
        }

        //关闭模态框


    }
});

//新增请求成功回调函数
function successResultSaveBuilding(result) {
    switch (result.returnCode) {
        case "0":
            $("#modalDistrict").modal("hide");
            tipModal("icon-checkmark", "保存成功");
            initBuildingTree();
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    }
}

//编辑建筑信息回调
function successResultEditBuilding(result){
    switch (result.returnCode) {
        case "0":
            $("#modalDistrict").modal("hide");
            tipModal("icon-checkmark", "保存成功");
            initBuildingTree();
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    }
}

//移除
function removeHoverDom(treeId, treeNode) {
    $("#addBtn_"+treeNode.tId).unbind().remove();
}


var countTreeNode;
function beforeRemove(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeOrg");
    zTree.selectNode(treeNode);
    countTreeNode=treeNode;
    $("#buildingId").val(treeNode.id);
    $("#modalDelBuilding").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    return false;
}

//删除节点
$("#btnDelete").on("click",function(){
    PostForm("/cwp/front/sh/intebuilding!execute", "uid=building003", "", delBuildingbyId, "","frmTree");
});


function delBuildingbyId(result){
    $("#modalDelBuilding").modal("hide");
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "删除成功");
            initBuildingTree();
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "删除失败");
            break;
    }
}

//编辑
function beforeEditName(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeOrg");
    zTree.selectNode(treeNode);
    setTimeout(function() {
        $("#buildingId").val(treeNode.id);
        countTreeId=treeNode.tId;
        PostForm("/cwp/front/sh/intebuilding!execute", "uid=building001", "", getBuildingById, '',"frmTree");
    }, 0);
    return false;
}
//显示建筑详情
function getBuildingById(result){
    modalDistrict();
    $("#modalDistrict .modal-title").html("编辑建筑");
    $("#parentId").val(result.object.parentId);
    $("#hidBuildingId").val(result.object.buildingId);
    $("#buildingType").val(result.object.buildingType);
    $("#num").val(result.object.num);
    $("#buildingDesc").val(result.object.buildingDesc);
}


//打开模态框
function modalDistrict(){
    //$("#modalAddDistrict").html(modal);
    $(".tipArea div").html("").attr("class","");
    $("#parentId").val("");
    $("#hidBuildingId").val("");
    $("#buildingType").val("");
    $("#num").val("");
    $("#buildingDesc").val("");
    $("#modalDistrict").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
}

//点击节点事件
function beforeClick(treeId, treeNode){
    var buildingId=treeNode.id;
    $("#buildingId").val(buildingId);
    isFirst=true;
    //searchUserList(1);
}




function getAllBuilding(pageIndex){
    var districtTable = new LoaderTableNew("districtTable","/cwp/front/sh/intebuilding!execute","uid=i005",pageIndex,10,buildingResultArrived,"",paginationCallback);
    districtTable.Display();
}
//请求成功，执行回调函数拼接表格信息
function buildingResultArrived(result)
{
    var outHtml = template("dataTmpl", result);
    $("#districtTable tbody").html(outHtml);
}

//当前页，加载容器
function paginationCallback(page_index){
    if (!isFirst) getAllBuilding(page_index + 1);
}

//新增
$('#btnAddBuilding').on("click",function(){
    modalDistrict();
    $("#parentId").val(0);
});

//对象排序
function  sortObj(propertyName){
    return function(object1,object2){
        var value1 = object1[propertyName];
        var value2 = object2[propertyName];

        if(value1 < value2){
            return -1;
        }else if(value1 > value2){
            return 1;
        }else{
            return 0;
        }
    }
}
