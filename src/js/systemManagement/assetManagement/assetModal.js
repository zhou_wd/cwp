// 加载中提示
var loader = "<div class='loading' style='margin:0 auto;text-align:center; padding-top:20px;'><img src='/cwp/src/assets/img/loading/cmcc_loading_lg.gif' style='height:35px;width:35px;display:inline;' alt=''/><p style='font-size:12px;display:block;margin-top: 10px'>载入中，请稍候...</p></div>";

var vendorIds="";
var childrenInfo = [];

var numberChildren=0;

//获取建筑结构列表
function getDeptTreeModal(zNodes){
    if(zNodes!=null){
        //配置组织结构树
        var setting = {
            view: {
                dblClickExpand: false,
                showIcon: false,                 //隐藏节点图标
                selectedMulti: false             //设置是否允许同时选中多个节点
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                beforeClick: beforeClickSelect
            }
        };
        $.fn.zTree.init($("#treeOrgModal"), setting, zNodes);
    }
}

function getDeptTreeModal2(zNodes){
    if(zNodes!=null){
        //配置组织结构树
        var setting = {
            view: {
                dblClickExpand: false,
                showIcon: false,                 //隐藏节点图标
                selectedMulti: false             //设置是否允许同时选中多个节点
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                beforeClick: beforeClickSelect2
            }
        };
        $.fn.zTree.init($("#treeOrgModal2"), setting, zNodes);
    }
}

function beforeClickSelect(treeId, treeNode) {
    // var treeObj = $.fn.zTree.getZTreeObj("treeOrgModal");
    $("#buildingname").val(treeNode.name);
    $("#buildingname").blur();
    $("#hidRegionId").val(treeNode.id);
    hideMenu();
    return false;
}

function beforeClickSelect2(treeId, treeNode) {
    // var treeObj = $.fn.zTree.getZTreeObj("treeOrgModal2");
    $("#buildingname2").val(treeNode.name);
    $("#buildingname2").blur();
    $("#hidAssetTypeId").val(treeNode.id);
    hideMenu2();
    return false;
}


function getParentNodes(node,allNode){
    if(node!=null){
        allNode += ">"+node['text'];
        curNode = node.getParentNode();
        getParentNodes(curNode,allNode);
    }else{
        //根节点
        curLocation = allNode;
    }
}

$("#buildingname").on("focus",function(){
    showMenu();
});

$("#buildingname2").on("focus",function(){
    showMenu2();
});

function showMenu() {
    var deptIdObj = $("#buildingname");
    var deptIdOffset = $("#buildingname").offset();
    $("#buildingModal").css({left:deptIdOffset.left + "px", top:deptIdOffset.top + deptIdObj.outerHeight() + "px"}).slideDown("fast");
    $("body").bind("mousedown", onBodyDown);
}

function showMenu2() {
    var deptIdObj = $("#buildingname2");
    var deptIdOffset = $("#buildingname2").offset();
    $("#buildingModal2").css({left:deptIdOffset.left + "px", top:deptIdOffset.top + deptIdObj.outerHeight() + "px"}).slideDown("fast");
    $("body").bind("mousedown", onBodyDown2);
}

function hideMenu() {
    $("#buildingModal").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);
}

function hideMenu2() {
    $("#buildingModal2").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown2);
}


function onBodyDown(event) {
    if (!(event.target.id == "buildingname" || event.target.id == "buildingModal" || $(event.target).parents("#buildingModal").length>0)) {
        hideMenu();
    }
}

function onBodyDown2(event) {
    if (!(event.target.id == "buildingname2" || event.target.id == "buildingModal2" || $(event.target).parents("#buildingModal2").length>0)) {
        hideMenu2();
    }
}



//新增验证
function equipmentValidtor()
{
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });
    $("#assetName").formValidator({
        onfocus:"请输入资产名称",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请输入资产名称",
        onerror: "请输入资产名称"
    });
    
    $("#assetCode").formValidator({
        onfocus:"请输入资产编码",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请输入资产编码",
        onerror:"请输入资产编码"
    });
}

//新增 显示模态框
$("#btnAddEquipment").on("click",function(){
    modalDistrict();
    $("#hidAssetId").val("");
    $("#modalEquipment").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
});

//保存
$("#btnSave").on("click", function () {
    if ($.formValidator.PageIsValid()==true) {
        var hidAssetId = $("#hidAssetId").val();
        if (hidAssetId == "") {//新增
            PostForm("/cwp/front/sh/asset!execute", "uid=d009x", "post", addDeviceResultArrived, '', "frmMain");
        } else {
            PostForm("/cwp/front/sh/asset!execute", "uid=d011x", "post", editDeviceResultArrived, '', "frmMain");
        }
    }
});

//添加 保存回调
function addDeviceResultArrived(result){
    switch (result.returnCode) {
        case "0":
            $("#modalEquipment").modal("hide");
            tipModal("icon-checkmark", "保存成功");
            isFirst=true;
            getDeviceByPage(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
        default:
            break;
    }
}
function editDeviceResultArrived(result){
    switch (result.returnCode) {
        case "0":
            $("#modalEquipment").modal("hide");
            tipModal("icon-checkmark", "修改成功");
            isFirst=true;
            getDeviceByPage(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "修改失败");
            break;
        default:
            break;
    }
}


//获取设备详情
$(document).on("click",".btn-detail",function(){
    modalDistrict();
    var assetId = this.getAttribute("data-id");
    PostForm("/cwp/front/sh/asset!execute", "uid=d010x&assetId="+assetId,"",deviceByIdResultArrived, '');
});

//设备详情
function deviceByIdResultArrived(result){
    $("#modalEquipment .modal-title").html("设备详情");
    $('input,select',$('form[id="frmMain"]')).attr('disabled',true);
    $('#hidAssetId').val(result.object.assetId);
    $('#hidRegionId').val(result.object.regionId);
    $('#buildingname').val(result.object.regionName);
    $('#hidAssetTypeId').val(result.object.assetTypeId);
    $('#buildingname2').val(result.object.assetTypeName);
    $('#assetName').val(result.object.assetName);
    $('#assetCode').val(result.object.assetCode);
    $('#assetValuation').val(result.object.assetValuation);
    $('#assetBrand').val(result.object.assetBrand);
    $('#assetType').val(result.object.assetType);
    $('#assetLevel').val(result.object.assetLevel);
    $('#assetStatus').val(result.object.assetStatus);
    $('#assetAcceptDate').val(result.object.assetAcceptDate);
    $('#assetProCode').val(result.object.assetProCode);
    $('#assetLocation').val(result.object.assetLocation);
    $('#assetSupplier').val(result.object.assetSupplier);
    $('#assetStopDate').val(result.object.assetStopDate);
    $('#assetUnit').val(result.object.assetUnit);
    $('#assetUnitTel').val(result.object.assetUnitTel);
    $('#assetUnitContactor').val(result.object.assetUnitContactor);
    $('#assetFixContactor').val(result.object.assetFixContactor);
    $('#assetTecParam').val(result.object.assetTecParam);
    $('#assetRuntimeParam').val(result.object.assetRuntimeParam);
    $('#assetToolParam').val(result.object.assetToolParam);
    $('#assetPartsParam').val(result.object.assetPartsParam);
    $('#assetAssociationNo').val(result.object.assetAssociationNo);
    $('#assetMaintainUnit').val(result.object.assetMaintainUnit);
    $('#assetContent').val(result.object.assetContent);
    //add
    $('#originalCoding').val(result.object.originalCoding);
    $('#useYear').val(result.object.useYear);
    $('#dateProduction').val(result.object.dateProduction);
    $('#bimId').val(result.object.bimId);
    $('#externalContractNumber').val(result.object.externalContractNumber);
    $('#instrumentType').val(result.object.instrumentType);
    $('#instrumentTableNo').val(result.object.instrumentTableNo);
    $('#name').val(result.object.name);
    $('#parentInstrumentTableNo').val(result.object.parentInstrumentTableNo);
    $('#meterReadingType').val(result.object.meterReadingType);
    $('#rate').val(result.object.rate);
    $('#units').val(result.object.units);
    $('#upperLimit').val(result.object.upperLimit);
    $('#lowerLimit').val(result.object.lowerLimit);
    $('#instrumentCategory1').val(result.object.instrumentCategory1);
    $('#instrumentCategory2').val(result.object.instrumentCategory2);
    $('#instrumentCategory3').val(result.object.instrumentCategory3);
    $('#instrumentCategory4').val(result.object.instrumentCategory4);
    $('#instrumentCategory5').val(result.object.instrumentCategory5);
    $('#instrumentCategory6').val(result.object.instrumentCategory6);
    $('#instrumentGroup').val(result.object.instrumentGroup);
    $('#maxNumber').val(result.object.maxNumber);
    $('#monitoringProtocolType').val(result.object.monitoringProtocolType);
    $('#monitorChannel').val(result.object.monitorChannel);
    $('#monitoryPoint').val(result.object.monitoryPoint);
    $('#displayPriority').val(result.object.displayPriority);
    $('#materials').val(result.object.materials);
    $('#voltage').val(result.object.voltage);
    $('#electricity').val(result.object.electricity);
    $('#ratedPower').val(result.object.ratedPower);
    //addend
    //add2
    $('#interactionSpace').val(result.object.interactionSpace);
    $('#interactionAttribute').val(result.object.interactionAttribute);
    $('#remark').val(result.object.remark);
    //add2end
    $("#btnSave").hide();
    $("#modalEquipment").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());

}
//打开模态框
function modalDistrict(){
	$(".tipArea div").html("").attr("class","");
	$('input,select',$('form[id="frmMain"]')).attr('disabled',false);
	$('#hidAssetId').val("");
    $('#hidRegionId').val("");
    $('#buildingname').val("");
    $('#hidAssetTypeId').val("");
    $('#buildingname2').val("");
    $('#assetName').val("");
    $('#assetCode').val("");
    $('#assetValuation').val("");
    $('#assetBrand').val("");
    $('#assetType').val("");
    $('#assetLevel').val("");
    $('#assetStatus').val("");
    $('#assetAcceptDate').val("");
    $('#assetProCode').val("");
    $('#assetLocation').val("");
    $('#assetSupplier').val("");
    $('#assetStopDate').val("");
    $('#assetUnit').val("");
    $('#assetUnitTel').val("");
    $('#assetUnitContactor').val("");
    $('#assetFixContactor').val("");
    $('#assetTecParam').val("");
    $('#assetRuntimeParam').val("");
    $('#assetToolParam').val("");
    $('#assetPartsParam').val("");
    $('#assetAssociationNo').val("");
    $('#assetMaintainUnit').val("");
    $('#assetContent').val("");

    //add
    $('#originalCoding').val("");
    $('#useYear').val("");
    $('#dateProduction').val("");
    $('#bimId').val("");
    $('#externalContractNumber').val("");
    $('#instrumentType').val("");
    $('#instrumentTableNo').val("");
    $('#name').val("");
    $('#parentInstrumentTableNo').val("");
    $('#meterReadingType').val("");
    $('#rate').val("");
    $('#units').val("");
    $('#upperLimit').val("");
    $('#lowerLimit').val("");
    $('#instrumentCategory1').val("");
    $('#instrumentCategory2').val("");
    $('#instrumentCategory3').val("");
    $('#instrumentCategory4').val("");
    $('#instrumentCategory5').val("");
    $('#instrumentCategory6').val("");
    $('#instrumentGroup').val("");
    $('#maxNumber').val("");
    $('#monitoringProtocolType').val("");
    $('#monitorChannel').val("");
    $('#monitoryPoint').val("");
    $('#displayPriority').val("");
    $('#materials').val("");
    $('#voltage').val("");
    $('#electricity').val("");
    $('#ratedPower').val("");
    //addend
    //add2
    $('#interactionSpace').val("");
    $('#interactionAttribute').val("");
    $('#remark').val("");
    //add2end
    $("#btnSave").show();
}

//删除
function beforeRemove(obj) {
	$("#delId").val($(obj).attr("data-id"));
    $("#modalDelBuilding").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    return false;
}
$("#btnDelete").click(function (){
    var assetId = $("#delId").val();
    PostForm("/cwp/front/sh/asset!execute", "uid=building003x&assetId="+assetId, "", delBuildingbyId, "","frmTree");
});

function delBuildingbyId(result){
    $("#modalDelBuilding").modal("hide");
    $("#delId").val("");
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "删除成功");
            isFirst=true;
            getDeviceByPage(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "删除失败");
            break;
        default:
            break;
    }
}

//修改
$(document).on("click",".t-btn-edit",function(){
    modalDistrict();
    var assetId = this.getAttribute("data-id");
    PostForm("/cwp/front/sh/asset!execute", "uid=d010x&assetId="+assetId,"",deviceEdit, '');
});

function deviceEdit(result){
    $("#modalEquipment .modal-title").html("修改设备");
    $('#hidAssetId').val(result.object.assetId);
    $('#hidRegionId').val(result.object.regionId);
    $('#buildingname').val(result.object.regionName);
    $('#hidAssetTypeId').val(result.object.assetTypeId);
    $('#buildingname2').val(result.object.assetTypeName);
    $('#assetName').val(result.object.assetName);
    $('#assetCode').val(result.object.assetCode);
    $('#assetValuation').val(result.object.assetValuation);
    $('#assetBrand').val(result.object.assetBrand);
    $('#assetType').val(result.object.assetType);
    $('#assetLevel').val(result.object.assetLevel);
    $('#assetStatus').val(result.object.assetStatus);
    $('#assetAcceptDate').val(result.object.assetAcceptDate);
    $('#assetProCode').val(result.object.assetProCode);
    $('#assetLocation').val(result.object.assetLocation);
    $('#assetSupplier').val(result.object.assetSupplier);
    $('#assetStopDate').val(result.object.assetStopDate);
    $('#assetUnit').val(result.object.assetUnit);
    $('#assetUnitTel').val(result.object.assetUnitTel);
    $('#assetUnitContactor').val(result.object.assetUnitContactor);
    $('#assetFixContactor').val(result.object.assetFixContactor);
    $('#assetTecParam').val(result.object.assetTecParam);
    $('#assetRuntimeParam').val(result.object.assetRuntimeParam);
    $('#assetToolParam').val(result.object.assetToolParam);
    $('#assetPartsParam').val(result.object.assetPartsParam);
    $('#assetAssociationNo').val(result.object.assetAssociationNo);
    $('#assetMaintainUnit').val(result.object.assetMaintainUnit);
    $('#assetContent').val(result.object.assetContent);
    //add
    $('#originalCoding').val(result.object.originalCoding);
    $('#useYear').val(result.object.useYear);
    $('#dateProduction').val(result.object.dateProduction);
    $('#bimId').val(result.object.bimId);
    $('#externalContractNumber').val(result.object.externalContractNumber);
    $('#instrumentType').val(result.object.instrumentType);
    $('#instrumentTableNo').val(result.object.instrumentTableNo);
    $('#name').val(result.object.name);
    $('#parentInstrumentTableNo').val(result.object.parentInstrumentTableNo);
    $('#meterReadingType').val(result.object.meterReadingType);
    $('#rate').val(result.object.rate);
    $('#units').val(result.object.units);
    $('#upperLimit').val(result.object.upperLimit);
    $('#lowerLimit').val(result.object.lowerLimit);
    $('#instrumentCategory1').val(result.object.instrumentCategory1);
    $('#instrumentCategory2').val(result.object.instrumentCategory2);
    $('#instrumentCategory3').val(result.object.instrumentCategory3);
    $('#instrumentCategory4').val(result.object.instrumentCategory4);
    $('#instrumentCategory5').val(result.object.instrumentCategory5);
    $('#instrumentCategory6').val(result.object.instrumentCategory6);
    $('#instrumentGroup').val(result.object.instrumentGroup);
    $('#maxNumber').val(result.object.maxNumber);
    $('#monitoringProtocolType').val(result.object.monitoringProtocolType);
    $('#monitorChannel').val(result.object.monitorChannel);
    $('#monitoryPoint').val(result.object.monitoryPoint);
    $('#displayPriority').val(result.object.displayPriority);
    $('#materials').val(result.object.materials);
    $('#voltage').val(result.object.voltage);
    $('#electricity').val(result.object.electricity);
    $('#ratedPower').val(result.object.ratedPower);
    //addend
    //add2
    $('#interactionSpace').val(result.object.interactionSpace);
    $('#interactionAttribute').val(result.object.interactionAttribute);
    $('#remark').val(result.object.remark);
    //add2end
    $("#btnSave").show();

    $("#modalEquipment").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());

}

//删除提示框
$("#delDevice").click(function(){
    $("#modalDelDept").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
})
$("#btnDeleteDept").click(function(){
    $('#addChildrenList').find("ul").remove();
    $("#modalDelDept").modal("hide");
    $("#addChildren").addClass("hide");
})
$("#delBack").click(function(){
    $("#yesDevice")[0].checked = true;
})
$("#yesDevice").click(function(){
    $("#addChildren").removeClass("hide");
})