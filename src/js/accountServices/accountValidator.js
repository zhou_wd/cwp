﻿//验证信息
$(document).ready(function () {
    $.formValidator.initConfig({ formid: "frmMain", onerror: function (msg) { return false; }, onsuccess: function () { return true; } });
    $("#loginName").formValidator({
        onfocus: "请输入用户名", oncorrect: " "
    }).InputValidator({
        min: 2,
        max:11,
        onempty: "请输入用户名",
        onerror: "用户名不能为空"
    });
    $("#phone").formValidator({
        onfocus: "请输入手机号", oncorrect: " "
    }).InputValidator({
        min: 11,
        max:11,
        onempty: "请输入手机号",
        onerror: "手机号码应为11位数字，以13/14/15/17/18开头"
    }).RegexValidator({
        regexp: "tel",
        datatype: "enum",
        onerror: "手机号码应为11位数字，以13/14/15/17/18开头"
    });
    //$("#code").formValidator({ onfocus: "请输入短信验证码，长度为6位", oncorrect: " " }).InputValidator({ min: 6, max: 6, onerror: "请输入短信验证码", onempty: "请输入短信验证码"});

});
//核对数据信息
$("#btnCheckAccount").on("click", function () {
    if ($.formValidator.PageIsValid()) {
        PostForm("/cwp/front/sh/login!login", "uid=L002", "LOGIN", CheckPwdResultArrived, '');
    }
});
//发送账户信息回调函数
function CheckPwdResultArrived(result)
{
    switch (result.returnCode) {
        case "0":
            var user = result.object;
            localStorage.setItem("loginId",user.loginId);
            localStorage.setItem("loginName",user.loginName);
            localStorage.setItem("emloyeeNo",user.childen.emloyeeNo); //参数通过session传递至其他页面
            window.location.href="/cwp/src/static/accountService/forgotPassword/resetPassword.html";
            break;
        case "1":
            $("#codeTip").attr("class","onError").html(result.returnMessage);
            break;
        case "2":
            $("#codeTip").attr("class","onError").html(result.returnMessage);
            break;
        case "3":
            $("#phoneTip").attr("class","onError").html(result.returnMessage);
            break;
        case "4":
            $("#loginNameTip").attr("class","onError").html(result.returnMessage);
            break;
    };
}
