﻿$(document).ready(function () {
    var loginId = localStorage.getItem("loginId");
    var loginName = localStorage.getItem("loginName");
    var emloyeeNo = localStorage.getItem("emloyeeNo");
    $("#loginId").val(loginId);
    $("#userName").html(loginName + "(" + emloyeeNo + ")");
    sessionStorage.clear();
    $.formValidator.initConfig({ formid: "frmMain", onerror: function (msg) { return false; }, onsuccess: function () { return true; } });
    $("#loginPwd").formValidator({
        onfocus: "6-20位字符，建议由字母，数字和符号两种以上组合",
        oncorrect: " "
    }).InputValidator({
        min: 6,
        max: 20,
        onempty: "请输入密码",
        onerror: "密码长度只能在6-20位字符之间"
    }).RegexValidator({
        regexp: "password",
        datatype: "enum",
        onerror: "密码只能由英文、数字及标点符号组成"
    });
    $("#confirmPwd").formValidator({
        onfocus: "请再次输入密码",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请确认密码",
        onerror: "密码长度只能在6-20位字符之间"
    }).CompareValidator({
        desID: "loginPwd",
        operateor: "=",
        onerror: "两次输入密码不一致"
    });
});


//修改密码成功
$("#btnConfrim").on("click", function () {
    if (jQuery.formValidator.PageIsValid()) {
    PostForm("/cwp/front/sh/login!login", "uid=L003", "", '', '');
    window.location.href = "/cwp/src/static/accountService/forgotPassword/modifyTheSuccess.html";
    }
});

