﻿
/**
 * Created by Administrator on 2016/6/1.
 */
var contractType = "";
$(function(){
    var parkId = localStorage.getItem("parkId");
    PostForm("/cwp/front/sh/user!execute", "uid=u001_2&parkId=" + parkId, "", LoginResultArrived, '');
});
function getContractType(){
    PostForm("/cwp/front/sh/dict!execute", "uid=D012&dictKey=contract_type", "", successResultContractType, '');
}
//发送账户信息回调函数
function LoginResultArrived(result)
{
    switch (result.returnCode) {
        case "0":
            $("#trueName").html(result.object.trueName);
            if(result.object.sex == 0){
                $("#sex").html("男");
            }else{
                $("#sex").html("女");
            }
            $("#userName").html(result.object.userName);
            $("#phone").html(result.object.phone);
            $("#identityCard").html(result.object.identityCard);
            $("#email").html(result.object.email);
            $("#passWord").html(result.object.passWord);
            $("#companyId").html(result.object.companyId);
            //$("#roleName").html(result.object.roleName);
            if(result.object.isChinamobile == 0){
                $("#isChinamobile").html("是");
            }else{
                $("#isChinamobile").html("否");
            }
            $("#department").html(result.object.department);
            $("#rank").html(result.object.rank);
            $("#emloyeeNo").html(result.object.emloyeeNo);
            $("#creator").html(result.object.creator);
            $("#createdTime").html(result.object.createdTime);
            $("#lastModifier").html(result.object.lastModifier);
            $("#lastModifiedTime").html(result.object.lastModifiedTime);
            contractType = result.object.contractType;
            getContractType();
            break;
        case "1":
    };
}
//获取合同类型
function successResultContractType(result){
    if(result.returnCode=="0" && result.beans.length>0){
        for(var i=0; i<result.beans.length; i++){
            if(contractType == result.beans[i].dictdataKey){
                $("#contractType").html(result.beans[i].dictdataName);
            }
        }
    }

}