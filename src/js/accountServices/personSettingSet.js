﻿/**
 * Created by Administrator on 2016/6/1.
 */

$(function(){
    var parkId = localStorage.getItem("parkId");
    PostForm("/cwp/front/sh/user!execute", "uid=u001_3&parkId=" + parkId, "", loadingArrived, '');
    PostForm("/cwp/front/sh/dict!execute", "uid=D012&dictKey=contract_type", "", successResultContractType, '');
});

//发送账户信息回调函数
function loadingArrived(result)
{
    switch (result.returnCode) {
        case "0":
            $("#parkId").val(result.object.parkId);
            $("#trueName").val(result.object.trueName);
            if(result.object.sex == 0){
                $("#sexboy").attr("checked",true);
            }else{
                $("#sexgirl").attr("checked",true);
            }
            $("#userName").val(result.object.userName);
            $("#phone").val(result.object.phone);
            $("#identityCard").val(result.object.identityCard);
            $("#email").val(result.object.email);
            $("#passWord").val(result.object.passWord);
            $("#companyId").val(result.object.companyId);
            //$("#roleName").html(result.object.roleName);
            if(result.object.isChinamobile == 0){
                $("#yes").attr("checked",true);
            }else{
                $("#no").attr("checked",true);
            }
            $("#department").val(result.object.department);
            $("#rank").val(result.object.rank);
            $("#emloyeeNo").val(result.object.emloyeeNo);
            $("#creator").val(result.object.creator);
            $("#createdTime").val(result.object.createdTime);
            $("#lastModifier").val(result.object.lastModifier);
            $("#lastModifiedTime").val(result.object.lastModifiedTime);
            $("#contractType").val(result.object.contractType);
            break;
        case "1":
    };
}

//绑定点击事件
$("#btnSaveInfo").on("click", function () {
    var parkId = localStorage.getItem("parkId");
    var contractType = $("#contractType").val();
    PostForm("/cwp/front/sh/user!execute", "uid=u012&parkId=" + parkId+"&lastModifier="+parkId+"&contractType="+contractType, "LOGIN", saveArrived, 'frmMain');
});

function saveArrived(result)
{
    switch (result.returnCode) {
        case "0":
            $("#myTip").modal({
                keyboard: true
            }).on('show.bs.modal', centerModal());
            $(window).on('resize', centerModal());
            //定时关闭
            setTimeout(function () { $("#myTip").modal("hide") }, 2000);
            setTimeout(function () { window.location.href = "personSetting.html?menuId=5.0"; }, 1000);
            tipModal("icon-checkmark", "保存成功");
            break;
        case "1":
            tipModal("icon-cross", "保存失败");
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    };
}

//////绑定点击事件
$("#btnSaveInfo").on("click", function () {
    ////成功

    ////失败
    //tipModal("icon-cross", "保存失败");
    //提示
    //tipModal("icon-info2", "请输入用户名");

});
//获取合同类型
function successResultContractType(result){
    if(result.returnCode=="0" && result.beans.length>0){
        for(var i=0; i<result.beans.length; i++){
            option="<option value='"+result.beans[i].dictdataKey+"'>"+result.beans[i].dictdataName+"</option>"
            $("#contractType").append(option);
        }
    }

}