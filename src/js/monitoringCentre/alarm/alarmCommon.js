// 告警数据
var rightFloorArr = [
    {
        deviceName:'主页',
        deviceType:'0',
        sel:true
    },
    {
        deviceName:'视频监控',
        deviceType:'1010',
        bgColor:'#3FC899',
        content:[
            {
                title:'新园区',
                detail:[
                    {
                        type:'总量',
                        unit:'个',
                        code:'1-deviceCount',
                        count:'NA'
                    },
                    {
                        type:'故障',
                        unit:'个',
                        code:'1-FaultDeviceCount',
                        count:'NA'
                    }
                ]
            },
            {
                title:'老园区',
                detail:[
                    {
                        type:'总量',
                        unit:'个',
                        code:'0-deviceCount',
                        count:'NA'
                    },
                    {
                        type:'故障',
                        unit:'个',
                        code:'0-FaultDeviceCount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'照明设备',
        deviceType:'1005',
        bgColor:'#9273B4',
        content:[
            {
                title:'回路',
                detail:[
                    {
                        type:'个数',
                        unit:'个',
                        code:'null-deviceCount',
                        count:'NA'
                    }
                ]
            },
            {
                title:'故障',
                detail:[
                    {
                        type:'个数',
                        unit:'个',
                        code:'null-FaultDeviceCount',
                        count:'NA'
                    },
                    {
                        type:'处理中',
                        unit:'个',
                        code:'null-Foultcount',
                        count:'NA'
                    }
                ]
            },
            {
                title:'能耗',
                detail:[
                    {
                        type:'度数',
                        unit:'KW/H',
                        code:'null-eps',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'空调机组',
        deviceType:'1003185',
        bgColor:'#6494C7',
        content:[
            {
                title:'总量',
                detail:[
                    {
                        type:'个数',
                        unit:'个',
                        code:'null-deviceCount',
                        count:'NA'
                    }
                ]
            },
            {
                title:'能耗',
                detail:[
                    {
                        type:'度数',
                        unit:'KW/H',
                        // code:'null-eps',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'送排风机',
        deviceType:'1003186',
        bgColor:'#6494C7',
        content:[
            {
                title:'数量',
                detail:[
                    {
                        type:'个数',
                        unit:'台',
                        code:'null-deviceCount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'集水坑',
        deviceType:'1003187',
        bgColor:'#6494C7',
        content:[
            {
                title:'数量',
                detail:[
                    {
                        type:'故障',
                        unit:'个',
                        code:'null-FaultDeviceCount',
                        count:'NA'
                    },
                    {
                        type:'处理中',
                        unit:'个',
                        code:'null-Foultcount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'生活给水',
        deviceType:'1003189',
        bgColor:'#6494C7',
        content:[
            {
                title:'数量',
                detail:[
                    {
                        type:'故障',
                        unit:'个',
                        code:'null-FaultDeviceCount',
                        count:'NA'
                    },
                    {
                        type:'处理中',
                        unit:'个',
                        code:'null-Foultcount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'门禁',
        deviceType:'1004',
        bgColor:'#3FC899',
        content:[
            {
                title:'新园区',
                detail:[
                    {
                        type:'故障',
                        unit:'个',
                        code:'1-FaultDeviceCount',
                        count:'NA'
                    },
                    {
                        type:'处理中',
                        unit:'个',
                        code:'1-Foultcount',
                        count:'NA'
                    }
                ]
            },
            {
                title:'老园区',
                detail:[
                    {
                        type:'故障',
                        unit:'个',
                        code:'0-FaultDeviceCount',
                        count:'NA'
                    },
                    {
                        type:'处理中',
                        unit:'个',
                        code:'0-Foultcount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'能耗管理',
        deviceType:'1006',
        bgColor:'#3FC899',
        content:[
        ]
    },
    {
        deviceName:'电子巡更',
        deviceType:'ds001',
        tableCode:'wait',
        bgColor:'#9273B4',
        content:[
            {
                title:'新园区',
                detail:[
                    {
                        type:'点量',
                        unit:'个',
                        code:'1-deviceCount',
                        count:'NA'
                    }
                ]
            },
            {
                title:'老园区',
                detail:[
                    {
                        type:'点量',
                        unit:'个',
                        code:'0-deviceCount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'信息发布',
        deviceType:'xin001',
        tableCode:'wait',
        bgColor:'#9273B4',
        content:[
            {
                title:'新园区',
                detail:[
                    {
                        type:'数量',
                        unit:'个',
                        // code:'1-deviceCount',
                        code:'',
                        count:'NA'
                    }
                ]
            },
            {
                title:'老园区',
                detail:[
                    {
                        type:'数量',
                        unit:'个',
                        code:'0-deviceCount',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'停车场',
        deviceType:'11',
        tableCode:'wait',
        bgColor:'#9273B4',
        content:[
            {
                title:'每天',
                detail:[
                    {
                        type:'数量',
                        unit:'个',
                        code:'11-carsNum',
                        count:'NA'
                    }
                ]
            }
        ]
    },
    {
        deviceName:'动环',
        deviceType:'1003191',
        // tableCode:'special',
        page: 'ring.html',
        bgColor:'#9273B4',
        content:[
            {
                title:'未处理',
                detail:[
                    {
                        type:'数量',
                        unit:'个',
                        code:'ring-1',
                        count:'NA'
                    }
                ]
            },
            {
                title:'处理中',
                detail:[
                    {
                        type:'数量',
                        unit:'个',
                        code:'ring-2',
                        count:'NA'
                    }
                ]
            },
            {
                title:'已处理',
                detail:[
                    {
                        type:'数量',
                        unit:'个',
                        code:'ring-3',
                        count:'NA'
                    }
                ]
            }
        ]
    }
];

// 告警菜单组装
var alarmMenu = function(dataArr){
    var alarmModuleMenu = "<ul>";
    dataArr.forEach(function(value){
        alarmModuleMenu += "<li><a dataSel='" + (value.sel?value.sel:'0') + "' dataType='" + value.deviceType + "' tableCode='" + (value.tableCode?value.tableCode:'')
        // 特殊模块（动环等）
        if (value.tableCode == 'special') {
            alarmModuleMenu += "' page='" + value.page
        }
        alarmModuleMenu += "'>" + value.deviceName + "<span class='deviceLiIcon'></span></a></li>"
    });
    alarmModuleMenu += "</ul>";
    return alarmModuleMenu;
};
// 告警菜单渲染
var alarmMenuRender = function(dataArr){
    $('#left-menu-secondary .menu-list-secondary').html(alarmMenu(dataArr));
    //菜单栏滚动条
    $('#left-menu-secondary .menu-list-secondary').slimScroll({
        width: 'auto', //可滚动区域宽度
        height: '100%', //可滚动区域高度
        size: '5px' //组件宽度
    });
};

// 告警菜单
alarmMenuRender(rightFloorArr);

$(function() {
    // 菜单点击(通用)
    $(document).on("click","#left-menu-secondary a",function() {
        isPostBack=true;
        if (!$(this).hasClass('selected')) {
            var deviceType = $(this).attr("datatype");
            var tableCode = $(this).attr("tableCode");

            // 待开发模块
            if (tableCode == "wait" || deviceType == "") {
                return tipModal("icon-cross", "设备安装调试中");
            }

            $("#left-menu-secondary li").removeClass("selected");
            $("#left-menu-secondary a").find('span.deviceLiIcon').removeClass('icon-circle-right');
            $(this).parents("li").addClass("selected");
            $(this).find('span.deviceLiIcon').addClass('icon-circle-right');
        }
    })

    setTimeout(function () {
        // 模块跳转
        var secQuery = location.search.split('&')[1];
        if (secQuery) {
            var index = secQuery.split('=')[1]

            $('.menu-list-secondary li:eq('+ index +') a').trigger('click');
        }
    }, 0);

});