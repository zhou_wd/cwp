﻿/**
 * Created by again on 2016/5/23.
 */
//登录验证
$("#btn-login").on("click",function(){
    login();
});


function login(){
    var userName = $("#login_name").val();
    var password=$("#txt_login_pwd").val();
    var loginCode=$("#loginCode").val();
    if (userName == "请输入用户名"||userName=="") {
        showTipBox('tip_error', 'down_arrow', '请输入用户名', 'login_name');
        return;
    }
    if (password == "请输入密码" || password=="") {
        showTipBox('tip_error', "down_arrow", '请输入密码', 'login_name');
        return;
    }
    if (loginCode == "请输验证码" || loginCode=="") {
        showTipBox('tip_error', "down_arrow", '请输验证码', 'login_name');
        return;
    }
    /*
     * post提交数据
     * 发送地址，uid，action行为，成功回调函数，失败回调函数
     * */
    // $("#login_pwd").val(Encrypt(password));
    // PostForm("/cwp/front/sh/login!login", "uid=L001", "post", LoginResultArrived, '');
    // $.getJSON('/cwp/src/json/login_L001.json',function(result){
    //     LoginResultArrived(result);
    // })
    //登录按钮加载
    $("#btn-login")[0].disabled=true;
    $("#btn-login").addClass("login-btn-disabled").text("登录中...");
    //if(localStorage.getItem("memorypassword") == "true"){
    //    localStorage.setItem("userName",userName);
    //    localStorage.setItem("password",password);
    //}else{
    //    localStorage.removeItem("userName");//清除userName的值
    //    localStorage.removeItem("password");//清除password的值
    //}

    $("#btn-login")[0].disabled=false;
    $("#btn-login").removeClass("login-btn-disabled").text("登录");

    $.getJSON('/cwp/src/json/menu.json', function(data) {
        // console.log(data);
        // initMenu(data.beans);
        LoginResultArrived(data);
        // alert(data);
        // window.location.href = "/cwp/index.html";
    });

    // 
}

//加密
function Encrypt(word){
    var key = CryptoJS.enc.Utf8.parse("wp.onlinecmcc.cn");
    var srcs = CryptoJS.enc.Utf8.parse(word);
    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
    return encrypted.toString();
}


//焦点文本框隐藏提示框
$("#login_name").on("focus", function () {
    hideTipBox();
});

$("#login_pwd").on("focus", function () {
    hideTipBox();
});

//页面加载时判断是否需要进行记住密码
//$(function(){
//    //读取记住密码
//    //if(localStorage.getItem("memorypassword") == "true"){
//    //    $("#login_name").val(localStorage.getItem("userName"));
//    //    $("#login_pwd").val(localStorage.getItem("password"));
//    //    $("#chkSaveLoginState").attr("checked", true);
//    //}else {
//    //    $("#chkSaveLoginState").attr("checked", false);
//    //}
//
//});
//
//$("#chkSaveLoginState").change(function(){
//    if($("#chkSaveLoginState").is(':checked')){
//        var userName = $("#login_name").val();
//        var password=$("#login_pwd").val();
//        localStorage.setItem("userName",userName);
//        localStorage.setItem("password",password);
//        localStorage.setItem("memorypassword","true");
//    }else{
//        localStorage.removeItem("userName");//清除userName的值
//        localStorage.removeItem("password");//清除password的值
//        localStorage.setItem("memorypassword","false");
//    }
//});

//回车登录

$("#login_pwd").keypress(function () {
    if (event.keyCode == 13) login();
});

$("#loginCode").keypress(function () {
    if (event.keyCode == 13) login();
});

//点击更换验证码
$("#changeCode").on("click",function(){
    $("#login-code-img").attr("src","http://120.194.44.254:20888/cwp/servlet/captchaCode?rid=" + Math.random());
});


//发送账户信息回调函数
function LoginResultArrived(result)
{
    if(result.returnCode==undefined)
    {
        showTipBox('tip_error', 'down_arrow', "用户名或密码错误", 'login_name');
    }
    switch (result.returnCode) {
        case "0":
            if(result.beans.length>0){
                localStorage.setItem("username", result.object.loginName);
                localStorage.setItem("parkId",result.object.userId);
                var roles = $.makeArray(result.object.role);
                localStorage.setItem("roles",JSON.stringify(roles));
                localStorage.setItem("trueName",result.object.realName);
                localStorage.setItem("userInfo",JSON.stringify(result.object));
                initMenu(result.beans);
            }else{
                showTipBox('tip_error', 'down_arrow', "该用户无权限登录智慧园区平台", 'login_name');
            }
            break;
        case "1":
            showTipBox('tip_error', 'down_arrow', "用户名或密码错误", 'login_name');
            break;
        case "2":
            showTipBox('tip_error', 'down_arrow', "此账号被冻结", 'login_name');
            break;
        case "3":
            showTipBox('tip_error', 'down_arrow', "验证码错误", 'login_name');
            break;
        case "4":
            showTipBox('tip_error', 'down_arrow', "登录失败次数过多账号已锁定", 'login_name');
            break;
        case "5":
            showTipBox('tip_error', 'down_arrow', "该用户无权限登录智慧园区平台", 'login_name');
            break;
        case "-9999":
            showTipBox('tip_error', 'down_arrow', "登录失败", 'login_name');
            break;
    };
    $("#login-code-img").attr("src","http://120.194.44.254:20888/cwp/servlet/captchaCode?rid=" + Math.random());
    $("#btn-login")[0].disabled=false;
    $("#btn-login").removeClass("login-btn-disabled").text("登录");
}

//初始化菜单字符串
function initMenu(menuArray){
    var menuHtml="";
    //生成二维数组
    var pid="";
    if(menuArray.length>0){

        // 添加排序父ID
        for(var i=0; i<menuArray.length; i++){
            if(menuArray[i].sortNumber.length == 4){
                menuArray[i].sortNumberParent = '0';
            }else{
                menuArray[i].sortNumberParent = String(menuArray[i].sortNumber.slice(0,(menuArray[i].sortNumber.length-2)));
            }
        }

        for(var i=0; i<menuArray.length; i++){
            //一级菜单
            if(menuArray[i].sortNumberParent=="0"){
                menuHtml+="<dl>";

                switch(menuArray[i].sortNumber)
                {
                    case "1001":
                        menuHtml+="<dt class = 'indexLeftListBtn'><a href="+(menuArray[i].url==""?'javascript:void(0);':menuArray[i].url)+"><b class='icon-home2'></b><i class='title'>"+menuArray[i].description+"</i></a></dt>";

                        break;
                    case "1002":
                        menuHtml+="<dt><a href='javascript:void(0);'><b class='icon-equalizer'></b><i class='title'>"+menuArray[i].description+"</i><span class='icon-circle-left'></span></a></dt>";

                        break;
                    case "1003":
                        menuHtml+="<dt><a href='javascript:void(0);'><b class='icon-office'></b><i class='title'>"+menuArray[i].description+"</i><span class='icon-circle-left'></span></a></dt>";

                        break;
                    case "1004":
                        menuHtml+="<dt><a href='javascript:void(0);'><b class='icon-article'></b><i class='title'>"+menuArray[i].description+"</i><span class='icon-circle-left'></span></a></dt>";

                        break;
                    case "1005":
                        menuHtml+="<dt><a href='javascript:void(0);'><b class='icon-man'></b><i class='title'>"+menuArray[i].description+"</i><span class='icon-circle-left'></span></a></dt>";

                        break;
                    case "1006":
                        menuHtml+="<dt><a href='javascript:void(0);'><b class='icon-stats-dots'></b><i class='title'>"+menuArray[i].description+"</i><span class='icon-circle-left'></span></a></dt>";

                        break;
                    default:
                        menuHtml+="<dt><a href='javascript:void(0);'><b class='icon-list'></b><i class='title'>"+menuArray[i].description+"</i><span class='icon-circle-left'></span></a></dt>";

                }

                pid=menuArray[i].sortNumber;
                menuHtml+="<div class=dd-list><div class=div-c>";
                for(var j=0; j<menuArray.length; j++){
                    //二级菜单
                    if(pid==menuArray[j].sortNumberParent){
                        var sid = menuArray[j].sortNumber;
                        if(sid == "100304" || sid == "100305" || sid == "100201" || sid == "100403" || sid == "100401"){
                            menuHtml+="<dd><a href="+(menuArray[j].url==""?'javascript:void(0);':menuArray[j].url)+">"+menuArray[j].description+"<span class='icon-circle-left'></span></a><ul>";
                        }
                        else{
                            menuHtml+="<dd><a href="+(menuArray[j].url==""?'javascript:void(0);':menuArray[j].url)+">"+menuArray[j].description+"</a>";
                        }
                        for(var k=0; k<menuArray.length; k++){
                            //三级菜单
                            if((sid == "100304" && menuArray[k].sortNumberParent == "100304")||(sid == "100305" && menuArray[k].sortNumberParent == "100305")||(sid == "100201" && menuArray[k].sortNumberParent == "100201")||(sid == "100401" && menuArray[k].sortNumberParent == "100401")||(sid == "100403" && menuArray[k].sortNumberParent == "100403")){
                                menuHtml+="<li><a href="+(menuArray[k].url==""?'javascript:void(0);':menuArray[k].url)+">"+menuArray[k].description+"</a></li>";
                            }
                        }

                        // if(sid== "100304" || sid == "100305"){
                        //     menuHtml+="</ul>";
                        // }

                        menuHtml+="</dd>";
                    }
                }
                menuHtml+="</div></div></dl>";
            }
        }
    }

    localStorage.setItem("menuHtml",menuHtml);
    window.location.href = "/cwp/index.html?menuId=0.0";
}

