﻿/**
 * 
 */
var currentTap="mytodo";

$(function(){
	// 默认加载Todo
	myTodoList(1);
    //加载告警类别下拉框
    //processTypeList();
	
	$("#myTabs li").on("click",function(){
        isPostBack = true;
		var oper=$(this).data("id");
		if(currentTap!=oper){
			resetOptions();
			switch(oper){
			case "mytodo":
				currentTap="mytodo";
				myTodoList(1);
				break;
			case "myaudit":
				currentTap="myaudit";
				myAuditList(1);
				break;
			case "ccTome":
				currentTap="ccTome";
				break;
            case 'myDraft':
                currentTap='myDraft';
                myDraftList(1);
                break;
			default:

			}
		}

	});
	
	//故障告警查询点击事件
	$("#btnSearchFaultAlarm").on("click",function(){
		switch(currentTap){
		case "mytodo":
			myTodoList(1);
			break;
		case "myaudit":
			myAuditList(1);
			break;
		case "ccTome":

			break;
        case "myDraft":
            myDraftList(1);
            break;
		default:

		}
	});
	

	
});

function resetOptions(){
	$("#startTime").val("");
	$("#endTime").val("");
	$("#processType").val("");
    var optionActivit = $("<option>").text("请选择").val("");
    $("#activityIdSelect").empty().append(optionActivit);
}

function traceWorkFlow(){
    $("#processWorkFlowTab").find('.trace').click(function(){
        $('#centerCheck').hide();
        $('#beizhushuoming').hide();
        var options={
            warningEventId:$(this).attr("eid"),
            orderId:$(this).attr("oid"),
            taskId:$(this).attr("taskId"),
            processInstId:$(this).attr("processInstId"),
            processDefId:$(this).attr("processDefId")
        }
        graphTrace(options);
    });
    $('#hasCheckTab').find('.trace').click(function () {
        $('#centerCheck').hide();
        $('#beizhushuoming').hide();
        var options={
            warningEventId:$(this).attr("eid"),
            orderId:$(this).attr("oid"),
            taskId:$(this).attr("taskId"),
            processInstId:$(this).attr("processInstId"),
            processDefId:$(this).attr("processDefId")
        };
        graphTrace(options);
    });
    $("#myDraftTab").find('.trace').click(function(){
        $('#centerCheck').hide();
        $('#beizhushuoming').hide();
        var options={
            warningEventId:$(this).attr("eid"),
            orderId:$(this).attr("oid"),
            taskId:$(this).attr("taskId"),
            processInstId:$(this).attr("processInstId"),
            processDefId:$(this).attr("processDefId")
        }
        graphTrace(options);
    });
    $("#processWorkFlowTab").find('.operAction').click(function(){
        $('#centerCheck').show();
        $('#beizhushuoming').show();
        var options={
            warningEventId:$(this).attr("eid"),
            orderId:$(this).attr("oid"),
            taskId:$(this).attr("taskId"),
            processInstId:$(this).attr("processInstId"),
            processDefId:$(this).attr("processDefId"),
            activityId:$(this).attr("activiti-id"),
            businessKey:$(this).attr("oid")
        };
        options.activityId == 'maketask'?$('#invalidDisplayNone').hide():$('#invalidDisplayNone').show();

        graphTrace(options);
    })
}

////故障告警
function myAuditList(pageIndex){
    var processType=$("#processType").val();
    if(processType==null){
    	processType="";
    }
    
    var activityIdSelect=$("#activityIdSelect").val();
    if(activityIdSelect==null){
    	activityIdSelect="";
    }
    
    var startTime=$("#startTime").val();
    var endTime=$("#endTime").val();
    
    if("undefined" == typeof startTime){
        startTime="";
    }
    if("undefined" == typeof endTime){
        endTime="";
    }
    


    $.getJSON('/cwp/src/json/workflow/workflow_myAuditList.json',function(data){
        var processMyAuditTab = new LoaderTable("hasCheckTab","hasCheckTab","/cwp/front/sh/workflow!execute",
            "uid=myAuditList&processType="+processType+"&activityId="+activityIdSelect+"&startTime="+startTime+"&endTime="+endTime,pageIndex,10,MyAuditListResultArrived,"");
        // processMyAuditTab.Display();
        processMyAuditTab.loadSuccessed(data);
    });
}

function myTodoList(pageIndex){
    var processType=$("#processType").val();
    if(processType==null){
    	processType="";
    }
    
    var activityIdSelect=$("#activityIdSelect").val();
    if(activityIdSelect==null){
    	activityIdSelect="";
    }
    
    var startTime=$("#startTime").val();
    var endTime=$("#endTime").val();
    
    if("undefined" == typeof startTime){
        startTime="";
    }
    if("undefined" == typeof endTime){
        endTime="";
    }
    

    $.getJSON('/cwp/src/json/workflow/workflow_myTodoList.json',function(data){
        var processMyTodoTab = new LoaderTable("processWorkFlowTab","processWorkFlowTab","/cwp/front/sh/workflow!execute",
            "uid=myTodoList&processType="+processType+"&activityId="+activityIdSelect+"&startTime="+startTime+"&endTime="+endTime,pageIndex,10,MyTodoListResultArrived,"");
        // processMyTodoTab.Display();
        processMyTodoTab.loadSuccessed(data);
    });
}

//==================我的起草


function myDraftList (pageIndex) {
    var processType=$("#processType").val();
    if(processType==null){
        processType="";
    }
    var activityIdSelect=$("#activityIdSelect").val();
    if(activityIdSelect==null){
        activityIdSelect="";
    }

    var startTime=$("#startTime").val();
    var endTime=$("#endTime").val();

    if("undefined" == typeof startTime){
        startTime="";
    }
    if("undefined" == typeof endTime){
        endTime="";
    }


    $.getJSON('/cwp/src/json/workflow/workflow_myCreateList.json',function(data){
        var processMyDraftTab = new LoaderTable("myDraftTab","myDraftTab","/cwp/front/sh/workflow!execute",
            "uid=myCreateList&processType="+processType+"&activityId="+activityIdSelect+"&startTime="+startTime+"&endTime="+endTime,pageIndex,10,myDraftListSuccess,"");
        // processMyDraftTab.Display();
        processMyDraftTab.loadSuccessed(data);
    });
}

//==============我的起草列表回调函数
var myDraftListSuccess = function (res) {
    var thead="<thead>"+
        "<tr>"+
        "<th>工单编号</th>"+
        "<th>工单名称</th>"+
        "<th>当前环节</th>"+
        "<th>创建人</th>"+
        "<th>发起时间</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html
    var tbodys = "<tbody>";
    var beans=res.beans;
    if(beans.length == 0){
        $('')
    }
    for(var i=0;i<beans.length;i++){

        tbodys+="<tr>"+
            "<td>"+beans[i].orderId+"</td>"+
            "<td>"+beans[i].warningName+"</td>"+
            "<td>"+beans[i].activityName+"</td>"+
            "<td>"+beans[i].trueName+"</td>"+
            "<td>"+beans[i].createTime+"</td>"+
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal trace' id='myDraftListDetail' processDefId='"+beans[i].processDefId+"' taskId='"+beans[i].taskId+"'  processInstId='"+beans[i].processInstId+"' eid='"+beans[i].eventId+"'  oid='"+beans[i].orderId+"' pid='"+beans[i].processInstId+"' >详情</a></td></tr>";
    }
    tbodys += "</tbody>";
    //显示表格html
    $("#myDraftTab").html(thead+tbodys);
    // 跟踪
    traceWorkFlow();
}

//回调函数 当前页
function paginationCallback(page_index){
    if(!isPostBack){
		switch(currentTap){
		case "mytodo":
			myTodoList(page_index+1);
			break;
		case "myaudit":
	    	myAuditList(page_index+1);
			break;
		case "ccTome":
			break;
        case "myDraft":
            myDraftList(page_index+1);
            break;
		default:

		}
    } 	
}

////发送告警信息回调函数
function MyAuditListResultArrived(result)
{

    var thead="<thead>"+
        "<tr>"+
        "<th>工单编号</th>"+
        "<th>工单名称</th>"+
        "<th>当前环节</th>"+
        "<th>创建人</th>"+
        "<th>发起时间</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html
    var tbodys = "<tbody>";
    var beans=result.beans;
    
    for(var i=0;i<beans.length;i++){

        tbodys+="<tr>"+
            "<td>"+beans[i].businessKey+"</td>"+
            "<td>"+beans[i].warningName+"</td>"+
            "<td>"+beans[i].activityName+"</td>"+
            "<td>"+beans[i].trueName+"</td>"+
            "<td>"+beans[i].createTime+"</td>"+
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal trace' processDefId='"+beans[i].processDefId+"' taskId='"+beans[i].taskId+"'  processInstId='"+beans[i].processInstId+"' eid='"+beans[i].eventId+"'  oid='"+beans[i].businessKey+"' pid='"+beans[i].processInstId+"' >详情</a></td></tr>";
    }
    tbodys += "</tbody>";
    //显示表格html
    $("#hasCheckTab").html(thead+tbodys);
    // 跟踪
    traceWorkFlow();
    
}

////发送告警信息回调函数
function MyTodoListResultArrived(result)
{

    var thead="<thead>"+
        "<tr>"+
        "<th>工单编号</th>"+
        "<th>工单名称</th>"+
        "<th>当前环节</th>"+
        "<th>创建人</th>"+
        "<th>发起时间</th>"+
        "<th colspan='2' style='text'>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html
    var tbodys = "<tbody>";
    var beans=result.beans;
    
    for(var i=0;i<beans.length;i++){
       
       var operation="审批";
       var operationClass=".info";
       switch (beans[i].activityId){
	       case "AuditOrder":
	    	   operation="审核工单";
	    	   break;
	       case "RepairFault":
	    	   operation="处理完成";
	    	   break;
	       case "AcceptanceOrder":
	    	   operation="验收";
	    	   break;	    	   
	       default:
               break;

       }
        
        tbodys+="<tr><td>"+beans[i].orderId+"</td>"+
        "<td>"+beans[i].warningName+"</td>"+
        "<td>"+beans[i].activityName+"</td>"+ 
            "<td>"+beans[i].trueName+"</td>"+
            "<td>"+beans[i].createTime+"</td>"+
            //"<td><a class='trace t-btn t-btn-sm t-btn-blue t-btn-deal' processDefId='"+beans[i].processDefId+"' taskId='"+beans[i].taskId+"'  processInstId='"+beans[i].processInstId+"' eid='"+beans[i].eventId+"'  oid='"+beans[i].orderId+"' activiti-id='"+beans[i].activityId+"' pid='"+beans[i].processInstId+"' pid='"+beans[i].processInstId+"' >详情</a></td>"+
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal operAction' processDefId='"+beans[i].processDefId+"' serviceType='"+beans[i].serviceType+"' planCompleteDate='"+beans[i].planCompleteDate+"' serviceBudget='"+beans[i].serviceBudget+"' odesc='"+beans[i].orderDescribe+"' taskId='"+beans[i].taskId+"'  processInstId='"+beans[i].processInstId+"' eid='"+beans[i].eventId+"' oid='"+beans[i].orderId+"' activiti-id='"+beans[i].activityId+"' pid='"+beans[i].processInstId+"' >"+"审批"+"</a></td></tr>";
    } 
    
    tbodys += "</tbody>";
    //显示表格html
    $("#processWorkFlowTab").html(thead+tbodys);   
    
    traceWorkFlow();
    
    
    $("#processWorkFlowTab").find('.operAction').click(function(){
        $('#auditComments').val("");
    	var options={
    			warningEventId:$(this).attr("eid"),
    			orderId:$(this).attr("oid"),
    			taskId:$(this).attr("taskId"),
    			processInstId:$(this).attr("processInstId"),
    			activitiId:$(this).attr("activiti-id"),
    			odesc:$(this).attr("odesc"),
    			serviceBudget:$(this).attr("serviceBudget"),
    			planCompleteDate:$(this).attr("planCompleteDate"),
    			serviceType:$(this).attr("serviceType"),
                processDefId:$(this).attr('processDefId')
    	}
    	detailSelectToTask(options,function(ret){
            ret.processDefId.match('MakeCard')?myDraftCheck(ret):initAuditComment(ret);
    		var operDiv=["AuditOrder","RepairFault","AcceptanceOrder"];
    		
    		$.each(operDiv,function(i,item){
    			$(ret.modal).find("."+item).hide();
    		})
    		
    		$(ret.modal).find("."+ret.activitiId).show(0,function(){
    			
    			//审核表单操作
    			$(this).find(".invalidOrder").click(function(){
        			var self=this;

        			var auditComment=$("#auditComments").val();
					if(auditComment==""||auditComment==undefined){
						alert("请填写备注说明！");
						return;
					}
        			var operId=$(this).attr("oper-id");

        			PostForm("/cwp/front/sh/faultRepairWf!execute", 
        					"uid=auditOrder&invalidOrder="+operId+"&taskId="+ret.taskId+"&processInstId="+ret.processInstId+"&businessKey="+ret.orderId+"&auditComment="+auditComment+"&eventId="+ret.warningEventId, "post", function(result){

        				if(result.returnCode!=undefined && result.returnCode=="0"){
                            alert("提交成功！");
        					$(ret.modal).modal('hide'); 
            				myTodoList(1);
        				}else{
        					alert("审核失败！");
        				}
    					//移除disabled属性
    					//$(self).removeAttr("disabled");

        			}, '',"frmMainConfirmApprover");
        			

        		});
    			
    			//表单处理完成
    			$(this).find(".repairComplete").click(function(){
        			var self=this;

        			var auditComment=$("#auditComments").val();
					if(auditComment==""||auditComment==undefined){
						alert("请填写备注说明！");
						return;
					}
        			PostForm("/cwp/front/sh/faultRepairWf!execute",
        					"uid=repairComplete&taskId="+ret.taskId+"&processInstId="+ret.processInstId+"&businessKey="+ret.orderId+"&auditComment="+auditComment+"&eventId="+ret.warningEventId,"post",function(result){

        				if(result.returnCode!=undefined && result.returnCode=="0"){
                            alert("提交成功！")
                            $(ret.modal).modal('hide');
            				myTodoList(1);
        				}else{
        					alert("提交失败！")
        				}
    					//移除disabled属性
    					//$(self).removeAttr("disabled");
        			},'',"frmMainConfirmApprover");
    			})
    			
    			
    			//表单验收
    			$(this).find(".acceptanceResult").click(function(){
        			var self=this;
        			var operId=$(this).attr("oper-id"); 			
        			var auditComment=$("#auditComments").val();
        			if(auditComment==""||auditComment==undefined){
						alert("请填写备注说明！");
						return;
					}
        			PostForm("/cwp/front/sh/faultRepairWf!execute",
        					"uid=acceptanceOrder&acceptanceResult="+operId+"&taskId="+ret.taskId+"&processInstId="+ret.processInstId+"&businessKey="+ret.orderId+"&auditComment="+auditComment+"&eventId="+ret.warningEventId,"post",function(result){

        				if(result.returnCode!=undefined && result.returnCode=="0"){
                            alert("提交成功！")
        					$(ret.modal).modal('hide'); 
            				myTodoList(1);
        				}else{
        					alert("验收失败！")
        				}
    					//移除disabled属性
    					//$(self).removeAttr("disabled");
    					
        			},'',"frmMainConfirmApprover");
    			})
    			
    			
    		});
    		
    		
    	});
    })
    
}

function initAuditComment(ret){
	var auditComment=$(ret.modal).find(".modal-body");
	auditComment.empty();
	var tColumns=$("<div>").addClass("t-columns-1");
	var tColumnsGroup=$("<ul>").addClass("t-columns-group").css({"padding-right": "0px"});
	
	var orderDescLable=$("<label>").text("维修说明：").addClass("auditLable").css({"height":"95px"});
	var orderDescDiv=$("<div>").html(ret.odesc).css({"height":"95px","overflow-y": "scroll","padding":" 0 5px"});
	var orderDescli=$("<li>").append(orderDescLable,orderDescDiv).css({"height":"100px"});
	
	var serviceBudgetLable=$("<label>").text("维修预算:").addClass("auditLable");
	var serviceBudgetDiv=$("<div>").html(ret.serviceBudget);
	var serviceBudgetli=$("<li>").append(serviceBudgetLable,serviceBudgetDiv).css({"height":"41px"});
	
	var planCompleteDateLable=$("<label>").text("计划完成时间:").addClass("auditLable");
	var planCompleteDateDiv=$("<div>").html(ret.planCompleteDate.split(' ')[0]);
	var planCompleteDateli=$("<li>").append(planCompleteDateLable,planCompleteDateDiv).css({"height":"41px"});
	
	var serviceTypeLable=$("<label>").text("维修服务:").addClass("auditLable");
	var serviceTypeDiv=$("<div>").html(ret.serviceType=="0"?"无偿服务":"有偿服务");
	var serviceTypeli=$("<li>").append(serviceTypeLable,serviceTypeDiv).css({"height":"41px"});
	
	

	// PostForm("/cwp/front/sh/workflow!execute","uid=auditHistory&currentPage=1&pageSize=1000&businessKey="+ret.orderId,"",function(result){
    //
	// 	var auditCommentLable=$("<label>").text("审批记录：").css({"height":"200px"}).addClass("auditLable");
	// 	var auditCommentDiv=$("<div>").addClass("auditComments").css({"height":"200px","overflow-y":"auto"}).scroll();
	// 	if(result.returnCode!=undefined && result.returnCode=="0"){
    //
	//
	// 		$.each(result.beans,function(i,item){
    //
	// 			if(item.auditComment!="" && item.auditComment!=null){
	// 				var $item=$("<div>").addClass("item");
	//
	// 				var $name=$("<a>").addClass("name").text("流程节点："+item.activityName).append("<small class='text-muted pull-right' style='margin-right:10px;'><i class='glyphicon glyphicon-calendar'></i> "+item.operDate+"</small>");
	//
	// 				var message=$("<p>").addClass("message");
	// 				message.append($name);
	// 				message.append("("+item.auditComment+")");
	// 				$item.append(message);
	// 				auditCommentDiv.append($item);
	// 			}
	// 		});
	//
	// 	}else{
	// 		auditCommentDiv.text("无");
	// 	}
    //
	// 	var auditCommentli=$("<li>").append(auditCommentLable,auditCommentDiv).css({"height":"200px"}).addClass("clearfix");
    //
	// 	tColumnsGroup.append(orderDescli,serviceBudgetli,planCompleteDateli,serviceTypeli,auditCommentli);
	// 	tColumns.append(tColumnsGroup);
	//
	// 	auditComment.append(tColumns);
	//
	// },'')

    $.getJSON('/cwp/src/json/workflow/workflow_auditHistory.json',function(result){
        var auditCommentLable=$("<label>").text("审批记录：").css({"height":"200px"}).addClass("auditLable");
        var auditCommentDiv=$("<div>").addClass("auditComments").css({"height":"200px","overflow-y":"auto"}).scroll();
        if(result.returnCode!=undefined && result.returnCode=="0"){


            $.each(result.beans,function(i,item){

                if(item.auditComment!="" && item.auditComment!=null){
                    var $item=$("<div>").addClass("item");

                    var $name=$("<a>").addClass("name").text("流程节点："+item.activityName).append("<small class='text-muted pull-right' style='margin-right:10px;'><i class='glyphicon glyphicon-calendar'></i> "+item.operDate+"</small>");

                    var message=$("<p>").addClass("message");
                    message.append($name);
                    message.append("("+item.auditComment+")");
                    $item.append(message);
                    auditCommentDiv.append($item);
                }
            });

        }else{
            auditCommentDiv.text("无");
        }

        var auditCommentli=$("<li>").append(auditCommentLable,auditCommentDiv).css({"height":"200px"}).addClass("clearfix");

        tColumnsGroup.append(orderDescli,serviceBudgetli,planCompleteDateli,serviceTypeli,auditCommentli);
        tColumns.append(tColumnsGroup);

        auditComment.append(tColumns);
    });
	

}