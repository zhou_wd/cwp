/**
 * 点击详情
 * @param    投诉建议
 */
//获取详细信息
function getcomplaintt(result){
    if(busiType == 0){
        var str = "投诉详情";
    }
    else{
        var str = "建议详情";
    }
    $("#exampleModalLabel").text(str);
    var details ="";
    $("#adduser").text(result.bean.submitUserName);
    $("#adddept").text(result.bean.submitUserDept);
    $("#addphone").text(result.bean.submitUserPhone);
    $("#addcontent").text(result.bean.submitMsg);
    $("#addtime").text(result.bean.submitTime);
    $("#addfeedbackId").text(result.bean.feedbackId);
    $("#addtype").text(result.bean.busiType);
    if(result.bean.status == 1){
        var status = "已处理";

    }
    else{
        var status = "未处理";
        if(order==1){
            details = "<li class='width-all' style='padding: 0;'> <label>回复内容　：</label> <textarea id='replyMsg' cols='50' maxlength='200' style='resize: none'></textarea></li>" +
                "<li class='width-all' style='padding: 0;'> <a class='t-btn t-btn-sm t-btn-blue t-btn-deal t-btn-blue-order'id='btncomplaint'>确认回复</a></li>" ;
        }
        else if(order==0){
            details ="";
        }
    }
    $("#addstatus").text(status);
    if(result.bean.status == 1){
        details = "<li style='padding: 0;'> <label>管理员　　：</label> <div id='detailsreplyUserName'>"+result.bean.replyUserName +"</div></li>"+
        "<li style='padding: 0;'> <label>反馈时间　：</label> <div id='detailsreplyTime'>"+result.bean.replyTime +"</div></li>"+
        "<li class='width-all' style='padding: 0;'> <label>反馈内容　：</label> <div id='detailsreplyMsg'>"+result.bean.replyMsg +"</div></li>" ;

    }
    $("#detailshide").html(details);

    $("#modalDetailsAlarm").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
}
var order = 0;
function repairord(userphone){
    order = 1;
    PostForm("/cwp/front/sh/feedback!execute","uid=f003&feedbackId="+userphone,"",getcomplaintt,"");
}
function detailSelect(userphone){
    order = 0;
    PostForm("/cwp/front/sh/feedback!execute","uid=f003&feedbackId="+userphone,"",getcomplaintt,"");
}


$(function(){
    searchComplaintList(1);
});

//查询
$("#btnSearchComplaint").on("click",function(){
    isPostBack=true;
    searchComplaintList(1);
});
//搜索列表
function searchComplaintList(pageIndex)
{
    var startComplaintTime = $("#startApplyTime").val();
    var endComplaintTime = $("#endApplyTime").val();
    if("undefined" == typeof startComplaintTime){
        startComplaintTime="";
    }
    if("undefined" == typeof endComplaintTime){
        endComplaintTime="";
    }
    var feedbackTab = new LoaderTable("feedbackTab","feedback","/cwp/front/sh/feedback!execute","uid=f002&startComplaintTime="+ startComplaintTime +"&endComplaintTime="+endComplaintTime+"&busiType=0",pageIndex,8,feedback,"");
    feedbackTab.Display();
}
function paginationCallback(page_index){
    if(!isPostBack) searchComplaintList(page_index+1);
}
//回调函数成功 拼接列表
function feedback(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>提交人姓名</th>"+
        "<th>提交人部门</th>"+
        "<th>提交人电话</th>"+
        //"<th>内容</th>"+
        "<th>时间</th>"+
        "<th>流程状态</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        if(result.beans[i].status == 1){
            var string = "已处理";
            var pepairbtn = "<a class='t-btn t-btn-sm t-btn-disabled t-btn-deal'>回复</a>";
        }
        else{
            var string = "未处理";
            var pepairbtn = "<a class='t-btn t-btn-sm t-btn-red t-btn-deal' onclick='repairord("+result.beans[i].feedbackId+")'>回复</a>";
        }
        tbodys += "<tr>" +
        "<td>" +  result.beans[i].submitUserName + "</td>" +
        "<td>" +  result.beans[i].submitUserDept + "</td>" +
        "<td>" +  result.beans[i].submitUserPhone + "</td>" +
        //"<td class='table-overflow'>" +  result.beans[i].submitMsg + "</td>" +
        "<td>" +  result.beans[i].submitTime + "</td>" +
        "<td>" +  string + "</td>" +
        "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal' onclick='detailSelect("+result.beans[i].feedbackId+")'>详情</a>"+pepairbtn+"</td>" +
        "</tr>"
    }
    tbodys+= "</tbody>";
    $("#feedback").html(thead+tbodys);
}
//切换分类
var busiType=0;
$("#myTabs li").on("click", function () {
    busiType=$(this).index();
    searchComplaintList(1);

});

//新建

$("#btnComplaintNew").on("click", function () {
    //验证
    addComplaintValidator();
    $("#modalComplaintNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    var userId=localStorage.getItem("parkId")
    $("#submitUserId").val(userId);
});
////绑定点击事件
/**
 * 保存
 */
$("#saveComplaint").on("click",function(){

    if ($.formValidator.PageIsValid()) {
        //保存
        PostForm("/cwp/front/sh/feedback!execute", "uid=f001","", successSaveComplaint, 'frmMain');
        $("#modalComplaintNew").modal("hide");
    }
});

//请求成功回调函数
function successSaveComplaint(result) {
    //关闭模态框
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "保存成功");
            isPostBack=true;
            searchComplaintList(1);
            break;
        case "2":
            tipModal("icon-cross", "保存失败");
            break;
        case "-9999":
            tipModal("icon-cross", "保存失败");
            break;
    }
}



//新增角色验证
function addComplaintValidator(){
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });

    $("#submitMsg").formValidator({
        onfocus: "请输入反馈内容",
        oncorrect: " "
    }).InputValidator({
        min: 6,
        max: 400,
        onempty: "请输入反馈内容",
        onerror: "反馈内容长度为3~200个字以内"
    });
}
var userId = localStorage.getItem("parkId");
//确认回复
$(document).on("click","#btncomplaint", function () {
    if ($("#replyMsg").val()!="") {
        $.ajax({
            url:'/cwp/front/sh/feedback!execute',
            beforeSend:function(requests){
                requests.setRequestHeader("platform", "pc");
            },
            data:{
                uid:'f005',
                feedbackId:$("#addfeedbackId").text(),
                busiType:$("#addtype").text(),
                replyUserId:userId,
                replyMsg:$("#replyMsg").val()
            },
            type:'post',
            success:function (res) {
                if(res.returnCode == '0'){
                    tipModal("icon-checkmark", "回复成功");
                    isPostBack=true;
                    searchComplaintList(1);
                }else{
                    tipModal("icon-cross", "回复失败");
                }
            },
            error:function () {
                _self.removeAttr('disabled');
                _self.text('保存')
            }
        })
        $("#modalDetailsAlarm").modal("hide");

        //关闭模态框
    }
    else{
        tipModal("icon-cross", "提交内容不能为空");
    }
});