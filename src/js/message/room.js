/**
 * 点击详情
 * 住宿管理
 */
$(function(){
    searchComplaintList(1);
});

//查询
$("#btnSearchComplaint").on("click",function(){
    isPostBack=true;
    searchComplaintList(1);
});
//搜索列表
function searchComplaintList(pageIndex)
{
    var activeStatus = $("#CactiveStatus").val();
    if("undefined" == typeof startComplaintTime){
        startComplaintTime="";
    }
    if("undefined" == typeof endComplaintTime){
        endComplaintTime="";
    }
    var feedbackTab = new LoaderTable("feedbackTab","feedback","/cwp/front/sh/apartment!execute","uid=R006&activeStatus="+activeStatus,pageIndex,8,feedback,"","roomform");
    feedbackTab.Display();
}
function paginationCallback(page_index){
    if(!isPostBack) searchComplaintList(page_index+1);
}
//回调函数成功 拼接列表
function feedback(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>楼栋</th>"+
        "<th>房间号</th>"+
        "<th>房间类型</th>"+
        "<th>性别</th>"+
        "<th>房间状态</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        if(result.beans[i].activeStatus ==0){
            var stractiveStatus = "<a class='t-btn t-btn-sm t-btn-red t-btn-deal locked-cole' data-roomId="+result.beans[i].roomId+" >锁定</a>";
        }
        if(result.beans[i].activeStatus ==1){
            var stractiveStatus = "<a class='t-btn t-btn-sm t-btn-green t-btn-deal locked-open ' data-roomId="+result.beans[i].roomId+" >开放</a>";
        }
        tbodys += "<tr>" +
            "<td></td>" +
            "<td>" +  result.beans[i].roomNumber + "</td>" +
            "<td>" +  result.beans[i].roomType + "</td>" +
            "<td>" +  (result.beans[i].sex=="0"?"男":"女") + "</td>" +
            "<td class='table-overflow'>" +  (result.beans[i].activeStatus=="0"?"开放":"锁定") + "</td>" +
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal detailSelect' data-roomId="+result.beans[i].roomId+">编辑</a><a class='t-btn t-btn-sm t-btn-red t-btn-deal roomdel' data-roomId="+result.beans[i].roomId+" >删除</a><a class='t-btn t-btn-sm t-btn-blue t-btn-deal details' data-roomNumber="+result.beans[i].roomNumber+" data-roomId="+result.beans[i].roomId+">详情</a>"+stractiveStatus+"</td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#feedback").html(thead+tbodys);
}
//删除
$(document).on("click",".roomdel", function () {
        $("#modalDelDept").modal("show");
        $("#delhide").val($(this).attr('data-roomId'));
});
$("#btnDeleteDept").on("click",function(){
    var roomId = $("#delhide").val();
    PostForm("/cwp/front/sh/apartment!execute","uid=R003&roomId="+roomId,"",delectroom,"");
    $("#modalDelDept").modal("hide");
});
function delectroom(result){
    isPostBack=true;
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "删除成功");
            searchComplaintList(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "删除失败");
            break;
    }
    searchComplaintList(1);
}
//编辑
var btnadd = 0;
$(document).on("click",".detailSelect", function () {
    var roomId =$(this).attr('data-roomId') ;
    addComplaintValidator();
    PostForm("/cwp/front/sh/apartment!execute","uid=R004&roomId="+roomId,"",getroom,"");
});
var hideroomId="";
function getroom(result){
    btnadd = 1;
    $(".modal-title").text("编辑");
    $(".tipArea div").html("").attr("class","");
    $("#roomNumber").val(result.bean.roomNumber);
    $("#roomDesc").val(result.bean.roomDesc);
    $("#roomTypeDictdataId").val(result.bean.roomTypeDictdataId);
    $("#sex").val(result.bean.sex);
    $("#activeStatus").val(result.bean.activeStatus);
    hideroomId = result.bean.roomId;
    $("#hidroomNumber").val(result.bean.roomNumber);
    $("#modalComplaintNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal',centerModal());

    $("#modalComplaintNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());

}
//请求成功回调函数
function successSaveComplaint(result) {
    //关闭模态框
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "添加成功");
            isPostBack=true;
            searchComplaintList(1);
            break;
        case "2":
            tipModal("icon-cross", "添加失败");
            break;
        case "-9999":
            tipModal("icon-cross", "添加失败");
            break;
    }
}
//新建
$("#btnComplaintNew").on("click", function () {
    btnadd = 0;
    //验证
    addComplaintValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalComplaintNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    var userId=localStorage.getItem("parkId")
    $("#submitUserId").val(userId);
    $(".modal-title").text("新增");
    $("#roomNumber").val("");
    $("#roomDesc").val("");
    $("#roomTypeDictdataId").val("1009004");
    $("#sex").val("0");
    $("#activeStatus").val("0");
});
$("#saveComplaint").on("click",function(){

    if ($.formValidator.PageIsValid()) {
        //保存
        if(btnadd == 0){
            $.ajax({
                type: "post",
                url: "/cwp/front/sh/apartment!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                async: false,
                dataType: "json",
                data: {
                    uid: "R001",
                    userId:localStorage.getItem("parkId"),
                    roomNumber:$("#roomNumber").val(),
                    roomDesc:$("#roomDesc").val(),
                    roomTypeDictdataId:$("#roomTypeDictdataId").val(),
                    sex:$("#sex").val(),
                    activeStatus:$("#activeStatus").val(),
                },
                success:function (res) {
                    if(res.returnCode == '0'){
                        tipModal("icon-checkmark", "添加成功");
                    }else{
                        tipModal("icon-cross", "回复");
                    }
                },
                error:function () {
                    _self.removeAttr('disabled');
                    _self.text('保存')
                }
            });
        }
        else if(btnadd == 1){
            $.ajax({
                type: "post",
                url: "/cwp/front/sh/apartment!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                async: false,
                dataType: "json",
                data: {
                    uid: "R002",
                    userId:localStorage.getItem("parkId"),
                    roomNumber:$("#roomNumber").val(),
                    roomDesc:$("#roomDesc").val(),
                    roomTypeDictdataId:$("#roomTypeDictdataId").val(),
                    activeStatus:$("#activeStatus").val(),
                    sex:$("#sex").val(),
                    roomId:hideroomId,
                },
                success:function (res) {
                    if(res.returnCode == '0'){
                        tipModal("icon-checkmark", "修改成功");
                    }else{
                        tipModal("icon-cross", "回复");
                    }
                },
                error:function () {
                    _self.removeAttr('disabled');
                    _self.text('保存')
                }
            });
        }
        $("#modalComplaintNew").modal("hide");
        isPostBack=true;
        searchComplaintList(1);

    }
});
//新增房间验证
function addComplaintValidator(){
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });

    $("#roomNumber").formValidator({
        onfocus: "请输入房间号",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请输入房间号",
    }).FunctionValidator({
        fun:function(val,elem) {
            var val = $("#hidroomNumber").val();
            var roomNumber = $("#roomNumber").val();
            if(roomNumber != val){
                var htmlObj = $.ajax({
                    type: "post",
                    url: "/cwp/front/sh/apartment!execute",
                    beforeSend:function(requests){
                        requests.setRequestHeader("platform", "pc");
                    },
                    async: false,
                    dataType: "json",
                    data: {
                        uid: "R005",
                        roomNumber:roomNumber,
                    }
                });
                var result = JSON.parse(htmlObj.responseText);
                if (result.returnCode == "0") {
                    return true;
                } else {
                    return "该房间号已存在";
                }
            }
        }
    });
    $("#roomDesc").formValidator({
        onfocus: "请输入房间描述",
        oncorrect: " "
    }).InputValidator({
        min: 1,
        onempty: "请输入房间描述",
    });
}
//房间开放/锁定
$(document).on("click",".locked-open", function () {
    var roomId =$(this).attr('data-roomId') ;
    PostForm("/cwp/front/sh/apartment!execute","uid=R007&roomId="+roomId+"&activeStatus=0","",locked,"","");

});
$(document).on("click",".locked-cole", function () {
    var roomId =$(this).attr('data-roomId') ;
    PostForm("/cwp/front/sh/apartment!execute","uid=R007&roomId="+roomId+"&activeStatus=1","",locked,"","");

});
function locked(result){
    var num = $(".current").text();
    var num = num.replace(/[^0-9]/ig,"");
    if(result.returnCode==0){
        tipModal("icon-checkmark",result.returnMessage);
        searchComplaintList(num);
    }
    else if(result.returnCode==1){
        tipModal("icon-cross",result.returnMessage);
    }
    else{
        tipModal("icon-cross","锁定失败");
    }
}
//获取床位详情
$(document).on("click",".details", function () {
    var roomId =$(this).attr('data-roomId') ;
    var roomNumber =$(this).attr('data-roomNumber') ;
    $("#roomId").val(roomId);
    $("#exampleModalLabel").text(roomNumber+"房间详情");
    PostForm("/cwp/front/sh/apartment!execute","uid=B007&pageSize=10&currentPage=1","",getberths,"","search");
});
function getberths(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>房间床位</th>"+
        "<th>标签</th>"+
        "<th>添加时间</th>"+
            /*"<th>操作</th>"+*/
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
            if(result.beans[i].bunkBed ==0){
                var strbunkBed = "下铺";
            }
            if(result.beans[i].bunkBed ==1){
                var strbunkBed = "上铺";
            }
            tbodys += "<tr>" +
                "<td>" +  result.beans[i].bedNumber + "</td>" +
                "<td>" +  strbunkBed + "</td>" +
                "<td class='table-overflow'>" +  result.beans[i].createDate + "</td>" +
                "</tr>"


    }
    tbodys+= "</tbody>";
    $("#berfeedback").html(thead+tbodys);
    $("#modalDetailsAlarm").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal',centerModal());
}