/**
 * Created by again on 2017/1/6.
 */
/**
 * 点击详情
 * 黑名单管理
 */
$(function(){
    searchComplaintList(1);
});

//查询
$("#btnSearchComplaint").on("click",function(){
    isPostBack=true;
    searchComplaintList(1);
});


//搜索列表
function searchComplaintList(pageIndex)
{
    var blackUserTab = new LoaderTable("loaderBlackUserTab","blackUserTab","/cwp/front/sh/blackUser!execute","uid=bu005",pageIndex,8,successResultArrived,"","frmMain");
    blackUserTab.Display();
}

//回调函数成功 拼接列表
function successResultArrived(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>姓名</th>"+
        "<th>开始时间</th>"+
        "<th>结束时间</th>"+
        "<th>状态</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        tbodys += "<tr>" +
            "<td>" +  result.beans[i].trueName + "</td>" +
            "<td>" +  result.beans[i].startEffectiveDate + "</td>" +
            "<td>" +  result.beans[i].endEffectiveDate + "</td>" +
            "<td>" +  (result.beans[i].status=="0"?"未解除":"已解除") + "</td>" +
            "<td>" +
                (result.beans[i].status=="0"?"<a class='t-btn t-btn-sm t-btn-blue btn-cancel' data-blackId="+result.beans[i].blackId+"  data-blackUserId="+result.beans[i].blackUserId+">解除</a></td>"
                    :"<a class='t-btn t-btn-sm' >已解除</a></td>")
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#blackUserTab").html(thead+tbodys);
}




//分页
function paginationCallback(page_index){
    if(!isPostBack) searchComplaintList(page_index+1);
}


//解除黑名单
$(document).on("click",".btn-cancel",function(){
    var blackId=$(this).attr("data-blackId");
    PostForm("/cwp/front/sh/blackUser!execute", "uid=bu002&blackId="+blackId+"&statusMY=1", "", cancelResult, "");
});

//解除回调
function cancelResult(result){
    if(result.returnCode=="0"){
        tipModal('icon-checkmark','解除成功');
        isPostBack=true;
        searchComplaintList(1);
    }
    else if(result.returnCode=="-9999"){
        tipModal('icon-cross',"解除失败");
    }
    else{
        tipModal('icon-cross',result.returnMessage);
    }
}