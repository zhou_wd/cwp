/**
 * 点击详情
 * 床位管理
 */
$(function(){
    searchComplaintList(1);
});

//查询
$("#btnSearchComplaint").on("click",function(){
    isPostBack=true;
    searchComplaintList(1);
});
//搜索列表
function searchComplaintList(pageIndex)
{

    var feedbackTab = new LoaderTable("feedbackTab","feedback","/cwp/front/sh/apartment!execute","uid=B006",pageIndex,8,feedback,"","","timegroup");
    feedbackTab.Display();
}
function paginationCallback(page_index){
    if(!isPostBack) searchComplaintList(page_index+1);
}
//回调函数成功 拼接列表
function feedback(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>房间号</th>"+
        "<th>房间床位</th>"+
        "<th>标签</th>"+
        "<th>添加时间</th>"+
        /*"<th>操作</th>"+*/
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        if(result.beans[i].bunkBed ==0){
            var strbunkBed = "下铺";
        }
        if(result.beans[i].bunkBed ==1){
            var strbunkBed = "上铺";
        }
        tbodys += "<tr>" +
            "<td>" +  result.beans[i].roomNumber + "</td>" +
            "<td>" +  result.beans[i].bedNumber + "</td>" +
            "<td>" +  strbunkBed + "</td>" +
            "<td class='table-overflow'>" +  result.beans[i].createDate + "</td>" +
            /*"<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal detailSelect' data-bedId="+result.beans[i].bedId+">编辑</a><a class='t-btn t-btn-sm t-btn-red t-btn-deal roomdel' data-bedId="+result.beans[i].bedId+" >删除</a></td>" +*/
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#feedback").html(thead+tbodys);
}
//删除
$(document).on("click",".roomdel", function () {
    var bedId =$(this).attr('data-bedId') ;
    PostForm("/cwp/front/sh/apartment!execute","uid=B003&bedId="+bedId,"",delectroom,"");
});
function delectroom(result){
    isPostBack=true;
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "删除成功");
            searchComplaintList(1);
            break;
        case "1":
            tipModal("icon-cross", result.returnMessage);
            break;
        case "-9999":
            tipModal("icon-cross", "删除失败");
            break;
    }
    searchComplaintList(1);
}
//编辑
var btnadd = 0;
$(document).on("click",".detailSelect", function () {
    var bedId =$(this).attr('data-bedId') ;
    addComplaintValidator();
    $("#modalComplaintNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    PostForm("/cwp/front/sh/apartment!execute","uid=B004&bedId="+bedId,"",getroom,"");
});
var hideroomId="";
function getroom(result){
    btnadd = 1;
    $(".modal-title").text("编辑");
    $(".tipArea div").html("").attr("class","");
    $("#roomId").val(result.bean.roomId);
    var Bednum = result.bean.bedNumber;
    Bednum = Bednum.split("-");
    $("#Bednum").val(Bednum[1]);
    $("#hidbedId").val(result.bean.bedId);
    $("#HidBednum").val(Bednum[1]);
}
//请求成功回调函数
function successSaveComplaint(result) {
    //关闭模态框
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "添加成功");
            isPostBack=true;
            searchComplaintList(1);
            break;
        case "2":
            tipModal("icon-cross", "添加失败");
            break;
        case "-9999":
            tipModal("icon-cross", "添加失败");
            break;
    }
}
//新建
$("#btnComplaintNew").on("click", function () {
    btnadd = 0;
    //验证
    addComplaintValidator();
    $(".tipArea div").html("").attr("class","");
    $("#modalComplaintNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    var userId=localStorage.getItem("parkId")
    $(".modal-title").text("新增");
});
PostForm("/cwp/front/sh/apartment!execute","uid=R006_2&pageSize=100&currentPage=1","",getroomnum,"");
//房间类别
function getroomnum(result){
    var sel = $("#roomId");
    var option = "";
    sel.empty();
    for(var i=0;i<result.beans.length;i++) {
        option = $("<option>").text(result.beans[i].roomNumber).val(result.beans[i].roomId);
        sel.append(option);
    }
}
$("#saveComplaint").on("click",function(){
    if ($.formValidator.PageIsValid()) {
        //保存
        var bunkBed = $("#Bednum").val();
        bunkBed = bunkBed%2;
        var bedNumber =$("#roomId option:selected").text() +"-"+$("#Bednum").val();
        if(btnadd == 0){
            $.ajax({
                type: "post",
                url: "/cwp/front/sh/apartment!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                async: false,
                dataType: "json",
                data: {
                    uid: "B001",
                    userId:localStorage.getItem("parkId"),
                    roomId:$("#roomId").val(),
                    bedNumber:bedNumber,
                    bunkBed:bunkBed,
                },
                success:function (res) {
                    if(res.returnCode == '0'){
                        tipModal("icon-checkmark", "添加成功");
                    }else{
                        tipModal("icon-cross", "回复");
                    }
                },
                error:function () {
                    _self.removeAttr('disabled');
                    _self.text('保存')
                }
            });
        }
        else if(btnadd == 1){
            $.ajax({
                type: "post",
                url: "/cwp/front/sh/apartment!execute",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                async: false,
                dataType: "json",
                data: {
                    uid: "B002",
                    userId:localStorage.getItem("parkId"),
                    roomId:$("#roomId").val(),
                    bedNumber:bedNumber,
                    bunkBed:bunkBed,
                    bedId:$("#hidbedId").val(),
                },
                success:function (res) {
                    if(res.returnCode == '0'){
                        tipModal("icon-checkmark", "修改成功");
                    }else{
                        tipModal("icon-cross", "回复");
                    }
                },
                error:function () {
                    _self.removeAttr('disabled');
                    _self.text('保存')
                }
            });
        }
        $("#modalComplaintNew").modal("hide");
        isPostBack=true;
        searchComplaintList(1);
    }
});
//新增角色验证
function addComplaintValidator(){
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {return false;}, onsuccess: function () {return true;}
    });

    $("#Bednum").formValidator({
        onfocus:"该床位号已存在",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:0,
        onempty:"该床位号已存在",
        onerror: "该床位号已存在"
    }).FunctionValidator({
        fun:function(val,elem) {
            var roomId =$("#roomId").val();
            var bedNumber =$("#roomId option:selected").text() +"-"+$("#Bednum").val();
            var Bednum =$("#Bednum").val();
            var HidBednum =$("#HidBednum").val();
            if(Bednum != HidBednum){
                var htmlObj = $.ajax({
                    type: "post",
                    url: "/cwp/front/sh/apartment!execute",
                    async: false,
                    dataType: "json",
                    data: {
                        uid: "B005",
                        roomId:roomId,
                        bedNumber:bedNumber,
                    }
                });
                var result = JSON.parse(htmlObj.responseText);
                if (result.returnCode == "0") {
                    return true;
                } else {
                    return "该床位号已存在";
                }
            }
        }
    });
}