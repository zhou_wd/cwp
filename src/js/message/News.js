/**
 * 点击详情
 * @param warningEventId   告警事件id
 */

var modalHtml="";
$(function(){
    modalHtml=$("#modalDetailsAlarm").html();
});


function detailSelect(type){

    PostForm("/cwp/front/sh/news!execute","uid=N003_2&newsId="+type,"",getrepor,"");
}


//获取详细信息
function getrepor(result){
    $("#modalDetailsAlarm .modal-title").text("新闻详情");
    $("#newstitle").text(result.bean.title);
    $("#newstime").text(result.bean.createTime);
    $("#newscontent").html(result.bean.content);
    $("#newsuser").text(result.bean.trueName);

    $("#modalDetailsAlarm").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');

}
//切换分类
var busiType=0;
//查询
$("#btnSearchFaultAlarm").on("click",function(){
    isPostBack=true;
    getrepair(1);
});


function getrepair(pageIndex){
    var date = $("#startApplyTime").val();
    var date2 = $("#endApplyTime").val();
    if("undefined" == typeof date){
        date="";
    }
    var loaderRealtyTab = new LoaderTable("faultAlarmLoader","faultAlarmTab","/cwp/front/sh/news!execute","uid=N002_2&type="+busiType+"&date="+date+"&date2="+date2,pageIndex,8,repair,"");
    loaderRealtyTab.Display();
}

function paginationCallback(page_index){
    if(!isPostBack) getrepair(page_index+1);
}

var e1 = 0;
function repair(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>内容标题</th>"+
        //"<th>内容详情</th>"+
        "<th>发布时间</th>"+
        "<th>发布人</th>"+
        "<th class='unBind'>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        if(result.beans[i].status == 1){
            var string = "已处理";
        }
        else{
            var string = "未处理";
        }
        tbodys += "<tr>" +
            "<td class='table-overflow'>" +  result.beans[i].title + "</td>" +
            //"<td class='table-overflow'>" +  result.beans[i].content + "</td>" +
            "<td>" +  result.beans[i].createTime + "</td>" +
            "<td>" +  result.beans[i].trueName + "</td>" +
            "<td>" +
                "<a class='t-btn t-btn-sm t-btn-blue t-btn-deal'  id='btnent' onclick='revisions("+result.beans[i].newsId+")'>编辑</a>" +
                "<a class='t-btn t-btn-sm t-btn-blue t-btn-deal'   onclick='detailSelect("+result.beans[i].newsId+")'>详情</a>" +
                "<a class='t-btn t-btn-sm t-btn-red t-btn-deal' id='btnDelete' data-newsId=" +result.beans[i].newsId + ">删除</a>" +
            "</td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#faultAlarmTab").html(thead+tbodys);
}

$(function() {
    getrepair(1);
});



//新增验证
function eventValidtor()
{
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });

    $("#title").formValidator({
        onfocus: "请输入内容标题",
        oncorrect: " "
    }).InputValidator({
        min:4,
        max: 100,
        onempty: "请输入内容标题",
        onerror: "描述长度为4-100个字符"
    });
}
/*
 * post提交数据
 * 发送地址，uid，action行为，成功回调函数，失败回调函数
 *@author cheqniuxu
 *@created 2016/05/31
 *通过字典表查询字典数据表告警类别
 * */

$(function() {
    //验证
    eventValidtor();
});



//-----------------新增开始--------------------------//
var userId = localStorage.getItem("parkId");
//添加新闻公告
function saveWarningEvent() {
    //判断表单验证是否通过
    if ($.formValidator.PageIsValid()) {

        if(UE.getEditor('editor').hasContents()==true){
            $("#contentTip").html("").attr("class","onSuccess");
        }else{
            $("#contentTip").html("请编辑新闻内容").attr("class","onError");
            return false;
        }
        var host=window.location.protocol+"//"+window.location.host;
        var title=$("#title").val();
        var newContent=UE.getEditor('editor').getContent();
        if(btnrevisions == "")//新增
        {
            $.ajax({
                type:'post',
                url:"/cwp/front/sh/news!execute?uid=N001_2",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                data:{
                    rid:Math.random(),
                    createUser:userId,
                    title:title,
                    type:busiType,
                    content:newContent
                },
                dataType: "json",
                success:function (res) {
                    if(res.returnCode == '0'){
                        tipModal("icon-checkmark", "添加成功");
                        getrepair(1);
                    }else{
                        tipModal("icon-cross", "添加失败");
                    }
                },
                error:function () {
                    tipModal("icon-cross", "添加失败");
                }
            });
        }
        else{//编辑
            $.ajax({
                type:'post',
                url:"/cwp/front/sh/news!execute?uid=N005_2",
                beforeSend:function(requests){
                    requests.setRequestHeader("platform", "pc");
                },
                data:{
                    rid:Math.random(),
                    userId:userId,
                    newsId:btnrevisions,
                    title:title,
                    type:busiType,
                    content:newContent,
                    status:status
                },

                dataType: "json",
                success:function (res) {
                    if(res.returnCode == '0'){
                        tipModal("icon-checkmark", "编辑成功");
                        getrepair(1);
                    }else{
                        tipModal("icon-cross", "编辑失败");
                    }
                },
                error:function () {
                    tipModal("icon-cross", "编辑失败");
                }
            });
        }
        //重置关闭模态框

        $("#modalMaintenanceNew").modal("hide");
    }
}

//关闭模态框 当调用 hide 实例方法时触发。
$('#modalMaintenanceNew').on('hide.bs.modal', function () {
    $(".tipArea div").attr("class","").html("");
})


////绑定点击事件
//新建
var btnrevisions = "";
var status ="";
function revisions(type){

    //根据ID获取新闻
    PostForm("/cwp/front/sh/news!execute","uid=N003_2&newsId="+type,"",getrevisions,"");
}

function getrevisions(result){
    $("#modalMaintenanceNew .modal-title").text("编辑新闻");
    $("#title").val(result.bean.title);
    UE.getEditor('editor').setContent(result.bean.content);


    $("#busiType").val(result.bean.type);
    btnrevisions = result.bean.newsId;
    status  = result.bean.status;

    $("#modalMaintenanceNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
}

$("#btnMaintenanceNew").on("click", function () {
    //告警类型
    btnrevisions = "";
    $("#modalMaintenanceNew .modal-title").text("新增新闻");
    $("#title").val("");
    UE.getEditor('editor').setContent('');
    $("#busiType").val("");
    $("#modalMaintenanceNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
});
//-------------新增结束---------------------//

//删除开始
var newsIdDel;

$(document).on("click","#btnDelete",function(){
    // if(confirm("确定要删除吗？")){
    //     var newsId=$(this).attr("data-newsId");
    //     PostForm("/cwp/front/sh/news!execute", "uid=N004_2&newsId="+newsId, "", delectnews, '');
    // }

    newsIdDel=$(this).attr("data-newsId");
    $("#delList .modal-title").text("删除");
    $("#delList").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    return false;
});

//删除节点
$("#btnDelList").on("click",function(){
    PostForm("/cwp/front/sh/news!execute", "uid=N004_2&newsId="+newsIdDel, "", delectnews, '');
    $("#delList").modal('hide');
});

//
function beforeRemove(id) {
    $("#buildingId").val(id.getAttribute("data-id"));
    $("#modalDelBuilding").modal({//启用modal ID
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
    return false;
}
//

//删除新闻
function delectnews(){
    isPostBack=true;
    getrepair(1);
}

$(function(){
    initEditor();
});

////实例化编辑器
//
function initEditor(){
    var ue = UE.getEditor('editor',{
        elementPathEnabled: false, //删除元素路径
        //wordCount: false,   //删除字数统计
        autoHeightEnabled:false//取消自动增高
    });
    UE.Editor.prototype._bkGetActionUrl=UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl=function(action){
        if(action == 'uploadimage'){
            var host=window.location.protocol+"//"+window.location.host;
            return host+"/cwp/ueditor/fileUpload.do";
        }else{
            return this._bkGetActionUrl.call(this,action);
        }
    }
}

