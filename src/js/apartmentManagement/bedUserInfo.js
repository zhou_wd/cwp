var timeSegmentIdsAM = "1007001,1007004,1007005";
var timeSegmentIdsPM = "1007002,1007003";
//切换分类
var tabId = 0;
getBedUserInfo(1);
$("#myTabs li").on("click", function () {
	isPostBack=true;
    $("#sex").val("");
    $("#roomNumber").val("");
	tabId = $(this).index();
    getBedUserInfo(1);
});
//查询
$("#btnSearchBedApply").on("click",function(){
	isPostBack=true;
    getBedUserInfo(1);
});
//获取床位分配信息
function getBedUserInfo(pageIndex){
    if(tabId==0){
        var loaderRealtyTab = new LoaderTable("faultAlarmLoader","faultApplyTab","/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis009&timeSegmentIds="+timeSegmentIdsAM,pageIndex,8,bedUser,"","frmSearch");
        loaderRealtyTab.Display();
    }
    else if(tabId==1){
        var loaderRealtyTab = new LoaderTable("PMfaultApplyTabPagination","PMfaultApplyTab","/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis009&timeSegmentIds="+timeSegmentIdsPM,pageIndex,8,bedUserPM,"","frmSearch");
        loaderRealtyTab.Display();
    }
}

function paginationCallback(page_index){
	if(tabId == 0){
		if(!isPostBack) getBedUserInfo(page_index+1);
	}else{
		if(!isPostBack) getBedUserInfo(page_index+1);
	}
}
function bedUserPM(result){
    if(result.returnCode==0){
    var thead="<thead>"+
        "<tr>"+
        "<th>房间号</th>"+
        "<th>房间类型</th>"+
        "<th>房间标签</th>"+
        "<th>已预约人数</th>"+
        "<th>已授权人数</th>"+
        "<th>剩余床位数</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        var bedUser = result.beans[i];
        if(bedUser.sex==""){
            var sex = ""
        }else{
            var sex = (bedUser.sex=="0"?"男":"女");
        }
        if(bedUser.roomId==0){
            var bedbtn = "t-btn t-btn-sm t-btn-disabled t-btn-deal";
        }
        else{
            var bedbtn ="t-btn t-btn-sm t-btn-blue t-btn-deal detailSelect";
        }
        tbodys += "<tr>" +
            "<td>" +  bedUser.roomNumber + "</td>" +
            "<td>" +  bedUser.roomDictdataName + "</td>" +
            "<td>" +  sex + "</td>" +
            "<td>" +  bedUser.pNum + "</td>" +
            "<td>" +  bedUser.checkInNum + "</td>" +
            "<td>" +  bedUser.residueBedNum + "</td>" +
            "<td><a class='"+bedbtn+"' data-time='"+timeSegmentIdsPM+"' data-roomid='"+bedUser.roomId +"'>详情</a></td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#PMfaultApplyTab").html(thead+tbodys);
    }
    else if(result.returnCode==1){
        $("#PMfaultApplyTab").html(result.returnMessage);
    }
    else{
        $("#PMfaultApplyTab").html("操作失败");
    }
}
function bedUser(result){
    if(result.returnCode==0) {
        var thead = "<thead>" +
            "<tr>" +
            "<th>房间号</th>" +
            "<th>房间类型</th>" +
            "<th>房间标签</th>" +
            "<th>已预约人数</th>" +
            "<th>已授权人数</th>" +
            "<th>剩余床位数</th>" +
            "<th>操作</th>" +
            "</tr>" +
            "</thead>";
        var tbodys = "<tbody>";
        for (var i = 0; i < result.beans.length; i++) {
            var bedUser = result.beans[i];
            if (bedUser.sex == "") {
                var sex = ""
            } else {
                var sex = (bedUser.sex == "0" ? "男" : "女");
            }
            if (bedUser.roomId == 0) {
                var bedbtn = "t-btn t-btn-sm t-btn-disabled t-btn-deal ";
            }
            else {
                var bedbtn = "t-btn t-btn-sm t-btn-blue t-btn-deal detailSelect";
            }
            tbodys += "<tr>" +
                "<td>" + bedUser.roomNumber + "</td>" +
                "<td>" + bedUser.roomDictdataName + "</td>" +
                "<td>" + sex + "</td>" +
                "<td>" + bedUser.pNum + "</td>" +
                "<td>" + bedUser.checkInNum + "</td>" +
                "<td>" + bedUser.residueBedNum + "</td>" +
                "<td><a class='" + bedbtn + "' data-time='" + timeSegmentIdsAM + "' data-roomid='" + bedUser.roomId + "'>详情</a></td>" +
                "</tr>"
        }
        tbodys += "</tbody>";
        $("#faultApplyTab").html(thead + tbodys);
    }
    else if(result.returnCode==1){
        $("#PMfaultApplyTab").html(result.returnMessage);
    }
    else{
        $("#PMfaultApplyTab").html("操作失败");
    }
}
//详情
$(document).on("click",".detailSelect", function () {
    var roomId =$(this).attr('data-roomId') ;
    var timeSegmentIds = $(this).attr('data-time') ;
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis011&roomId="+roomId+"&timeSegmentIds="+timeSegmentIds,"",delectroom,"");
});
function delectroom(result){
    $("#exampleModalLabel").text(result.beans[0].roomNumber+"房间入住详情");
    var thead="<thead>"+
        "<tr>"+
        "<th>床位号</th>"+
        "<th>入住人姓名</th>"+
        "<th>入住人性别</th>"+
        "<th>健康状况</th>"+
        "<th>部门</th>"+
        "<th>入住时间段</th>"+
        "<th>授权时间</th>"+
        "<th>预离时间</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        var bedUser = result.beans[i];
        tbodys += "<tr>" +
            "<td>" +  bedUser.bedNumber + "</td>" +
            "<td>" +  bedUser.trueName + "</td>" +
            "<td>" +  (bedUser.sex=="0"?"男":"女") + "</td>" +
            "<td>" +  bedUser.bodyDictdataName + "</td>" +
            "<td>" +  bedUser.deptName + "</td>" +
            "<td>" +  bedUser.timeDictdataName + "</td>" +
            "<td>" +  bedUser.opencardDate + "</td>" +
            "<td>" +  bedUser.checkoutDate + "</td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#roominfoTab").html(thead+tbodys);
    $("#modalDetailsAlarm").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal',centerModal());
}
//获取当前日期
function getToday(){
	var today = "";
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
	return today;
}