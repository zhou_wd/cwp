//切换分类
var applyTime =getToday(-1);
var echartColors = [];
$(function(){
	echartColors = ["#5b9bd5","#ed7d31","#ffc000"];
    $("#startApplyTime").val(getToday(-1));
    getBedUserInfo(1);
});

//查询

$("#btnSearchFaultAlarm").on("click",function(){
    getBedUserInfo(1);
});

//获取床位分配信息
function getBedUserInfo(pageIndex){
    var time = $("#startApplyTime").val();
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis010&applyTime="+time,"",getinfo,"","fromSearch");
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis008&currentPage="+pageIndex+"&pageSize=8&applyTime="+time+"&deptName=","",aptInfo,"","fromSearch");

}
function getinfo(result){
    if(result.returnCode == 0){ 

        var allNum = result.bean.allNum;
        var allRate = (result.bean.allRate*100).toFixed(0);
        var allGraNum = result.bean.allGraNum;

        var allAmNum = result.bean.allAmNum;
        var manAmNum = result.bean.manAmNum;
        var womanAmNum = result.bean.womanAmNum;
        var allAmRate = (result.bean.allAmRate*100).toFixed(0);
        var manAmRate = (result.bean.manAmRate*100).toFixed(0);
        var womanAmRate = (result.bean.womanAmRate*100).toFixed(0);
        var allAmGraNum = result.bean.allAmGraNum;

        var allPmNum = result.bean.allPmNum;
        var manPmNum = result.bean.manPmNum;
        var womanPmNum = result.bean.womanPmNum;
        var allPmRate = (result.bean.allPmRate*100).toFixed(0);
        var manPmRate = (result.bean.manPmRate*100).toFixed(0);
        var womanPmRate = (result.bean.womanPmRate*100).toFixed(0);
        var allPmGraNum = result.bean.allPmGraNum;

        var mansNumLegendData = ['入住人数(总)','入住人数(男)','入住人数(女)','孕妇人数'];
        var allNumData = [allNum,allAmNum,allPmNum];
        var allManNumData = [(Number(manAmNum)+Number(manPmNum)),manAmNum,manPmNum];
        var allWomanNumData = [(Number(womanAmNum)+Number(womanPmNum)),womanAmNum,womanPmNum];
        var allGraNumData = [allGraNum,allAmGraNum,allPmGraNum];

        var allRateData = [allRate,allAmRate,allPmRate];
        var allManRateData = [((Number(manAmRate) + Number(manPmRate))/2).toFixed(0),manAmRate,manPmRate];
        var allWomanRateData = [((Number(womanAmRate) + Number(womanPmRate))/2).toFixed(0),womanAmRate,womanPmRate];

        mansEchartFn('mansAllEcharts',allNumData[0],allManNumData[0],allWomanNumData[0],allGraNumData[0],allRateData[0],allManRateData[0],allWomanRateData[0],1);
        mansEchartFn('mansAmEcharts',allNumData[1],allManNumData[1],allWomanNumData[1],allGraNumData[1],allRateData[1],allManRateData[1],allWomanRateData[1],1);
        mansEchartFn('mansPmEcharts',allNumData[2],allManNumData[2],allWomanNumData[2],allGraNumData[2],allRateData[2],allManRateData[2],allWomanRateData[2],1);

    }else{
        mansEchartFn('mansAllEcharts');
        mansEchartFn('mansAmEcharts');
        mansEchartFn('mansPmEcharts');
    }
}

function aptInfo(result){
    var aptNameData = [];
    var aptNumData = [];

    var resultL = result.beans.length;

    var nameDif = resultL - 5;
    if(nameDif < 1 ) nameDif = 0

    for(var i=0;i<resultL;i++){
        for(var j = i + 1;j<resultL;j++){
            if(Number(result.beans[i].pNum)>Number(result.beans[j].pNum)){
                var tmp = result.beans[i];
                result.beans[i] = result.beans[j];
                result.beans[j] = tmp;
            }
        }
    }

    if(result.returnCode == 0){
        var dptRegex = /(^\u6cb3\u5357\u5206\u516c\u53f8)|(\(\u91cd\u8981\u5ba2\u6237\)$)|(\u4e2d\u5fc3$)|(^\u6d1b\u9633)|(\u56ed\u533a\u5206\u516c\u53f8$)|(\u56e2\u961f$)|(\u529e\u516c\u5ba4$)|(\u5171\u4eab\u4e2d\u5fc3$)|(^\u6cb3\u5357\u7701\u5206\u516c\u53f8)/g;

        var dptOther = 0;

        for (var i = 0; i < resultL; i++) {
            if(i<nameDif){
                dptOther += Number(result.beans[i].pNum);
            }else{
                aptNameData.push(result.beans[i].deptName.replace(dptRegex,''));
                aptNumData.push(result.beans[i].pNum);
            }
        };
        aptNameData.push('其它部门');
        aptNumData.push(dptOther);
    }
    aptEchartsFn('aptEcharts',['入住人数'],aptNameData,aptNumData,'人数(人)');
}

//获取当前日期
function getToday(timeD){
    var today = "";
    var date = new Date();
    date.setDate(date.getDate() + timeD);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
    return today;
}


function mansEchartFn (divId,allNumberData,manNumberData,womanNumberData,GraNumberData,allR,manR,womanR,rtCode) {

    var mansAllEchart = echarts.init(document.getElementById(divId));
    var allNumber  = parseInt(allNumberData);
    var manNumber  = parseInt(manNumberData);
    var womanNumber  = parseInt(womanNumberData);
    var graNumber  = parseInt(womanNumberData);
    
    if(isNaN(manR)){
        manR = 0
    }
    if(isNaN(womanR)){
        womanR = 0
    } 
    allR  = parseInt(allR) + '%';
    manR  = parseInt(manR) + '%';
    womanR  = parseInt(womanR) + '%';

    var option;
    if(rtCode == 1){
        option = {
            color:echartColors,
            legend: {
                data:['总人数:'+allNumber+',入住率:'+allR,'男生人数:'+manNumber+',入住率:'+manR,'女生人数:'+womanNumber+',入住率:'+womanR,'孕妇人数('+GraNumberData + ')']
            },
            calculable : true,
            series :[
                {
                    name:'入住人数',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {value:0, name:'总人数:'+allNumber+',入住率:'+allR},
                        {value:manNumber, name:'男生人数:'+manNumber+',入住率:'+manR},
                        {value:womanNumber, name:'女生人数:'+womanNumber+',入住率:'+womanR}
                    ]
                }
            ]
        };
    } else {
        option = {
            legend: {
                data:['正常','不正常']
            },
            series : [
                {
                    name:'',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[

                    ]
                }
            ]            
        }
    }

    mansAllEchart.setOption(option);
}

function aptEchartsFn(divId,legendData,xAxisData,allData,nameRotate){
    var gridLit = {
            borderWidth:0
        },
        barMaxW = 50;
    var winWidth;
    if (window.innerWidth){
        winWidth = window.innerWidth;
    }else if ((document.body) && (document.body.clientWidth)){
        winWidth = document.body.clientWidth;
    }
    if(winWidth<1400){
        barMaxW =40;
        gridLit = {
            x:30,
            x2:10,
            borderWidth:0
        };     
    };

    var aptEchart = echarts.init(document.getElementById(divId));
    var option = {
        title: {
            // x: 'center',
            // text: legendData[0],
            // textStyle: {
            //     fontFamily: '微软雅黑',
            //     fontWeight: 'bolder',
            //     color: 'rgb(57,146,208)'
            // },
            show : false
        },
        grid: gridLit,
        xAxis: [
            {
                type: 'category',
                splitLine: false,
                axisLabel: {
                    interval: 0,
                    // rotate: nameRotate
                },
                // show: false,
                data: xAxisData
            }
        ],
        yAxis: [
            {
                type: 'value',
                splitLine: false,
            }
        ],
        series: [
            {
                name: legendData[0],
                type: 'bar',
                data: allData,
                barMaxWidth:barMaxW,
                itemStyle: {
                    normal: {
                        color: function(params) {
                            // var colorList = [
                            //   '#C1232B','#B5C334','#FCCE10','#E87C25','#27727B',
                            //    '#FE8463','#9BCA63','#FAD860','#F3A43B','#60C0DD',
                            //    '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
                            // ];
                            var colorList = [echartColors[2],'rgb(141,219,241)',echartColors[0],'#80801A',echartColors[1],'rgb(243,181,149)','rgb(143,197,243)',echartColors[2]];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{b}\n{c}'
                            // formatter: '{c}'
                        }
                    }
                },
                markPoint: {
                    data: function(){
                        var xAxisArry = [];
                        for (var i = 0; i < xAxisData.length; i++) {
                            xAxisArry[i] = {
                                xAxis:i, 
                                y: 350, 
                                name:xAxisData[i], 
                                symbolSize:0
                            };
                        };
                        return xAxisArry;
                    }
                }
            }
        ]    
    };
    aptEchart.setOption(option);
}