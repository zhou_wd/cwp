//初始化部门
$(function(){
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute", "uid=bedUserDept_001", "", getDeptList, '');
});

function getDeptList(result){
    if(result.returnCode=="0"){
        var data=JSON.parse(result.object);
        var beans=result.beans;
        //区分已被禁用部门
        if(beans.length>0){
            for(var i=0; i < beans.length; i++){
                for(var j=0; j<data.length; j++){
                    if(beans[i].deptId==data[j].id.toString()){
                        data[j].iconSkin="selected"
                    }
                }
            }
        }
        //判断是否禁用10001
        template.helper('selectedFormat', function(inp) {
            if(inp == "selected") {
                return 'selected';
            } else {
                return '';
            }
        });
        template.helper('displayFormat', function(inp) {
            if(inp == "10001") {
                return 'hide';
            } else {
                return '';
            }
        });
        var html = template("deptListTmpl",data);
        $("#deptList").html(html);
    }
}

//选择部门
$(document).on("click",".t-tag-category",function(){
    if($(this).hasClass("selected")){
        $(this).removeClass("selected");
    }
    else{
        $(this).addClass("selected");
    }
});

//保存选择
$("#btnConfirm").on("click", function () {
    var deptArry = new Array();
    $("#deptList .selected").each(function(){
        var deptId= $(this).attr("dept-id");
        deptArry.push(deptId);
    });
    var deptStr=deptArry.toString()
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute", "uid=bedUserDept_002&limit_dept="+deptStr, "", updateDeptLimit, '');
});

function updateDeptLimit(result){
    if(result.returnCode=="0"){
        tipModal("icon-checkmark", "保存成功");
    }
    else{
        tipModal('icon-cross',"保存失败");
    }
}