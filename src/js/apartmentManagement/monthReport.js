//切换分类
var tabId = 0;
//查询
$("#btnSearchAll").on("click",function(){
    isPostBack=true;
    getReportAll();
});
$("#btnSearchDetail").on("click",function(){
    isPostBack=true;
    getReportInfo(1);
});

$("#btnExportAll").on("click",function(){
	var startApplyTime = $("#startApplyTimeAll").val();
	var endApplyTime = $("#endApplyTimeAll").val();
	var startApplyTime = $("#startApplyTimeAll").val();
	var endApplyTime = $("#endApplyTimeAll").val();
	if(startApplyTime == '' || endApplyTime == ''){
		tipModal("icon-cross", "开始时间、结束时间不能为空");
	}else{
        PostForm("/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis014","",function(res){
            if(res.returnCode == '0') {
                var host=window.location.protocol+"//"+window.location.host;
                var a = res.object;
                window.location.href = host + "/cwp/" + a;
            }
            else{
                tipModal("icon-cross", res.returnMessage);
            }
        },"","frmSearch");
        //$.ajax({
	    //    url:'/cwp/front/sh/bedapplyAnalysis!execute?uid=bedapplyAnalysis014&startApplyTime='+startApplyTime+'&endApplyTime='+endApplyTime,
	    //    type:'post',
	    //    success:function (res) {
	    //        if(res.returnCode == '0') {
	    //          var host=window.location.protocol+"//"+window.location.host;
	    //          var a = res.object;
	    //           window.location.href = host + "/cwp/" + a;
	    //        }
	    //    }
	    //});
	}
});

//获取总数据
function getReportAll(){
	var startApplyTime = $("#startApplyTimeAll").val();
	var endApplyTime = $("#endApplyTimeAll").val();
	if(startApplyTime == '' || endApplyTime == ''){
		tipModal("icon-cross", "开始时间、结束时间不能为空");
	}else{
		PostForm("/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis013","",getinfo,"","frmSearch");
	}
}

//获取床位分配信息
function getReportInfo(pageIndex){
	var startApplyTime = $("#startApplyTimeAll").val();
	var endApplyTime = $("#endApplyTimeAll").val();
	if(startApplyTime == '' || endApplyTime == ''){
		tipModal("icon-cross", "开始时间、结束时间不能为空");
	}else{	
		var loaderRealtyTab = new LoaderTable("detailReportLoader","detailReportTab","/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis012",pageIndex,8,report,"","detailSearch");
		loaderRealtyTab.Display();
	}
}

function paginationCallback(page_index){
    if(tabId == 1){
        if(!isPostBack) getReportInfo(page_index+1);
    }
}
function getinfo(result){
	tipModal("icon-cross", result.returnMessage);
    /*$("#allNum").text(result.bean.allNum);
    $("#allRate").text((result.bean.allRate*100).toFixed(0)+"%");;
    $("#allGraNum").text(result.bean.allGraNum);
    $("#allAmNum").text(result.bean.allAmNum);
    $("#manAmNum").text(result.bean.manAmNum);
    $("#womanAmNum").text(result.bean.womanAmNum);
    $("#allAmRate").text((result.bean.allAmRate*100).toFixed(0)+"%");
    $("#manAmRate").text((result.bean.manAmRate*100).toFixed(0)+"%");
    $("#womanAmRate").text((result.bean.womanAmRate*100).toFixed(0)+"%");
    $("#allPmNum").text(result.bean.allPmNum);
    $("#manPmNum").text(result.bean.manPmNum);
    $("#womanPmNum").text(result.bean.womanPmNum);
    $("#allPmRate").text((result.bean.allPmRate*100).toFixed(0)+"%");
    $("#manPmRate").text((result.bean.manPmRate*100).toFixed(0)+"%");
    $("#womanPmRate").text((result.bean.womanPmRate*100).toFixed(0)+"%");
    if(result.returnCode==1){
        $("#allNum").text("");
        $("#allRate").text("");
        $("#allGraNum").text("");
        $("#allAmNum").text("");
        $("#manAmNum").text("");
        $("#womanAmNum").text("");
        $("#allAmRate").text("");
        $("#manAmRate").text("");
        $("#womanAmRate").text("");
        $("#allPmNum").text("");
        $("#manPmNum").text("");
        $("#womanPmNum").text("");
        $("#allPmRate").text("");
        $("#manPmRate").text("");
        $("#womanPmRate").text("");
    }*/
}
function report(result){
    /*var thead="<thead>"+
        "<tr>"+
        "<th>部门名称</th>"+
        "<th>总人数</th>"+
        "<th>入住率</th>"+
        "<th>中午入住人数</th>"+
        "<th>中午入住率</th>"+
        "<th>晚上入住人数</th>"+
        "<th>晚上入住率</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
        var bedUser = result.beans[i];
        tbodys += "<tr>" +
            "<td>" +  bedUser.deptName + "</td>" +
            "<td>" +  bedUser.pNum + "</td>" +
            "<td>" +  (bedUser.checkInRate*100).toFixed(0) + "%</td>" +
            "<td>" +  bedUser.pAmNum + "</td>" +
            "<td>" +  (bedUser.checkInAmRate*100).toFixed(0) + "%</td>" +
            "<td>" +  bedUser.pPmNum + "</td>" +
            "<td>" +  (bedUser.checkInPmRate*100).toFixed(0) + "%</td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";

    if(result.returnCode==1){
        $("#faultApplyTab").html("暂无数据");
    }
    else{
        $("#faultApplyTab").html(thead+tbodys);
    }*/
	alert();
    $("#detailReportTab").html("暂无数据");
    $("#detailReportTabTotal").text("共"+result.beans.length+"条记录");
	tipModal("icon-cross", result.returnMessage);
}
//获取当前日期
function getToday(){
    var today = "";
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
    return today;
}
