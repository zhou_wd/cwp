$("#btnSearchCard").on("click",function(){
    var queryId = $("#queryId").val();
    if(queryId=="" || queryId==undefined){
        tipModal('icon-cross',"请选择查询类别");
        return;
    }
    getQueryResult();
});
//获取床位分配信息
function getQueryResult(){
    // PostForm("/cwp/front/sh/pay!execute","uid=P233","",printInfo,"","frmSearch");
    $.getJSON('/cwp/src/json/system/pay/pay_P233.json',function(data){
        printInfo(data);
    });
}

function printInfo(result){
    $("#msg").html(result.returnMessage);
    var info = result.bean==null?result.beans:result.bean;
    $("#resultInfo").html(JSON.stringify(info));
}

$('#msg').slimScroll({
    width: 'auto', //可滚动区域宽度
    height: '100px', //可滚动区域高度
    size: '5px', //组件宽度
});

$('#resultInfo').slimScroll({
    width: 'auto', //可滚动区域宽度
    height: '400px', //可滚动区域高度
    size: '5px', //组件宽度
});