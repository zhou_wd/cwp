//切换分类
var tabId = 0;
getPardRoomInfo(1);
$("#myTabs li").on("click", function () {
	isPostBack=true;
	tabId = $(this).index();
	if(tabId == 0)
		getPardRoomInfo(1);
	else
		getParkBedInfo(1);
});
//查询
$("#btnSearchApartment").on("click",function(){
	isPostBack=true;
	if(tabId == 0)
		getPardRoomInfo(1);
	else
		getParkBedInfo(1);
});
//获取房间信息
function getPardRoomInfo(pageIndex){
    var loaderRealtyTab = new LoaderTable("apartmentTabPagination","apartmentTab","/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis008",pageIndex,10,parkRoom,"","frmSearch");
    loaderRealtyTab.Display();
}

function paginationCallback(page_index){
	if(tabId == 0){
		if(!isPostBack) getPardRoomInfo(page_index+1);
	}else{
		if(!isPostBack) getParkBedInfo(page_index+1);
	}
}

function parkRoom(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>房间号</th>"+
        "<th>描述</th>"+
        "<th>房间类型</th>"+
        "<th>性别</th>"+
        "<th>创建时间</th>"+
        "<th>修改时间</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
    	if(result.beans[i].sex==0){
    		var sexZh = "男";
    	}else{
    		var sexZh = "女";
    	}
        srepairarray = result.beans;
        tbodys += "<tr>" +
            "<td>" +  result.beans[i].roomNumber + "</td>" +
            "<td>" +  result.beans[i].roomDesc + "</td>" +
            "<td>" +  result.beans[i].roomType + "</td>" +
            "<td>" +  sexZh + "</td>" +
            "<td>" +  result.beans[i].createDate + "</td>" +
            "<td>" +  result.beans[i].updateDate + "</td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#apartmentTab").html(thead+tbodys);
}
//获取申请记录
function getParkBedInfo(pageIndex){
    var roomNum = $("#roomNum").val();
    var sex = $("#sex").val();
    var loaderRealtyTab = new LoaderTable("apartmentTabPagination","apartmentTab","/cwp/front/sh/bedapplyAnalysis!execute","uid=bedapplyAnalysis009&roomNum="+roomNum+"&sex="+sex,pageIndex,10,parkBed,"");
    loaderRealtyTab.Display();
}

function parkBed(result){
    var thead="<thead>"+
        "<tr>"+
        "<th>床位号</th>"+
        "<th>标识</th>"+
        "<th>性别</th>"+
        "<th>创建时间</th>"+
        "<th>修改时间</th>"+
        "</tr>"+
        "</thead>";
    var tbodys= "<tbody>";
    for(var i=0;i<result.beans.length;i++) {
    	if(result.beans[i].bunkBed==0){
    		var bunk = "下铺";
    	}else{
    		var bunk = "上铺";
    	}
    	if(result.beans[i].sex==0){
    		var sexZh = "男";
    	}else{
    		var sexZh = "女";
    	}
        srepairarray = result.beans;
        tbodys += "<tr>" +
            "<td>" +  result.beans[i].bedNumber + "</td>" +
            "<td>" +  bunk + "</td>" +
            "<td>" +  sexZh + "</td>" +
            "<td>" +  result.beans[i].createDate + "</td>" +
            "<td>" +  result.beans[i].updateDate + "</td>" +
            "</tr>"
    }
    tbodys+= "</tbody>";
    $("#apartmentTab").html(thead+tbodys);
}