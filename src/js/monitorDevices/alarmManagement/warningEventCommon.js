﻿/**
 *@author chenqiuxu
 *@created 2016/05/31
 *加载页面时需要执行的方法
 */

//初始化告警类型和告警管理列表
$(function() {
    //故障告警列表
    faultAlarmList(1);
    //加载告警类别下拉框
    // warningDictionaryType();

});

//故障告警查询点击事件
$("#btnSearchFaultAlarm").on("click",function(){
    isPostBack=true;
    faultAlarmList(1);
});


var eventProcessStatus=1;
$("#myTabs li").on("click", function () {
    isPostBack=true;
    eventProcessStatus=$(this).index()+1;
    $(".bg-date").val("");
    $("#diName").val("");
    faultAlarmList(1);

});

//
////故障告警
function faultAlarmList(pageIndex){
    var diName=$("#diName").val();
    var startApplyTime=$("#startApplyTime").val();
    var endApplyTime=$("#endApplyTime").val();
    var eventDescribe=$("#eventDescribeFault").val();
    if("undefined" == typeof startApplyTime){
        startApplyTime="";
    }
    if("undefined" == typeof endApplyTime){
        endApplyTime="";
    }
    if("undefined" == typeof eventDescribe){
        eventDescribe="";
    }
    if(diName==null){
        var diName="";
    }

    $.getJSON('/cwp/src/json/warning/warningEvent_c001.json',function(data){
        var loaderFaultAlarmTab = new LoaderTable("faultAlarmLoader","faultAlarmTab","/cwp/front/sh/warningEvent!execute","uid=c001_2&startApplyTime="+startApplyTime+"&endApplyTime="+endApplyTime+"&dictValue="+diName+"&eventProcessStatus="+eventProcessStatus+"&eventType=1&eventDescribeFault="+eventDescribe,pageIndex,10,faultAlarmResultArrived,"","frmSearch");
        loaderFaultAlarmTab.loadSuccessed(data);
    })



    // loaderFaultAlarmTab.Display();
}


////发送告警信息回调函数
function faultAlarmResultArrived(result)
{
    var thead_tr="";
    if(result.beans[0].eventProcessStatus!="3"){
        thead_tr="<th>时间差</th>";
    }else{
        thead_tr="";
    }
    var thead="<thead>"+
        "<tr>"+
        "<th>告警编号</th>"+
        "<th>类别</th>"+
        "<th>位置</th>"+
        "<th>来源</th>"+
        "<th>告警级别</th>"+
        "<th>故障描述</th>"+
        "<th>时间</th>"+
        //result.beans[0].eventProcessStatus!="3"?"<th>时间差</th>":""
        thead_tr+
        "<th>状态</th>"+
        "<th>操作</th>"+
        "</tr>"+
        "</thead>";
    //拼接表格列表html
    var tbodys = "<tbody>";
    for(var i=0;i<result.beans.length;i++){
        var orsystem="";
        var state="";
        var level="";
        if(result.beans[i].eventFromSystem=="1"){
            orsystem="系统";
        }else{
            orsystem="手动";
        }
        if(result.beans[i].warningLevel=="3"){
            level="紧急"
        }else if(result.beans[i].warningLevel=="2"){
            level="高"
        }else if(result.beans[i].warningLevel=="1"){
            level="中"
        }else if(result.beans[i].warningLevel=="0"){
            level="低"
        }

        var state_td="";
        if(result.beans[i].eventProcessStatus=="1"){
            state="未处理";
            state_td="<td>"+result.beans[i].datedifferent+"</td>";
        }else if(result.beans[i].eventProcessStatus=="2"){
            state="处理中";
            state_td="<td>"+result.beans[i].datedifferent+"</td>";
        }else if(result.beans[i].eventProcessStatus=="3"){
            state="已处理";
            state_td="";
        }

        tbodys+="<tr>" +
            "<td>"+result.beans[i].alarmNumber+"</td>"+
            "<td>"+result.beans[i].deviceName+"</td>"+
            "<td>"+result.beans[i].buildingName+"</td>"+
            "<td>"+orsystem+"</td>"+
            "<td>"+level+"</td>"+
            "<td>"+result.beans[i].eventDescribe+"</td>"+
            "<td>"+result.beans[i].applyTime+"</td>"+
            state_td+
            "<td>"+state+"</td>"+
            "<td><a class='t-btn t-btn-sm t-btn-blue t-btn-deal' onclick=detailSelect('"+result.beans[i].warningEventId+"')>详情</a></td></tr>";
    }
    tbodys += "</tbody>";
    //显示表格html
    $("#faultAlarmTab").html(thead+tbodys);
}



/*
 * post提交数据
 * 发送地址，uid，action行为，成功回调函数，失败回调函数
 *@author cheqniuxu
 *@created 2016/05/31
 *通过字典表查询字典数据表告警类别
 * */
function warningDictionaryType(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_5&dictKey=intelligentType&dictdataKey='security','building_services'", "", WarningDictionarytResultArrived, '',"frmMain");
}

function WarningDictionarytResultArrived(result) {
    var sel = $("#diName");
    var option = ""
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for (var i = 0; i < result.beans.length; i++) {
        if (result.beans[i].dictdataValue != "1004001") {
            option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
            sel.append(option);
        }
    }
}



//当前页，加载容器
function paginationCallback(page_index){
    if(!isPostBack) faultAlarmList(page_index+1);
}



