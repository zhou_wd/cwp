﻿/**
 * 点击详情
 * @param warningEventId   告警事件id
 */

var modalHtml="";
$(function(){
    modalHtml=$("#modalDetailsAlarm").html();
});

function detailSelect(warningEventId){
    $("#modalDetailsAlarm").html(modalHtml);
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c003_3&warningEventId="+warningEventId, "", detailWarningById, '');

    $.getJSON('/cwp/src/json/warning/warningEvent_c003.json',function(data){
        detailWarningById(data);
    })

}


function detailWarningById(result){
    if(result.returnCode == 0){

        var state="";
        var warningLevelHtml="";
        //当前事件状态
        if(result.bean.eventProcessStatus=="1"){
            state="未处理"
        }else if(result.bean.eventProcessStatus=="2"){
            state="处理中"
        }else if(result.bean.eventProcessStatus=="3"){
            state="已处理"
        }
        //设备类型
        var deviceName="";
        switch (result.bean.warningType){
            case "1003185":
                deviceName="空调";
                break;
            case "1003186":
                deviceName="新风";
                break;
            case "1003187":
                deviceName="排污泵";
                break;
            case "1004":
                deviceName="门禁";
                break;
            case "1005":
                deviceName="照明";
                break;
            case "1006":
                deviceName="能耗";
                break;
            case "1010":
                deviceName="视频";
                break;
        }


        //$('#deviceType').html(result.bean.deviceName==""?"无":result.bean.deviceName);
        $('#deviceType').html(deviceName);

        //设备状态
        $('#deviceState').html(state);
        //告警级别warningLevel

        //当前责任人及角色
        var noticeNameArray=result.bean.appPush.split(',');
        if(result.bean.responseName!=""){
            $(".responsibe-name").html(result.bean.responseName);
            $(".responsibe-role").html(result.bean.response.split(',')[0]);
            $(".responsibe-phone span").html(result.bean.responsePhone);
        }
        else{
            $(".responsibe-name").html("暂无人接单");
            //$(".responsibe-name").html(noticeNameArray[1].split(':')[1]);
            //$(".responsibe-role").html(noticeNameArray[1].split(':')[0]);
        }

        //人员详情ID
        //$(".btn-user-info").attr("id",result.bean.responseOfficerId);

        //告警编号
        $("#alarmNumber").html(result.bean.alarmNumber==""?"无":result.bean.alarmNumber);
        //告警级别
        if(result.bean.warningLevel==0){
            warningLevelHtml="<i class='t-tag-del'>低</i>"
        }

        switch (result.bean.warningLevel){
            case "0":
                warningLevelHtml="<i class='t-tag-del'>低</i>";
                break;
            case "1":
                warningLevelHtml="<i class='t-tag-yellowColor'>中</i>";
                break;
            case "2":
                warningLevelHtml="<i class='t-tag-ing'>高</i>";
                break;
            case "3":
                warningLevelHtml="<i class='t-tag-red'>紧急</i>";
                break;
        }

        $("#messageLevel").html(warningLevelHtml);

        $('#devicePostion').html(result.bean.buildingName==""?"无":result.bean.buildingName);

        $('#warningDescribe').html(result.bean.eventDescribe==""?"无":result.bean.eventDescribe);

        $('#handleOpinion').html(result.bean.handleOpinion==""?"未完成":result.bean.handleOpinion);

        if(result.bean.applyTime!=""){
            $('#noticeTime').html(result.bean.applyTime);
            $('#noticeName').html(noticeNameArray[1]+"<br/>"+noticeNameArray[0]);
        }

        if(result.bean.responseTime!=""&&result.bean.responseName!=""){
            $('#responseTime').html(result.bean.responseTime);
            $('#responseName').html(result.bean.response.split(',')[0]+"："+result.bean.responseName);
        }

        if(result.bean.handleTime!=""){
            $('#handlerTime').html(result.bean.responseTime);
            $('#handlerName').html(result.bean.response.split(',')[0]+"："+result.bean.responseName);
        }

        if(result.bean.eventProgress=="2"){
            $('.bubble').eq(1).removeClass("comp");
            $('.com_squ').eq(1).removeClass("conp");
        } else if(result.bean.eventProgress == "3"){

            $('.bubble').eq(1).removeClass("comp");
            $('.bubble').eq(2).removeClass("comp");
            $('.bubble').eq(3).removeClass("comp");
            $('.com_squ').eq(1).removeClass("conp");
            $('.com_squ').eq(2).removeClass("conp");
            $('.com_squ').eq(3).removeClass("conp");
            $('.line .end').removeClass("conp");
            $('#completeTime').html(result.bean.handleTime);
            $('#completeResultStaus').html("已完成");
            $('#completeResult').html("")

        } else if(result.bean.eventProgress == "4"){
            $('.bubble').eq(1).removeClass("comp");
            $('.bubble').eq(2).removeClass("comp");
            $('.bubble').eq(3).removeClass("comp");
            $('.com_squ').eq(1).removeClass("conp");
            $('.com_squ').eq(2).removeClass("conp");
            $('.com_squ').eq(3).removeClass("conp");
            $('.line .end').removeClass("conp");
            $('#completeTime').html(result.bean.handleTime);
            $('#completeResultStaus').html("已完成");
            $('#completeResult').html("转工单")

        }
        //getStaffRole(result.bean.warningCategoryId);
        $("#modalDetailsAlarm").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal', centerModal());
        $(window).on('resize', centerModal());
    }
}

function getStaffRole(warningCategoryId){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c008&warningCategoryId="+warningCategoryId,"", StaffRoleResultArrived, '');
}


function StaffRoleResultArrived(result){
    var appInfo="";
    for(var i=0;i<result.beans.length;i++){
        appInfo+=result.beans[i].roleName+":"+result.beans[i].appPush+"</br>"
    }
    $('#noticeName').html(appInfo);
}


/*
 * post提交数据
 * 发送地址，uid，action行为，成功回调函数，失败回调函数
 *@author cheqniuxu
 *@created 2016/05/31
 *通过字典表查询字典数据表告警类别
 * */

$(function() {
    //warningDictionaryType();
    //验证
    eventValidtor();
});

//function warningDictionaryType(){
//    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_5&dictKey=intelligentType&dictdataKey='security','building_services'", "", WarningDictionarytResultArrived, '');
//}
//
//function WarningDictionarytResultArrived(result){
//    var sel = $("#diName");
//    var option = ""
//    sel.empty();
//    option = $("<option>").text("请选择").val("");
//    sel.append(option);
//    for ( var i = 0; i <   result.beans.length; i++) {
//        option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
//        sel.append(option);
//    }
//}

//-----------------新增开始--------------------------//
//获取设备类别
function addWarningDictionaryType(){
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_5&dictKey=intelligentType&dictdataKey='security','building_services'", "", AddWarningDictionarytResultArrived, '');
    $.getJSON('/cwp/src/json/warning/warningEvent_c002_5.json',function(data){
        AddWarningDictionarytResultArrived(data);
    })
}
//获取设备类别回调函数
function AddWarningDictionarytResultArrived(result){
    var sel = $("#addWaringType");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        if (result.beans[i].dictValue != "1004"){
            option = $("<option>").text(result.beans[i].dictName).val(result.beans[i].dictValue);
            sel.append(option);
        }
    }
}

//选择设备显示楼栋
$("#addWaringType").on("change",function(){
    $("#buildingNo").val("");
    $(".buildingChildLi").addClass("hide");
    $("#buildingChild").html("<option>请选择</option>");
    $(".buildingRoomLi").addClass("hide");
    $("#buildingRoom").html("<option>请选择</option>");
    $(".devicesLi").addClass("hide");
    $("#device").html("<option>请选择</option>");
    var result=$(this).val();
    if(result!=""){
        $(".buildingNoLi").removeClass("hide");
    }
});

//物业管理-新增
////查找所有楼栋 chenqiuxu
function buildingQuary(){
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c004_3", "", buildingResultArrived, '');
    $.getJSON('/cwp/src/json/warning/warningEvent_c004_3.json',function(data){
        buildingResultArrived(data);
    })
}
//获取所有楼栋并拼接html
function buildingResultArrived(result){
    var sel = $("#buildingNo");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
        sel.append(option);
    }
}

//根据楼，显示层，显示房间，三级联动
$("#buildingNo").on("change",function(){
    var parentId=$(this).val();
    if(parentId!=""){
        PostForm("/cwp/front/sh/warningEvent!execute", "uid=c005_2&parentId="+parentId, "", bParentResultArrived, '');
    }else{
        $(".buildingChildLi").addClass("hide");
    }
});

//显示层
function bParentResultArrived(result){
    $(".buildingChildLi").addClass("hide");
    $("#buildingChild").html("<option>请选择</option>");
    $(".buildingRoomLi").addClass("hide");
    $("#buildingRoom").html("<option>请选择</option>");
    $(".devicesLi").addClass("hide");
    $("#device").html("<option>请选择</option>");
    if(result.returnCode=="0" && result.beans.length>0){

        var sel = $("#buildingChild");
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for ( var i = 0; i <   result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
            sel.append(option);
        }
        $(".buildingChildLi").removeClass("hide");
    }
    else{
        alert("查询失败，该建筑无相应设备");

    }

}
//显示房间
$("#buildingChild").on("change",function(){
    var count = $(this).val();
    $(".buildingRoomLi").addClass("hide");
    $("#buildingRoom").html("<option>请选择</option>");
    $(".devicesLi").addClass("hide");
    $("#devices").html("<option>请选择</option>");
    var deviceType=$("#addWaringType").val();
    if(count!=""){
        var parentId = $(this).children('option:selected').val();
        //获取楼层
        PostForm("/cwp/front/sh/warningEvent!execute", "uid=c005_2&parentId="+parentId, "", bRoomResultArrived, '');
        PostForm("/cwp/front/sh/device!execute", "uid=d014_2&buildingId="+parentId+"&deviceType="+deviceType, "", deviceBuildingResultArrived, '');
    }else{
        $(".buildingRoomLi").addClass("hide");
        $("#buildingRoom").html("<option>请选择</option>");
    }
});
//选择层后，加载楼层，加载设备

//显示房间
function bRoomResultArrived(result){
    if(result.beans.length>0 && result.returnCode=="0"){
        var sel = $("#buildingRoom");
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for ( var i = 0; i <   result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
            sel.append(option);
        }
        $(".buildingRoomLi").removeClass("hide");
    }
    else{
        var buildingId=$("#buildingChild").val();
        var deviceType=$("#addWaringType").val();
        if(buildingId!="" && deviceType!="" && $.formValidator.IsOneValid('addWaringType')){
            PostForm("/cwp/front/sh/device!execute", "uid=d014_2&buildingId="+buildingId+"&deviceType="+deviceType, "", deviceResultArrived, '');
        }
    }

}

/**
 *读取设备
 * 1、判断房间是否有值
 * 如果房间被选择，开始进行请求数据
 */

$(function () {
    $("#buildingRoom").change(function(){
        var buildingId = $(this).val();
        var deviceType = $("#addWaringType").val();
        if(buildingId != "" && deviceType != null){
            PostForm("/cwp/front/sh/device!execute", "uid=d014_2&buildingId="+buildingId+"&deviceType="+deviceType, "", deviceResultArrived, '');
        }else{
            $(".devicesLi").addClass("hide");
            $("#device").html("<option>请选择</option>");
        }
    })
});

//根据楼层显示所有设备
function deviceBuildingResultArrived(result){
    if(result.beans.length>0 && result.returnCode == 0){
        var sel = $("#devices");
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for ( var i = 0; i <   result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].devicePositionCode).val(result.beans[i].deviceId);
            sel.append(option);
        }
        $(".devicesLi").removeClass("hide");
    }
}

//显示所有设备
function deviceResultArrived(result){
    if(result.beans.length>0 && result.returnCode == 0){
        var sel = $("#devices");
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for ( var i = 0; i <   result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].devicePositionCode).val(result.beans[i].deviceId);
            sel.append(option);
        }
        $(".devicesLi").removeClass("hide");
    }else{
        alert(result.returnMessage);
        $("#devices").html("<option>请选择</option>");
        $(".devicesLi").addClass("hide");
    }

}

//验证
function eventValidtor()
{
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });

    $("#addDetailDesc").formValidator({
        onfocus: "请输入故障描述",
        oncorrect: " "
    }).InputValidator({
        min:4,
        max: 100,
        onempty: "请输入故障描述",
        onerror: "描述长度为4-100个字符"
    });

    $("#addWaringType").formValidator({
        onfocus:"请选择设备类别",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择设备类别",
        onerror: "请选择设备类别"
    });

    $("#addLevel").formValidator({
        onfocus:"请选择告警级别",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择告警级别",
        onerror: "请选择告警级别"
    });

    $("#buildingNo").formValidator({
        onfocus:"请选择楼栋",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择楼栋",
        onerror: "请选择楼栋"
    });

    $("#buildingChild").formValidator({
        onfocus:"请选择楼层",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择楼层",
        onerror: "请选择楼层"
    });

    $("#devices").formValidator({
        onfocus:"请选择设备",
        oncorrect:"&nbsp;"
    }).InputValidator({
        min:1,
        onempty:"请选择设备",
        onerror: "请选择设备"
    });
}

//添加告警事件
function saveWarningEvent() {
    //判断表单验证是否通过
    if ($.formValidator.PageIsValid()) {
        var applierId = localStorage.getItem("parkId");
        PostForm("/cwp/front/sh/warningEvent!execute", "uid=c006_2&applierId="+applierId+"&eventProgress=1&eventType=1", "", saveWarningEventResultArrived, '',"frmMain");
        //关闭模态框
        $("#modalMaintenanceNew").modal("hide");
    }
}
//saveWarningEventResultArrived
function saveWarningEventResultArrived(result){
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "添加成功");
            isPostBack=true;
            faultAlarmList(1);
            break;
        case "2":
            tipModal("icon-cross", "添加失败");
            break;
        case "-9999":
            tipModal("icon-cross", "添加失败");
            break;
    }
}
////绑定点击事件
//新建
$("#btnMaintenanceNew").on("click", function () {
    //告警类型
    addWarningDictionaryType();
    buildingQuary();
    $(".tipArea div").html("").attr("class","");
    $("#modalMaintenanceNew select").val("");
    $("#addDetailDesc").val("");
    $("#buildingNo").val("");
    $(".buildingNoLi").addClass("hide");
    $(".buildingChildLi").addClass("hide");
    $("#buildingChild").html("<option>请选择</option>");
    $(".buildingRoomLi").addClass("hide");
    $("#buildingRoom").html("<option>请选择</option>");
    $(".devicesLi").addClass("hide");
    $("#device").html("<option>请选择</option>");
    $("#modalMaintenanceNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    //}).on('show.bs.modal', centerModal());
    //$(window).on('resize', centerModal());
});
//-------------新增结束---------------------//



