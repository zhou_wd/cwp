var mapid = "";
var deviceid ="";
var deviceType="";
var videoIP="";
//时间戳
var outVideoHtml="";


//初始化楼栋
$(function(){
    $(document).off("click",".left-menu dt");
    getMenuList();
    outVideoHtml=$("#objVlc").html();
});

//获取楼层菜单
function getMenuList(){
    // PostForm("/cwp/front/sh/intebuilding!execute", "uid=intelligentbuilding001_2", "", outputMenuList, '',"");


    $.getJSON('/cwp/src/json/build.json', function(data) {
        outputMenuList(data);
    });
}
//输出楼层菜单
function outputMenuList(result){
    if(result.returnCode == 0){
        template.helper('numFormat', function(inp) {
            if(inp == 2){
                return "cur";
            }
        });
        var outHtml = template("menuListTmpl", result);
        $("#floorMenuList").html(outHtml);
        template.helper('mapFormat', function(inp) {
            if(inp == 2){
                return "cur animated fadeInUp";
            }
        });
        template.helper('mapImgFormat', function(inp) {
            if(inp == 1){
                return "../../../assets/img/building/floor-0.png";
            }else{
                return "../../../assets/img/building/floor-1.png";
            }
        });
        var outHtml = template("mapTmpl", result);
        $("#floor-list").html(outHtml);
        getDevice(result.beans[1].buildingId);

    }else{
        tipModal("icon-cross", "获取楼层失败");
    }
}
//获取所有子设备
function getDevice(floorId){
    mapid = floorId;
    // PostForm("/cwp/front/sh/device!execute", "uid=d001_3&pageSize=100&currentPage=1&deviceTypeId=1010&buildingId="+floorId, "", outputDevice, '');
    $.getJSON('/cwp/src/json/device.json',function(data){
        outputDevice(data);
    });
}
//输出有所子设备
function outputDevice(result){
    if(result.returnCode  == 0){
        result.beans.sort(sotrObj("room"));
        var outHtml = template("deviceTmpl", result);
        $("#deviceList").html(outHtml);
        var astr = "";
        var imgurl
        for(var i=0;i<result.beans.length;i++){
            if (result.beans[i].type == 1){
                imgurl = "shipin1-2.png";
            }else if(result.beans[i].type == 2) {
                imgurl = "shipin1-1.png";
            }else if(result.beans[i].type == 3) {
                imgurl = "shipin1-3.png";
            }
            astr += "<a class='shipin' title="+result.beans[i].devicePositionDescribe+" data-port=‘"+result.beans[i].devicePort+"’ data-id='"+result.beans[i].deviceId +"' data-ip='"+result.beans[i].controllerip +"'  data-interfaceaddr='"+result.beans[i].interfaceaddr +"'   title="+result.beans[i].devicePositionDescribe+" style='left:"+result.beans[i].deviceX+"%;top:"+ result.beans[i].deviceY +"%'><img src='/cwp/src/assets/img/"+ imgurl +"'/> </a>"

        }
        $("#bf2cac1e-d440-4e22-a0e4-a32c3ac34036").html(astr);
        //搜索框 搜索设备
        // var deviceAddr = $("#btn-search-device").val();
        // if(deviceAddr != ""){
            // searchDevice(deviceAddr);
        // }

    }else{
        tipModal("icon-cross", "获取设备失败");
    }
}

function animateFloor(floorId,action){
    $("#floor"+floorId).addClass("cur animated fadeInUp");
    $("#device-list").addClass("animated fadeInRight");
}



//楼层切换
$(document).on("click",".floor-item",function(){
    var floorId = $(this).attr("data-id");
    $(".floor-item").removeClass("cur");
    $(this).addClass("cur");
    $(".info-box").hide();
    $(".info-box").removeClass("show");
    $("#hidDeviceID").val("");
    //显示对应楼层
    getDevice(floorId);
    $("#floor-list .floor-image").removeClass("cur animated fadeInUp");

    $("#floor"+floorId).addClass("cur animated fadeInUp");
    $("#device-list").addClass("animated fadeInRight");
    $("#deviceInfo").find("li").remove();
    $(".t-building").removeClass("t-building-open");

    setTimeout(function() {
        $("#device-list").removeClass("animated fadeInRight");
    }, 1000);
});

//点击设备点查看设备详情
//给坐标点绑定点击事件 根据设备ID查看设备详情
$(document).on("click",".shipin",function(){
    $(".shipin").removeClass("cur");
    $(this).addClass("cur")
    var deviceId = $(this).attr("data-id");
    $("#btnCloseList").attr("data-id",deviceId);
    //关联选择设备编号列表
    $("#deviceList").find("a").each(function(){
        var iconId = $(this).attr("data-id");
        if(deviceId == iconId){
            $(this).addClass("cur");
        }else{
            $(this).removeClass("cur");
        }
    });

    //点击弹出监控视频
    var ip = $(".shipin.cur").attr("data-ip");
    //绑定门禁IP及门禁
    getdeviceInfo(deviceId);
});


//设备列表单击事件
$(document).on("click",".device-item",function(){
    $(".device-item").removeClass("cur");
    $(this).addClass("cur");
    var deviceId = $(this).attr("data-id");
    $("#btnCloseList").attr("data-id",deviceId);
    $("#floor-list").find("a").each(function(){
        var iconId = $(this).attr("data-id");
        if(deviceId == iconId){
            $(this).addClass("cur");
        }else{
            $(this).removeClass("cur");
        }
    });
    getdeviceInfo(deviceId);
    //点击弹出监控视频
    //var ip = $(".shipin.cur").attr("data-ip");
    //var port= $(".shipin.cur").attr("data-port");
    //var url = "rtsp://"+ip+"/h264";
    //var url = "rtsp://"+ip+"/"+port+"/h264";
    //newVideo(url);
});

//获取设备信息
function getdeviceInfo(deviceId){
    // PostForm("/cwp/front/sh/intebuilding!execute", "uid=i003_2&deviceId="+deviceId, "", outputDeviceInfo, '',"");
    $("#videoTip").modal({
            keyboard: true,
            backdrop: 'static'
    }).on('show.bs.modal');
}

function outputDeviceInfo(result){
    if(result.returnCode == 0){
        //显示详情
        template.helper('deviceFormat', function(inp) {
            if(inp == 2){
                return "枪式摄像机";
            }
            else if(inp == 1){
                return "半球摄像机";
            }
            else if(inp == 3){
                return "全球摄像机";
            }
        });
        deviceType = result.bean.type;
        videoIP = result.bean.videoIP;
        var outHtml = template("deviceInfoTmpl", result);
        $("#deviceInfo").html(outHtml);
        if(deviceType == 2){
            $("#controlVideo").hide();
        }else{
            $("#controlVideo").show();
        }
        //填充表单rtspPlayAddr rtmpPlayAddr
        var sourceVideoIp = result.bean.controllerip;
        var appId="app"+ new Date().getTime();
        var rtspPlayAddr = "rtsp://"+sourceVideoIp+":554/h264";
        var rtmpPlayAddr = "rtmp://"+rtmpVideoIp+"/live/";


        $("#sourceVideoIp").val(sourceVideoIp);
        $("#rtspPlayAddr").val(rtspPlayAddr);
        $("#rtmpPlayAddr").val(rtmpPlayAddr);
        $("#appId").val(appId);

        //显示模态框
        $("#videoTip").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal');
        $(".loading-box").show();
        $("#iframeLoading").show();
        $(".progress .progress-bar").progressbar({
            display_text: 'center',
            done:function(){
                $(".loading-box").hide();
                $("#iframeLoading").hide();
            }
        });

        //newVideo();
        //发布视频，请求转流
        pushVideoApp();
    }else{
        tipModal("icon-cross", "获取视频数据失败");
    }
}


function pushVideoApp(){
    PostForm("/cwp/front/sh/ipg!execute", "uid=ipg007", "post", pushVideoAppSuccess, '',"frmMain");
}

//请求视频流回调
function pushVideoAppSuccess(result){
    // var url = $("#rtmpPlayAddr").val()+$("#appId").val();
    //播放rtmp外网地址
    var url ="rtmp://"+public_rtmpVideoIp+"/live/"+$("#appId").val();
    if(result.returnCode == 0){
        newVideo(url);
    }else{
        tipModal("icon-cross", "获取数据失败");
    }
}



//搜索
$("#btn-search-device").on("input",function(){
    var keyword=$(this).val();
    searchDevice(keyword);
});

//搜索框
function searchDevice(keyword){
    if(keyword == ""){
        $(".device-item").parent().show();
    }
    else{
        $(".device-item").each(function(){
            var text=$(this).text();
            if(text.indexOf(keyword) > -1){
                $(this).parent().show();
            }else{
                $(this).parent().hide();
            }

        });
    }
}

//可视化组件
$('#deviceList').slimScroll({
    width: 'auto', //可滚动区域宽度
    height: '500px', //可滚动区域高度
    size: '5px', //组件宽度
    alwaysVisible: true
});


//动态加载视频
function newVideo(url) {
    var myPlayer = videojs('my_video');
    videojs('my_video').ready(function(){
        window.myPlayer = this;
        // myPlayer.src("rtmp://live.hkstv.hk.lxdns.com/live/hks");
        myPlayer.src(url);
        myPlayer.play();
    });
}


//模态框关闭事件
$("#videoTip").on('hidden.bs.modal',function(){
    //清理播放器，重置播放组件
    //if(window.myPlayer!=undefined){
    //    videojs('my_video').dispose();
    //    $("#objVlc").html(outVideoHtml);
    //}
    videojs('my_video').dispose();
    $("#objVlc").html(outVideoHtml);

    $("#hidDeviceID").val("");
    $(".t-building").removeClass("t-building-open");
    $(".info-box").removeClass("show");
    var deviceId = $("#btnCloseList").attr("data-id");
    $("#deviceList").find("a").removeClass("cur");
    $("#floor-list").find("a").removeClass("cur");
    $("#deviceInfo").html("");
    //清空进度条
    $(".loading .progress-bar").css("width","0");
    //关闭视频
    PostForm("/cwp/front/sh/ipg!execute", "uid=ipg008", "post", deleteVideoSuccess, '',"frmMain");

});

//关闭视频回调
function deleteVideoSuccess(result){
    if(result.returnCode == 0){
        // alert("视频流关闭成功");
    }else{
        // alert("视频流关闭失败");
    }
}



//楼层容器自适应
$(window).resize(function(){
    autoWidth();
});


function autoWidth(){
    var w=$(".floor-image.cur img")[0].offsetWidth;
    $(".floor-image.cur").css("width",w+"px");
}

////通过IP摄像头控制
$(".controlVideo-btn").click(function(){
    var operation = $(this).attr("data-val");
    var id = $("#deviceId").text().trim();
    if(operation!=""){
        $.ajax({
            timeout : 20000,
            type:"get",
            url: ipg_url+"/ipg/video/videoCtrl",
            async:true,
            dataType: "json",
            data:{
                id:id,
                operation:operation,
                videoIP:videoIP,
                speed:'7'
            },
            success:function (result) {
                console.log(result);
            },
            error:function (result) {
                console.log(result);
            }
        });

    }
});

//通过端口监控摄像头
//$(".controlVideo-btn").click(function(){
//    var operation = $(this).attr("data-val");
//    var id = $("#deviceId").text().trim();
//    var port = $(".shipin.cur").attr("data-port");
//    if(operation!=""){
//        $.ajax({
//            timeout : 20000,
//            type:"get",
//            url: ipg_url+port+"/ipg/video/videoCtrl",
//            async:true,
//            dataType: "json",
//            data:{
//                id:id,                //ip
//                operation:operation,  //控制
//                speed:'7'             //移动速度
//            },
//            success:function (result) {
//                console.log(result);
//            },
//            error:function (result) {
//                console.log(result);
//            }
//        });
//
//    }
//});

//	排序场景
function sotrObj(propertyName){
    return function(object1,object2){
        var value1 = object1[propertyName].split("区")[0];
        var value2 = object2[propertyName].split("区")[0];
        if(value1 > value2){
            return 1;
        }else if(value1 < value2){
            return -1;
        }else{
            return 0;
        }
    }
}