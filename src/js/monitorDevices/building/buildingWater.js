var mapid = "";
var deviceTypeId = '1003189';
var deviceBeansData;
var deviceBeansToolData;
//获取所有子设备
function getDevice(floorId){
    mapid = floorId;
    PostForm("/cwp/front/sh/device!execute", "uid=d001_3&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId+"&buildingId="+floorId, "", outputDeviceFn, '');
    function outputDeviceFn(result) {
        if(result.returnCode  == 0){
            deviceBeansData = result.beans;
            PostForm("/cwp/front/sh/device!execute", "uid=d001_3_2_1&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId+"&buildingId="+floorId, "", outputDevice, '');
        }else{
            tipModal("icon-cross", "获取设备失败");
        }
    }
}
//输出有所子设备
function outputDevice(result){
    if(result.returnCode  == 0){
        deviceBeansToolData = result.beans;
        result.beans = deviceBeansData;
        var astr = "";
        var imgurl;
        for(var i=0;i<result.beans.length;i++){
            var deviceState = result.beans[i].deviceState;
            if(deviceState == '0'){
                imgurl = "air4_1.png";
            }else if(deviceState == '1'){
                imgurl = "air4_2.png";
            }else{
                imgurl = "air4_3.png";
            }
            astr += "<a class='shipin' title='"+result.beans[i].devicePositionDescribe+"' data-port='"+result.beans[i].devicePort+"' data-id='"+result.beans[i].deviceId +"' data-ip='"+result.beans[i].controllerip +"'  data-interfaceaddr='"+result.beans[i].interfaceaddr +"' style='left:"+result.beans[i].deviceX+"%;top:"+ result.beans[i].deviceY +"%'><img src='/cwp/src/assets/img/airConditioner/"+ imgurl +"'/> </a>"
        }
        $("#" + mapid).html(astr);
        var deviceId = deviceBeansToolData[0].deviceId;
        for(i = 0;i < deviceBeansData.length;i ++){
            if(deviceBeansData[i].deviceId == deviceId){
                for(j = 0;j < deviceBeansToolData.length;j ++){
                    if(deviceBeansToolData[j].deviceId == deviceId){
                        deviceBeansData[i].toolData = deviceBeansToolData[j];
                        outputDeviceInfo(deviceBeansData[i]);
                        break;
                    }
                }
                break;
            }
        }
    }else{
        tipModal("icon-cross", "获取设备失败");
    }
}
function outputDeviceInfo(result){
    var outHtmlTH = template("deviceInfoTmplWaterTH", result);//编号、位置
    var outHtml1 = template("deviceInfoTmplWater1", result);//1#设备
    var outHtml2 = template("deviceInfoTmplWater2", result);//2#设备
    var outHtmlTop = template("deviceInfoTmplWaterTop", result);//温度、湿度
    if(result.deviceState == '-1'){
        $("#waterDetail").html('<span style="display:block;width:100%;text-align: center;">此设备"' + result.devicePositionCode + '"未开启</span>');
    }else{
        $("#waterDetailTH1,#waterDetailTH2").html(outHtmlTH);//编号、位置
        $("#waterDetail1").html(outHtml1);//1#设备
        $("#waterDetail2").html(outHtml2);//2#设备
        $("#waterDegree").html(outHtmlTop);//温度、湿度

        var wHeight = $(window).height();
        var pHeight = $(".waterRight .parameter").outerHeight();
        var lHeight = $(".waterImg .left").outerHeight();
        $(".waterImg").css({"height":(wHeight*0.95)-pHeight-130,"padding-top":(wHeight*0.95)-pHeight-180-lHeight});
        //当图片高度大于父盒子
        var mainHeight = $(".waterImg").outerHeight();
        var imgHeight = $(".waterImg img").outerHeight();
        if(imgHeight >= mainHeight){
            $(".waterImg img").css({"height":mainHeight,"width":"auto"});
        }else{
            $(".waterImg img").css({"width":"100%","height":"auto"});
        }

    }
}
// 获取地图的高度
$(window).resize(function(){
    autoResize();
});
function autoResize() {
    var wHeight = $(window).height();
    $(".wrapper").css({"height":wHeight});
    $(".waterMain").css({"margin-left":wHeight*0.4*0.85+10});
    $(".floor-out").css({"bottom":wHeight*0.05,"height":wHeight*0.6});
    var pHeight = $(".waterRight .parameter").outerHeight();
    var lHeight = $(".waterImg .left").outerHeight();
    if(pHeight>60){
        $(".waterImg").css({"height":(wHeight*0.95)-pHeight-130,"padding-top":(wHeight*0.95)-pHeight-180-lHeight});
        //当图片高度大于父盒子
        var mainHeight = $(".waterImg").outerHeight();
        var imgHeight = $(".waterImg img").outerHeight();
        //console.log(mainHeight,imgHeight);
        if(imgHeight >= mainHeight){
            $(".waterImg img").css({"height":mainHeight,"width":"auto"});
        }else{
            $(".waterImg img").css({"width":"100%","height":"auto"});
        }
    }
}
//楼层容器自适应
$(window).resize(function(){
    autoWidth();
    autoResize();
});
function autoWidth(){
    var w=$(".floor-image.cur img")[0].offsetWidth;
    $(".floor-image.cur").css("width",w+"px");   
}