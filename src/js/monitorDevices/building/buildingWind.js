var mapid = "";
var deviceTypeId = '1003186';
var deviceBeansData;
var deviceBeansToolData;

//获取所有子设备
function getDevice(floorId){
    mapid = floorId;
   
    // PostForm("/cwp/front/sh/device!execute", "uid=d001_3&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId+"&buildingId="+floorId, "", outputDeviceFn, '');
    $.getJSON('/cwp/src/json/device/device_wind_d001_3.json',function(data){
        outputDeviceFn(data)
    })
}
 function outputDeviceFn(result) {
        if(result.returnCode  == 0){
            deviceBeansData = result.beans;
            // PostForm("/cwp/front/sh/device!execute", "uid=d001_3_2_1&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId+"&buildingId="+floorId, "", outputDevice, '');
            $.getJSON('/cwp/src/json/device/device_wind_d001_3_2_1.json',function(data1){
                // var da = JSON.parse(data1);
                outputDevice1(data1);
            })
        }else{
            tipModal("icon-cross", "获取设备失败");
        }
    }

//输出有所子设备
function outputDevice1(result){
    if(result.returnCode  == 0){
        deviceBeansToolData = result.beans;
        result.beans = deviceBeansData;

        var astr = "";
        var imgurl;
        for(var i=0;i<result.beans.length;i++){
            var deviceState = result.beans[i].deviceState;
            if(deviceState == '0'){
                imgurl = "air2_1.png";
            }else if(deviceState == '1'){
                imgurl = "air2_2.png";
            }else{
                imgurl = "air2_3.png";
            }
            astr += "<a class='shipin' title='"+result.beans[i].devicePositionDescribe+"' data-port='"+result.beans[i].devicePort+"' data-id='"+result.beans[i].deviceId +"' data-ip='"+result.beans[i].controllerip +"'  data-interfaceaddr='"+result.beans[i].interfaceaddr +"' style='left:"+result.beans[i].deviceX+"%;top:"+ result.beans[i].deviceY +"%'><img src='/cwp/src/assets/img/airConditioner/"+ imgurl +"'/> </a>"
        }
        $("#" + mapid).html(astr);
    }else{
        tipModal("icon-cross", "获取设备失败");
    }
}

//点击设备点查看设备详情
//给坐标点绑定点击事件 根据设备ID查看设备详情
$(document).on("click",".shipin",function(){
    $(".shipin").removeClass("cur");
    $(this).addClass("cur");
    var deviceId = $(this).attr("data-id");

    getdeviceInfo(deviceId);
});

// //获取设备信息
function getdeviceInfo(deviceId){
    for(i = 0;i < deviceBeansData.length;i ++){
        if(deviceBeansData[i].deviceId == deviceId){
            for(j = 0;j < deviceBeansToolData.length;j ++){
                if(deviceBeansToolData[j].deviceId == deviceId){
                    deviceBeansData[i].toolData = deviceBeansToolData[j];
                    outputDeviceInfo(deviceBeansData[i]);
                    break;
                }
            }
            break;
        }
    }
}

function outputDeviceInfo(result){
    //显示详情
    var outHtml;
    var deviceTipImg = document.getElementById('deviceTipImg');

    outHtml = template("deviceInfoTmplWind", result);
    $('#deviceTipTitle').html('送排风机');

    if(result.toolData.fanRunningState == "开启"){
        deviceTipImg.src = '/cwp/src/assets/img/airConditioner/deviceWind2.gif';
    }else{
        deviceTipImg.src = '/cwp/src/assets/img/airConditioner/deviceWind.png';
    }

    if(result.deviceState == '-1'){
        $("#deviceInfo").html('<span style="display:block;width:100%;text-align: center;">此设备"' + result.devicePositionCode + '"未开启</span>');
    }else{
        $("#deviceInfo").html(outHtml);
    }
    //显示模态框
    $("#deviceTip").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('shown.bs.modal',centerModal());
}

//模态框关闭事件
$("#videoTip").on('hidden.bs.modal',function(){

    $("#floor-list").find("a").removeClass("cur");
    $("#deviceInfo").html("");
});

//楼层容器自适应
$(window).resize(function(){
    autoWidth();
});

function autoWidth(){
    var w=$(".floor-image.cur img")[0].offsetWidth;
    $(".floor-image.cur").css("width",w+"px");
}