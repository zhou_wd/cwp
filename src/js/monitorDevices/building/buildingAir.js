var mapid = "";
var deviceTypeId = '1003185';

//获取所有子设备
function getDevice(floorId){
    mapid = floorId;

    //输出所有子设备
    // PostForm("/cwp/front/sh/device!execute", "uid=d001_3&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId+"&buildingId="+floorId, "", outputDevice, '');
    $.getJSON('/cwp/src/json/device/deviceuidd001_3.json',function(data){
        outputDevice(data);
    })
}
//输出有所子设备
function outputDevice(result){
    if(result.returnCode  == 0){
        var astr = "";
        var imgurl;
        for(var i=0;i<result.beans.length;i++){
            var deviceState = result.beans[i].deviceState;
            if(deviceState == '0'){
                imgurl = "air1_1.png";
            }else if(deviceState == '1'){
                imgurl = "air1_2.png";
            }else{
                imgurl = "air1_3.png";
            }
            astr += "<a class='shipin' datastate='"+deviceState+"' title='"+result.beans[i].devicePositionDescribe+"' data-port='"+result.beans[i].devicePort+"' data-id='"+result.beans[i].deviceId +"' data-ip='"+result.beans[i].controllerip +"'  data-interfaceaddr='"+result.beans[i].interfaceaddr +"' style='left:"+result.beans[i].deviceX+"%;top:"+ result.beans[i].deviceY +"%'><img src='/cwp/src/assets/img/airConditioner/"+ imgurl +"'/> </a>"
        }
        $("#" + mapid).html(astr);
    }else{
        tipModal("icon-cross", "获取设备失败");
    }
}

//点击设备点查看设备详情
//给坐标点绑定点击事件 根据设备ID查看设备详情
$(document).on("click",".shipin",function(){
    var deviceState = $(this).attr("datastate");
    if(deviceState == "-1"){
        tipModal("icon-cross", "此设备未开启");
        return;
    }else{
        var deviceId = $(this).attr("data-id");
        getdeviceInfo(deviceId);
    }
});

//获取设备信息
function getdeviceInfo(result){

    $('#deviceToolBox li').removeClass('selected');
    $("#img-title").addClass("hide");
    $("#floorDeviceInfo").remove("li");

    var outHtml = template("deviceInfoTmplAirWait");
    $("#floorDeviceInfo").html(outHtml);

    $("#floor-list .floor-image").removeClass("cur animated fadeInUp");
    $("#floorDeviceInfo").addClass("cur animated fadeInUp");

    //获取设备信息
    var deviceBeansData;
    $.getJSON('/cwp/src/json/device/deviceuidd001_3.json', function(data){
                var resultMsg = data;
                if(resultMsg.returnCode  == 0){
            $.getJSON('/cwp/src/json/device/device_kongtiao_d001_3_2_1.json',function(data1){

                var resultTool = data1;
                if(resultTool.returnCode == 0){
                    for(i=0;i<resultMsg.beans.length;i++){
                        if(resultMsg.beans[i].deviceId == result){
                            deviceBeansData = resultMsg.beans[i];
                            for(j=0;j<resultTool.beans.length;j++){
                                if(resultTool.beans[j].deviceId == result){
                                    deviceBeansData.toolData = resultTool.beans[j];
                                    outputDeviceInfo(deviceBeansData);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }else{
                    tipModal("icon-cross", "获取设备控制信息失败");
                    return;
                }   
            })            
        }else{
            tipModal("icon-cross", "获取设备信息失败");
            return;
        }
    })

    // PostForm("/cwp/front/sh/device!execute", "uid=d001_3&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId, "", function (resultMsg) {
    //     if(resultMsg.returnCode  == 0){

    //         PostForm("/cwp/front/sh/device!execute", "uid=d001_3_2_1&pageSize=100&currentPage=1&deviceTypeId="+deviceTypeId+"&buildingId=''", "", function(resultTool){
    //             if(resultTool.returnCode == 0){
    //                 for(i=0;i<resultMsg.beans.length;i++){
    //                     if(resultMsg.beans[i].deviceId == result){
    //                         deviceBeansData = resultMsg.beans[i];
    //                         for(j=0;j<resultTool.beans.length;j++){
    //                             if(resultTool.beans[j].deviceId == result){
    //                                 deviceBeansData.toolData = resultTool.beans[j];
    //                                 outputDeviceInfo(deviceBeansData);
    //                                 break;
    //                             }
    //                         }
    //                         break;
    //                     }
    //                 }
    //             }else{
    //                 tipModal("icon-cross", "获取设备控制信息失败");
    //                 return;
    //             }
    //         }, '');
    //     }else{
    //         tipModal("icon-cross", "获取设备信息失败");
    //         return;
    //     }
    // }, '');

}

function transformInteger(result) {
    var integer = parseInt(result);
    if(isNaN(integer)){
        return 0;
    }else{
        return integer;
    }
}

// 输出设备信息
function outputDeviceInfo(result){

    var outHtml;
    var airimgUrl = "../../../assets/img/airConditioner/deviceAir/";

    //新风阀
    template.helper('airAdjustmentImg', function(inp) {
        if(inp == '开阀'){
            return airimgUrl+"damper1.gif";
        }else{
            return airimgUrl+"damper0.gif";
        }
    });
    template.helper('airAdjustmentC', function(inp) {
        if(inp == '开阀'){
            return "openControl";
        }else{
            return "closeControl";
        }
    });
    //新风滤网
    template.helper('freshAirFilterBlockAlarmImg', function(inp) {
        if(inp == '正常'){
            return airimgUrl+"filter1.gif";
        }else{
            return airimgUrl+"filter0.gif";
        }
    });
    template.helper('freshAirFilterBlockAlarmState', function(inp) {
        if(inp == '正常'){
            return "deviceStateNormal";
        }else{
            return "deviceStateAlarm";
        }
    });
    //送风机
    template.helper('airBlowerRunningStateImg', function(inp) {
        if(inp == '开启'){
            return airimgUrl+"fanRight1.gif";
        }else{
            return airimgUrl+"fanRight0.gif";
        }
    });
    template.helper('airBlowerFaultAlarmImg', function(inp) {
        if(inp == '正常'){
            return "hide";
        }else{
            return "";
        }
    });
    template.helper('airBlowerRunningStateS', function(inp) {
        if(inp == '开启'){
            return "deviceStateNormal";
        }else{
            return "deviceStateClose";
        }
    });
    template.helper('airBlowerFaultAlarmState', function(inp) {
        if(inp == '正常'){
            return "deviceStateNormal";
        }else{
            return "deviceStateAlarm";
        }
    });
    //回风滤网
    template.helper('returnAirFilterBlockAlarmImg', function(inp) {
        if(inp == '正常'){
            return airimgUrl+"filter1.gif";
        }else{
            return airimgUrl+"filter0.gif";
        }
    });
    template.helper('returnAirFilterBlockAlarmState', function(inp) {
        if(inp == '正常'){
            return "deviceStateNormal";
        }else{
            return "deviceStateAlarm";
        }
    });
    // 回风阀
    template.helper('returnAirSwitchImg', function(inp) {
        if(inp == '开阀'){
            return airimgUrl+"damper1.gif";
        }else{
            return airimgUrl+"damper0.gif";
        }
    });
    template.helper('returnAirSwitchC', function(inp) {
        if(inp == '开阀'){
            return "openControl";
        }else{
            return "closeControl";
        }
    });
    //防冻报警
    template.helper('airCondAntifreezingAlarmImg', function(inp) {
        if(inp == '正常'){
            return airimgUrl+"smokeDetector1.gif";
        }else{
            return airimgUrl+"smokeDetector0.gif";
        }
    });
    template.helper('airCondAntifreezingAlarmState', function(inp) {
        if(inp == '正常'){
            return "deviceStateNormal";
        }else{
            return "deviceStateAlarm";
        }
    });
    // 加湿伐
    template.helper('humidifierSwitchImg', function(inp) {
        if(inp == '开阀'){
            return airimgUrl+"humidifier1.gif";
        }else{
            return airimgUrl+"humidifier0.gif";
        }
    });
    template.helper('humidifierSwitchC', function(inp) {
        if(inp == '开阀'){
            return "openControl";
        }else{
            return "closeControl";
        }
    });
    //排风机
    template.helper('returnAirRunningStateImg', function(inp) {
        if(inp == '开启'){
            return airimgUrl+"fanLeft1.gif";
        }else{
            return airimgUrl+"fanLeft0.gif";
        }
    });
    template.helper('returnAirFaultAlarmImg', function(inp) {
        if(inp == '正常'){
            return "hide";
        }else{
            return "";
        }
    });
    template.helper('returnAirRunningStateS', function(inp) {
        if(inp == '开启'){
            return "deviceStateNormal";
        }else{
            return "deviceStateClose";
        }
    });
    template.helper('returnAirFaultAlarmState', function(inp) {
        if(inp == '正常'){
            return "deviceStateNormal";
        }else{
            return "deviceStateAlarm";
        }
    });
    //CO2浓度
    template.helper('co2ConcentrationVal', function(inp) {
        return transformInteger(inp);
    });
    template.helper('co2ConcentrationSettingVal', function(inp) {
        return transformInteger(inp);
    });
    template.helper('co2ConcentratContrOptD', function(inp) {
        if(inp == '启用'){
            return "";
        }else{
            return "disabled";
        }
    });
    template.helper('co2ConcentratContrOptBtn', function(inp) {
        if(inp == '启用'){
            return "";
        }else{
            return "deviceSubmitBtnCant";
        }
    });
    template.helper('co2ConcentratContrOptC', function(inp) {
        if(inp == '启用'){
            return "openControl";
        }else{
            return "closeControl";
        }
    });
    template.helper('co2ConcentratContrOptS', function(inp) {
        if(inp == '启用'){
            return "deviceStateNormal";
        }else{
            return "deviceStateClose";
        }
    });
    //送风湿度
    template.helper('humiditySettingVal', function(inp) {
        return transformInteger(inp);
    });
    template.helper('airSupplyHumidityVal', function(inp) {
        return transformInteger(inp);
    });
    //送风温度
    template.helper('temperatureSettingVal', function(inp) {
        return transformInteger(inp);
    });
    template.helper('airSupplyTemperatureVal', function(inp) {
        return transformInteger(inp);
    });

    outHtml = template("deviceInfoTmplAir", result);

    $("#floorDeviceInfo").html(outHtml);
}

// 设备列表点击
$(document).on("click",".deviceListli",function(){

    var deviceState = $(this).attr("datastate");
    if(deviceState == "-1"){
        tipModal("icon-cross", "此设备未开启");
        return;
    }else{
        var deviceId = $(this).attr("dataid");
        getdeviceInfo(deviceId);
    }

});

//楼层容器自适应
$(window).resize(function(){
    autoWidth();
});

function autoWidth(){
    if($(".floor-image.cur img").length != 0){
        var w=$(".floor-image.cur img")[0].offsetWidth;
        $(".floor-image.cur").css("width",w+"px");
    }
}

//设备控制
// input
$(document).on("click","#deviceInfoImgBox .deviceSubmitBtn",
    function(){
        var _this = $(this);
        if(!_this.hasClass("deviceSubmitBtnCant")){
            // 设备Id
            var _deviceId = $("#deviceInfoImgBox").attr("deviceId");
            //设备编号
            var _deviceIdCode = $("#deviceInfoImgBox").attr("deviceIdCode");
            //控制编号
            var _ipgCode = _this.attr("ipgCode");
            //请求值
            var _ipgValue = _this.prev().find("input").val();
            //原始值
            var oldVal = _this.attr("oldVal");
            //input
            var _input = _this.prev().find("input");
            //等待图标
            var _img = _this.find("img");

            if(_ipgCode == 16){
                if(_ipgValue<350 || _ipgValue>999){
                    tipModal("icon-cross", "CO₂浓度必须是350-999PPM");
                    _input.val(oldVal);
                    return;
                }
            }else if(_ipgCode == 17){
                if(_ipgValue<40 || _ipgValue>70){
                    tipModal("icon-cross", "湿度必须是40-70%");
                    _input.val(oldVal);
                    return;
                }
            }else{
                if(_ipgValue<18 || _ipgValue>28){
                    tipModal("icon-cross", "温度必须是18-28°C");
                    _input.val(oldVal);
                    return;
                }
            }

            _img.removeClass("hide");
            //ipg请求
            getControlAccessIpg(_deviceId,_deviceIdCode,_ipgCode,_ipgValue,function(){
                _img.addClass("hide");
            },function () {
                _input.val(oldVal);
                _img.addClass("hide");
            },function () {
                _input.val(oldVal);
                _img.addClass("hide");
            })
        }
    }
);

// 开关
$(document).on("click","#deviceInfoImgBox .deviceControlBtn",
    function (){
        var _this = $(this);
        if(_this.find("img").hasClass("hide")){
            // 设备Id
            var _deviceId = $("#deviceInfoImgBox").attr("deviceId");
            //设备编号
            var _deviceIdCode = $("#deviceInfoImgBox").attr("deviceIdCode");
            //控制编号
            var _ipgCode = _this.attr("ipgCode");
            //请求值
            var _ipgValue;
            //等待img
            var _img = _this.find("img");

            if(_this.hasClass('openControl')){
                _ipgValue = 0;
                _this.removeClass('openControl').addClass('closeControl');
            }else{
                _ipgValue = 1;
                _this.removeClass('closeControl').addClass('openControl');
            }
            _img.removeClass("hide");

            //ipg请求
            getControlAccessIpg(_deviceId,_deviceIdCode,_ipgCode,_ipgValue,function(){
                _img.addClass("hide");
                if(_ipgCode == 5){

                    var _co2Ctr = _this.next().eq(0);
                    var _co2Sub = _this.siblings().eq(4);
                    var _co2Inp = _this.siblings().eq(3).find("input");

                    if(_co2Ctr.html() == "停用"){
                        _co2Ctr.removeClass('deviceStateClose').addClass('deviceStateNormal').html('启用');
                        _co2Sub.removeClass('deviceSubmitBtnCant');
                        _co2Inp.removeAttr("disabled");
                    }else{
                        _co2Ctr.removeClass('deviceStateNormal').addClass('deviceStateClose').html("停用");
                        _co2Sub.addClass('deviceSubmitBtnCant');
                        _co2Inp.attr("disabled","disabled");
                    }
                }
            },function(){
                _img.addClass("hide");
                if(_this.hasClass('openControl')){
                    _this.removeClass('openControl').addClass('closeControl');
                }else{
                    _this.removeClass('closeControl').addClass('openControl');
                }
            },function(){
                _img.addClass("hide");
                if(_this.hasClass('openControl')){
                    _this.removeClass('openControl').addClass('closeControl');
                }else{
                    _this.removeClass('closeControl').addClass('openControl');
                }
            });
        }
    }
);