﻿var echartColors;
var dateMonth;
$(function () {
    var loader = "<div class='loading' style='margin:0 auto;text-align:center; padding-top:20px;'><img src='/cwp/src/assets/img/loading/cmcc_loading_lg.gif' style='height:35px;width:35px;display:inline;' alt=''/><p style='font-size:12px;display:block;margin-top: 10px'>载入中，请稍候...</p></div>";
    //$("#faultEcharts").html(loader);
    //$("#securityEcharts").html(loader);
    //$("#orderEcharts").html(loader);
    //$("#line-chart").html(loader);
    dateMonth = getToday();
    echartColors = ["#ed7d31","#ffc000","#5b9bd5"];
    requestEventNum();
//    displayOrderEcharts();
//    displayEnergyEcharts();
});

function getToday(){
    var today = "";
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
    return today;
}

function requestEventNum(){
    // PostForm("/cwp/front/sh/bedapplyAnalysis!execute", "uid=bedapplyAnalysis016&applyTime=" + dateMonth, "", dormitoryEchart, '');
    // PostForm("/cwp/front/sh/device!execute", "uid=d017&deviceTypeId=1004&buildingId=4fe6a6ba-72bd-46ef-b15b-f13045737823", "", entranceEchart, '');
    // PostForm("/cwp/front/sh/cardConsumInfo!execute", "uid=cardConsumInfo001&applyTime=" + dateMonth, "", consumptionEchart, '');
    // PostForm("/cwp/front/sh/warningEvent!execute", "uid=c028", "", buildingEchart, '');
    entranceEchart();
    dormitoryEchart();
    consumptionEchart();
    buildingEchart();
}

//宿舍入住
function dormitoryEchart(result){
    var option;
    var dEcharts = echarts.init(document.getElementById("dormitoryEcharts"));
    if(true){
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();
        var monthDays = new Date(year,month,0).getDate();

        var dataNum = [];
        // for (var i = 0; i < monthDays; i++) {
        //     if(i<result.beans.length){
        //         dataNum[i] = result.beans[i].currDayNum
        //     }else{
        //         dataNum[i] = 0
        //     }
        // }
        
        for (var i = 0; i < monthDays; i++) {
            if(i<40){
                dataNum[i] = parseInt(Math.random()*200) +400
            }else{
                dataNum[i] = 0
            }
        }        

        option = {
            tooltip : {                                                         //  气泡提示框
                trigger: 'axis'                                                //直角坐标系中的一个坐标轴，坐标轴可分为类目型、数值型或时间型
            },
            color:['#4b9ff5'],                                                //设置图表颜色
            legend: {                                                          //图例，表述数据和图形的关联
                data:['入住人数']
            },
            calculable : true,
            dataZoom : {                                                      //数据区域缩放，常用于展现大量数据时选择可视范围
               show : true,
               realtime : true,
               start : 0,
               end : day/monthDays*100,
            },
            xAxis : [                                                           //直角坐标系中的横轴，通常并默认为类目型
                {
                    type : 'category',
                    boundaryGap : false,
                    data : function (){                                        //加载横坐标数据
                        var list = [];
                        for (var i = 1; i <= 31; i++) {
                            list.push(year+'-'+month+'-' + i);
                        }
                        return list;
                    }()
                }
            ],
            yAxis : [                                                            // 直角坐标系中的纵轴，通常并默认为数值型
                {
                    name:'(人)', 
                    type : 'value',
                    max : '1000',
                    axisLabel : {                                              //设置纵坐标的单位
                        formatter: '{value}'
                    }
                }
            ],
            series : [                                                         //数据系列，一个图表可能包含多个系列，每一个系列可能包含多个数据
                {
                    name:'入住人数',                                       //所表示的类型
                    type:'line',
                    smooth:true,                                       //图标类型
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: 'default'
                            }
                        }
                    },//图表样式设置
                    data:dataNum
                }
            ]
        }
    }else{
        option = {
            legend: {
                data:[]
            },
            series : [
                {
                    name:'',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[]
                }
            ]     
        };
    }
    // 为echarts对象加载数据
    dEcharts.setOption(option);
}

// 门禁监控
function entranceEchart(result){
    var result = {
        "busiCode": null,
        "opId": null,
        "phone": null,
        "returnCode": "0",
        "returnMessage": "查询成功",
        "bean": {},
        "beans": [{
            "deviceCount": "127",
            "deviceState": "0",
            "deviceStateName": "正常"
        }, {
            "deviceCount": "3",
            "deviceState": "1",
            "deviceStateName": "故障"
        }, {
            "deviceCount": "1",
            "deviceState": "2",
            "deviceStateName": "严重故障"
        }],
        "object": null
    }
    var dataArr = [];
    var legendArr = [];
    var entranceColor = ['#49C057','#ED7C30','#C5585A'];
    if(result.returnCode == '0'){
        // var isNormal = 1;
        for (var i = 0 ; i < result.beans.length ; i++) {

            var legendArrChild = result.beans[i].deviceStateName + result.beans[i].deviceCount;
            var dataArrChild = {
                value : result.beans[i].deviceCount,
                name : legendArrChild
            };

            switch(result.beans[i].deviceState)
            {
                case '0':
                    dataArr.push(dataArrChild);
                    legendArr.push(legendArrChild);
                    break;
                case '1':
                    legendArr.push(legendArrChild);
                    if(result.beans[i].deviceCount != 0) {
                        dataArr.push(dataArrChild);
                        // isNormal = 0;
                    }
                    break;
                case '2':
                    legendArr.push(legendArrChild);
                    if(result.beans[i].deviceCount != 0) {
                        dataArr.push(dataArrChild);
                        // isNormal = 0;
                    }
                    break;
            }
        }
        // if(isNormal == 1){
        //     entranceColor = ['#19b955'];
        // }
    }

    var eEcharts = echarts.init(document.getElementById('entranceEcharts'));
    var optionFaultEcharts= {
        color:entranceColor,
        legend: {
            data:legendArr
        },
        calculable : true,
        series : [
            {
                name:'',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:dataArr
            }
        ]
    };
    eEcharts.setOption(optionFaultEcharts);
}

// 消费统计
function consumptionEchart(){
    var option;
    if(true){
        var dEcharts = echarts.init(document.getElementById('consumptionEcharts'));
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();
        var monthDays = new Date(year,month,0).getDate();

        var dataNum = [];
        // for (var i = 0; i < monthDays; i++) {
        //     if(i<result.beans.length){
        //         dataNum[i] = Math.abs(result.beans[i].currDayCount)
        //     }else{
        //         dataNum[i] = 0
        //     }
        // }
        
        for (var i = 0; i < monthDays; i++) {
            if(i<30){
                dataNum[i] = parseInt(Math.random()*30000) +25000
            }else{
                dataNum[i] = 0
            }
        }

        option = {
            tooltip : {                                                         //  气泡提示框
                trigger: 'axis'                                                //直角坐标系中的一个坐标轴，坐标轴可分为类目型、数值型或时间型
            },
            color:echartColors,                                                //设置图表颜色
            legend: {                                                          //图例，表述数据和图形的关联
                data:['消费总额']
            },
            calculable : true,
            xAxis : [                                                           //直角坐标系中的横轴，通常并默认为类目型
                {
                    type : 'category',
                    boundaryGap : false,
                    data : function (){                                        //加载横坐标数据
                        var list = [];
                        for (var i = 1; i <= 31; i++) {
                            list.push(year+'-'+month+'-' + i);
                        }
                        return list;
                    }()
                }
            ],
            yAxis : [                                                            // 直角坐标系中的纵轴，通常并默认为数值型
                {
                    name:'(元)', 
                    type : 'value',
                    max : '100000',
                    axisLabel : {                                              //设置纵坐标的单位
                        formatter: '{value}'
                    }
                }
            ],
            series : [                                                         //数据系列，一个图表可能包含多个系列，每一个系列可能包含多个数据
                {
                    name:'消费总额',                                       //所表示的类型
                    type:'line',
                    smooth:true,                                       //图标类型
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: 'default'
                            }
                        }
                    },
                    data:dataNum
                }
            ]
        }
    }else{
        option = {
            legend: {
                data:[]
            },
            series : [
                {
                    name:'',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[]
                }
            ]     
        };
    }
    // 为echarts对象加载数据
    dEcharts.setOption(option);
}

// 楼宇
function buildingEchart(result){
    var bEchart = echarts.init(document.getElementById('buildingEcharts'));
    var option= {
        color:echartColors,
        legend: {
            data:['正常44个','故障2个']
        },
        calculable : true,
        series : [
            {
                name:'',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {name:'正常40个',value:'40'},
                    {name:'故障2个',value:'2'}
                    ]
            }
        ]
    };
    bEchart.setOption(option);    
}