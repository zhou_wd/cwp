var echartColors;
var dateMonth;
$(function () {
    var loader = "<div class='loading' style='margin:0 auto;text-align:center; padding-top:20px;'><img src='/cwp/src/assets/img/loading/cmcc_loading_lg.gif' style='height:35px;width:35px;display:inline;' alt=''/><p style='font-size:12px;display:block;margin-top: 10px'>载入中，请稍候...</p></div>";
    //$("#faultEcharts").html(loader);
    //$("#securityEcharts").html(loader);
    //$("#orderEcharts").html(loader);
    //$("#line-chart").html(loader);
    dateMonth = getToday;
    echartColors = ["#ed7d31","#ffc000","#5b9bd5"];
    requestEventNum();
//    displayOrderEcharts();
//    displayEnergyEcharts();
});

function getToday(){
    var today = "";
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    today = year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day);
    return today;
}

function requestEventNum(){
    PostForm("/cwp/front/sh/bedapplyAnalysis!execute", "uid=bedapplyAnalysis016&applyTime=" + dateMonth, "", dormitoryEchart, '');
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c026&eventType=2", "", entranceEchart, '');
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c028", "", consumptionEchart, '');
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c028", "", buildingEchart, '');


    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c026&eventType=1", "", faultStatusCompared, '');
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c026&eventType=2", "", securityStatusCompared, '');
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c028", "", displayOrderEcharts, '');
    PostForm("/cwp/front/sh/energy!execute", "uid=e006", "", displayEnergyEcharts, '');

}

//宿舍入住
function dormitoryEchart(result){
    var option;
    if(result.returnCode == 1){
        var dEcharts = echarts.init(document.getElementById('dormitoryEcharts'));
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();
        var monthDays = new Date(year,month,0).getDate();

        var dataNum = [];
        for (var i = 0; i < monthDays; i++) {
            if(i<result.beans.length){
                dataNum[i] = result.beans[i].currDayNum
            }else{
                dataNum[i] = 0
            }
        }
        option = {
            color:echartColors,
            tooltip : {                                                         //  气泡提示框
                trigger: 'axis'                                                //直角坐标系中的一个坐标轴，坐标轴可分为类目型、数值型或时间型
            },
            color:['#4b9ff5'],                                                //设置图表颜色
            legend: {                                                          //图例，表述数据和图形的关联
                data:['入住人数']
            },
            calculable : true,
            dataZoom : {                                                      //数据区域缩放，常用于展现大量数据时选择可视范围
               show : true,
               realtime : true,
               start : 0,
               end : day/monthDays*100,
            },
            xAxis : [                                                           //直角坐标系中的横轴，通常并默认为类目型
                {
                    type : 'category',
                    boundaryGap : false,
                    data : function (){                                        //加载横坐标数据
                        var list = [];
                        for (var i = 1; i <= 31; i++) {
                            list.push(year+'-'+month+'-' + i);
                        }
                        xCount=day;
                        return list;
                    }()
                }
            ],
            yAxis : [                                                            // 直角坐标系中的纵轴，通常并默认为数值型
                {
                    type : 'value',
                    axisLabel : {                                              //设置纵坐标的单位
                        formatter: '{value} 人'
                    }
                }
            ],
            series : [                                                         //数据系列，一个图表可能包含多个系列，每一个系列可能包含多个数据
                {
                    name:'入住人数',                                       //所表示的类型
                    type:'line',
                    smooth:true,                                       //图标类型
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: 'default'
                            }
                        }
                    },//图表样式设置
                    data:dataNum
                }
            ]
        }
    }else{
        option = {
            legend: {
                data:['正常','不正常']
            },
            series : [
                {
                    name:'',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[

                    ]
                }
            ]     
        };
    }
    // 为echarts对象加载数据
    dEcharts.setOption(option);
}

// 门禁监控
function entranceEchart(data){
    var eEcharts = echarts.init(document.getElementById('entranceEcharts'));
    var optionFaultEcharts= {
        color:echartColors,
        legend: {
            data:['正常','不正常']
        },
        calculable : true,
        series : [
            {
                name:'',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[

                ]
            }
        ]
    };
    eEcharts.setOption(optionFaultEcharts);
}

// 消费统计
function consumptionEchart(data){
    var cEcharts = echarts.init(document.getElementById('consumptionEcharts'));
    cEcharts.setOption({
        legend: {
            data:['早上','中午','晚上','夜间']
        },
        color:echartColors,
        calculable : true,
        xAxis : [
            {
                type : 'category',
                data : ['班车','员工之家','中膳','和兴隆']
            }
        ],
        yAxis : [
            {
                type : 'value',
                splitArea : {show : true}
            }
        ],
        series : [
            {
                name:'早上',
                type:'bar',
                data:[]
            },
            {
                name:'中午',
                type:'bar',
                data:[]
            },
            {
                name:'晚上',
                type:'bar',
                data:[]
            },
            {
                name:'夜间',
                type:'bar',
                data:[]
            }
        ]
    });   
}

// 楼宇
function buildingEchart(){
    var bEchart = echarts.init(document.getElementById('buildingEcharts'));
    var option= {
        color:echartColors,
        legend: {
            data:['正常','不正常']
        },
        calculable : true,
        series : [
            {
                name:'',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[

                ]
            }
        ]
    };
    bEchart.setOption(option);    
}

//故障告警报表
function faultStatusCompared(result){
    if(result.returnCode == 0)
    {
        var faultEcharts = echarts.init(document.getElementById('faultEcharts'));
        var untreatedTotal =parseInt(result.bean.untreatedTotal);
        var dealingTotal=parseInt(result.bean.dealingTotal);
        var finishTotal=parseInt(result.bean.finishTotal);
        var userName=$("#header_username").html();
        //if(userName.indexOf("艾聪")>-1){
        //    untreatedTotal=untreatedTotal-10;
        //    dealingTotal=dealingTotal+5;
        //    finishTotal=finishTotal+5;
        //}

        var optionFaultEcharts= {
            color:["#ed7d31","#ffc000","#5b9bd5"],
            legend: {
                data:['未处理(共'+untreatedTotal+'条)','处理中(共'+dealingTotal+'条)','已处理(共'+finishTotal+'条)']
            },
            calculable : true,
            series : [
                {
                    name:'访问来源',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {value:untreatedTotal, name:'未处理(共'+untreatedTotal+'条)'},
                        {value:dealingTotal, name:'处理中(共'+dealingTotal+'条)'},
                        {value:finishTotal, name:'已处理(共'+finishTotal+'条)'}
                    ]
                }
            ]
        };
        faultEcharts.setOption(optionFaultEcharts);
    }

}



//安防告警报表
function securityStatusCompared(result){
    if(result.returnCode == 0){
            var securityEcharts = echarts.init(document.getElementById('securityEcharts'));
            var untreatedTotal =parseInt(result.bean.untreatedTotal);
            var dealingTotal=parseInt(result.bean.dealingTotal);
            var finishTotal=parseInt(result.bean.finishTotal);
            var userName=$("#header_username").html();
            //if(userName.indexOf("艾聪")>-1){
            //    untreatedTotal=untreatedTotal-22;
            //    dealingTotal=dealingTotal+11;
            //    finishTotal=finishTotal+11;
            //}

            var optionSecurityEcharts= {
                color:["#ed7d31","#ffc000","#5b9bd5"],
                legend: {
                    data:['未处理(共'+untreatedTotal+'条)','处理中(共'+dealingTotal+'条)','已处理(共'+finishTotal+'条)']
                },
                calculable : true,
                series : [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius : '55%',
                        center: ['50%', '60%'],
                        data:[
                            {value:untreatedTotal, name:'未处理(共'+untreatedTotal+'条)'},
                            {value:dealingTotal, name:'处理中(共'+dealingTotal+'条)'},
                            {value:finishTotal, name:'已处理(共'+finishTotal+'条)'}
                        ]
                    }
                ]
            };
            securityEcharts.setOption(optionSecurityEcharts);
    }
}



//工单审批
function displayOrderEcharts(data){
//	   "HVAC_Order": "2",
//       "HVAC_Device": "2",
//       "SewagePump_order": "0",
//       "VentilationSystem_Device": "0",
//       "Gate_Device": "5",
//       "VentilationSystem_Order": "0",
//       "Gate_Order": "5",
//       "SewagePump_Device": "0"
	var dataR=data.bean;
    var orderEcharts = echarts.init(document.getElementById('orderEcharts'));
    orderEcharts.setOption({
        legend: {
            data:['设备数','维修次数']
        },
        color:["#87cefa","#fa7d3c"],
        calculable : true,
        xAxis : [
            {
                type : 'category',
                data : ['空调','新风','排污泵','门禁']
            }
        ],
        yAxis : [
            {
                type : 'value',
                splitArea : {show : true}
            }
        ],
        series : [
            {
                name:'设备数',
                type:'bar',
                data:[dataR.HVAC_Device, dataR.VentilationSystem_Device, dataR.SewagePump_Device, dataR.Gate_Device]
            },
            {
                name:'维修次数',
                type:'bar',
                data:[dataR.HVAC_Order, dataR.VentilationSystem_Order,dataR.SewagePump_order, dataR.Gate_Order]
            }
        ]
    });
}




//初始化能耗图标
function displayEnergyEcharts(data){
    var myChart = echarts.init(document.getElementById('line-chart'));
    var xCount=0;
    var option = {
        tooltip : {                                                         //	气泡提示框
            trigger: 'axis'                                                //直角坐标系中的一个坐标轴，坐标轴可分为类目型、数值型或时间型
        },
        color:['#4b9ff5'],                                                //设置图表颜色
        legend: {                                                          //图例，表述数据和图形的关联
            data:['耗电量']
        },
        calculable : true,
        //dataZoom : {                                                      //数据区域缩放，常用于展现大量数据时选择可视范围
        //    show : true,
        //    realtime : true,
        //    start : 20,
        //    end : 80
        //},
        xAxis : [                                                           //直角坐标系中的横轴，通常并默认为类目型
            {
                type : 'category',
                boundaryGap : false,
                data : function (){                                        //加载横坐标数据
                    var list = [];
                    var date = new Date();
                    var month=date.getMonth()+1;
                    var day = date.getDate();
                    //alert("当月："+month+"当天："+day);
                    for (var i = 1; i <= day; i++) {
                        list.push('2017-'+month+'-' + i);
                    }
                    xCount=day;
                    return list;
                }()
            }
        ],
        yAxis : [                                                            //	直角坐标系中的纵轴，通常并默认为数值型
            {
                type : 'value',
                axisLabel : {                                              //设置纵坐标的单位
                    formatter: '{value} kwh'
                }
            }
        ],
        series : [                                                         //数据系列，一个图表可能包含多个系列，每一个系列可能包含多个数据
            {
                name:'耗电量',                                            //所表示的类型
                type:'line'      ,                                       //图标类型
                itemStyle: {normal: {areaStyle: {type: 'default'}}},//图表样式设置
                data:function (){ 
                	//加载数据
                    var list = [];
                    var dataR = data.beans;
//                            alert(dataR[0].month)
                    var bb=1;
                    for (var i = 1; i <= xCount; i++) {
                    	if(i>dataR.length){
                    		list.push(0);
                    	}else{
                    		if(dataR[i-bb].month!=i){
                    			bb++;
                    			list.push(0);
                    		}else{
                    			list.push(dataR[i-bb].eps);
                    		}
                    	}
                    }
                    return list;
                }()
            }
        ]
    };
    // 为echarts对象加载数据
    myChart.setOption(option);
}