﻿/**
 * Created by zhoujinwei on 2016/6/14.
 */
$(function(){
 	 // PostForm("/cwp/front/sh/energy!execute", "uid=e004", "", powerTrendsEcharts, '');
    //楼栋耗电量
    powerTrendsEcharts();
});


$("#myTabs li").on("click",function(){
    var index_id=$(this).index();
    if(index_id==0){
        setTimeout(function (){
        	PostForm("/cwp/front/sh/energy!execute", "uid=e004", "", powerTrendsEcharts, '');
        },200);
    }else{
        setTimeout(function (){
            banPower();
            PostForm("/cwp/front/sh/energy!execute", "uid=e005", "", devicesPowerCompared, '');
            PostForm("/cwp/front/sh/energy!execute", "uid=e004", "", banPowerMonth, '');
        },200);
    }

});





//用电趋势开始----------------------------------
$(".btnSearch").on("click",function(){
	 // PostForm("/cwp/front/sh/energy!execute", "uid=e004", "", powerTrendsEcharts, '');
     powerTrendsEcharts();
});


//初始用电趋势
var myChart1;
function powerTrendsEcharts(){
    var cmbYear=$("#cmbYear").val();
    var cmbMonth=$("#cmbMonth").val();
    if(cmbYear=="")
    {
        alert("请选择年");
        return;
    }
    var date = new Date();
    var month=date.getMonth()+1;//加载数据

    myChart1 = echarts.init(document.getElementById('powerTrendsEcharts'));
    var xCount=0
    var option = {
            tooltip : {                                                         //	气泡提示框
                trigger: 'axis'                                                //直角坐标系中的一个坐标轴，坐标轴可分为类目型、数值型或时间型
            },
            color:['#4b9ff5'],                                                //设置图表颜色
            legend: {                                                          //图例，表述数据和图形的关联
                data:['今年耗电量']
            },
            calculable : true,
            xAxis : [                                                           //直角坐标系中的横轴，通常并默认为类目型
                {
                    type : 'category',
                    boundaryGap : false,
                    data : function (){                                        //加载横坐标数据
                                var list = [];
                                var count=0;
                                var day=[31,29,31,30,31,30,31,31,30,31,30,31];
                                var time="";
                                //var date = new Date();
                                //var month=date.getMonth()+1;
                                if(cmbMonth==""){
                                    count=12;
                                    if(cmbYear=="2016")
                                    {
                                        count=month;
                                    }
                                    time=cmbYear+"-";
                                }
                                else{
                                    count=day[cmbMonth-1];
                                    time=cmbYear+"-"+cmbMonth+"-";
                                }
                                for (var i = 1; i <= count; i++) {
                                    list.push(time+i);
                                }
                                xCount=count;
                                return list;
                    }()
                }
            ],
            yAxis : [                                                            //	直角坐标系中的纵轴，通常并默认为数值型
                {
                    type : 'value',
                    axisLabel : {                                              //设置纵坐标的单位
                        formatter: '{value} kwh'
                    }
                }
            ],
            series : [                                                         //数据系列，一个图表可能包含多个系列，每一个系列可能包含多个数据
                {
                    name:'今年耗电量',                                            //所表示的类型
                    type:'line',                                            //图标类型
                    smooth:true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},    //图表样式设置
                    data:[820, 932, 901, 934, 1290, 1320,820, 932, 901, 934, 1290,1320]
                }
            ]
        };
    // 为echarts对象加载数据
    myChart1.setOption(option);

}





//用电趋势结束----------------------------------


//初始楼栋用电开始------------------------------
var myChart2;
function banPower(){
    myChart2 = echarts.init(document.getElementById('banPower'));
    myChart2.setOption({
        legend: {
            data:['耗电量']
        },
        color:["#87cefa"],
        calculable : true,
        grid: {
            x: 90,
            x2: 34,
            y: 35,
            y2: 30
        },
        xAxis : [
            {
                type : 'category',
                data:function (){                                       //加载数据
                    var list = [];
                    for (var i = 0; i <= 5; i++) {
                        var banNum=i+1;
                        list.push("CC-"+banNum);
                    }
                    return list;
                }()
            }
        ],
        yAxis : [
            {
                type : 'value',
                axisLabel : {                                              //设置纵坐标的单位
                    formatter: '{value} kwh'
                }
            }
        ],
        series : [
            {
                name:'耗电量',
                type:'bar',
                smooth:true,
                data:function (){                                       //加载数据
                    var list = [];
                    for (var i = 0; i <= 5; i++) {
                        if(i==0){
                            list.push(banPowerTotal);
                        }else{
                            list.push(Math.round(Math.random()*1120));
                        }
                    }
                    return list;
                }()
            }
        ]
    });

    //点击柱状图事件
    myChart2.on("click", function (param) {

        if(param.dataIndex!=0){
            tipModal("icon-cross", "暂无数据");
        }else{
            $("#banPowermonth").prev().text(param.name);
            banPowerMonth();
            devicesPowerCompared();
        }
    });
}

//根据楼栋显示当年每月耗电
var banPowerTotal=0;
var banPowerMonthChart;
function banPowerMonth(result){
    if(result.returnCode=="0"){
        var date = new Date();
        var month=date.getMonth()+1;//加载数据
        banPowerMonthChart= echarts.init(document.getElementById('banPowermonth'));
        banPowerMonthChart.setOption({
            legend: {
                data:['耗电量']
            },
            color:["#87cefa"],
            calculable : true,
            //grid: {
            //    x: 80,
            //    x2: 34,
            //    y: 35,
            //    y2: 30
            //},
            xAxis : [
                {
                    type : 'category',
                    data:function (){

                        var list = [];
                        for (var i = 1; i <= month; i++) {
                            list.push(i+"月");
                        }
                        return list;
                    }()
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    axisLabel : {                                              //设置纵坐标的单位
                        formatter: '{value} kwh'
                    }
                }
            ],
            series : [
                {
                    name:'耗电量',
                    type:'bar',
                    data:function (){                                       //加载数据
                        var list = [];
                        for (var i = 0; i < month; i++) {
                            list.push(result.beans[i].eps);

                        }
                        return list;
                    }()
                }
            ]
        });
    }

}

//根据楼栋显示当月设备耗电比
var devicesPowerComparedEcharts;
function devicesPowerCompared(result){
    //模拟数据
    if(result.returnCode=="0"){
        //耗电总量
        var kongtiaoEps=parseInt(result.bean.kongtiaoEps);
        var xinfengEps=parseInt(result.bean.xinfengEps);
        var menjiEps=parseInt(result.bean.menjiEps);
        var panwubangEps=parseInt(result.bean.panwubangEps);
        var total=kongtiaoEps+xinfengEps+menjiEps+panwubangEps;

        //百分比
        var kongtiaoEpsPer=Math.round(kongtiaoEps/total* 10000)/100.00;
        var xinfengEpsPer=Math.round(xinfengEps/total* 10000)/100.00;
        var menjiEpsPer=Math.round(menjiEps/total* 10000)/100.00;
        var panwubangEpsPer=Math.round(panwubangEps/total* 10000)/100.00;

        var powerArray=[[30,20,40,10],[46,23,13,18],[26,33,23,18],[35,23,25,17],[46,23,13,18]];
        var power=powerArray[Math.round(Math.random()*4)];
        devicesPowerComparedEcharts = echarts.init(document.getElementById('devicesPowerCompared'));

        var optionPowerCompared= {
            legend: {
                data : ['空调'+kongtiaoEpsPer + "%",'新风'+xinfengEpsPer + "%",'排污泵'+menjiEpsPer + "%",'门禁'+panwubangEpsPer + "%"]
            },
            calculable : true,
            series : [
                {
                    name:'访问来源',
                    type:'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {value:kongtiaoEpsPer, name:'空调'+kongtiaoEpsPer + "%"},
                        {value:xinfengEpsPer, name:'新风'+xinfengEpsPer + "%"},
                        {value:menjiEpsPer, name:'排污泵'+menjiEpsPer + "%"},
                        {value:panwubangEpsPer, name:'门禁'+panwubangEpsPer + "%"}
                    ]
                }
            ]
        };
        devicesPowerComparedEcharts.setOption(optionPowerCompared);
    }
}

//随浏览器尺寸变化自适应
setTimeout(function (){
    window.onresize = function () {
        var activeIndex=$("#myTabs .active").index();
        if(activeIndex==0){
            myChart1.resize();
        }
        else{
            myChart2.resize();
            banPowerMonthChart.resize();
            devicesPowerComparedEcharts.resize();
        }
    }
},200);


