/**
 * Created by again on 2016/10/11.
 */
//点击查看视频
$(".video-list .thumbnail").on("click", function () {
    var pId=$(this).attr("data-id");
    var address=$(this).find(".fn-center").html();
    $(".modal-title").html(address);
    if ((navigator.userAgent.indexOf('MSIE') >= 0)){
        alert("您的浏览器不支持视频播放，建议使用谷歌浏览器");
    }else{
        getPort(pId);
    }
});

//判断浏览器版本


//关闭modal触发视频请求
$('#modalVideoSurveillance').on('hide.bs.modal', function (e) {
    logout();
    $(".loading .progress-bar").css("width","0");
    $("#videoSurveillance").remove();

});

//get端口

var url="";
function getPort(pId){
    url=window.location.protocol+"//"+window.location.host+"/";
    //url="http://120.194.44.254:20888/";
    $.ajax({
        //url: "http://120.194.44.254:20888/spjkserver/videoapp",
        url: url+"spjkserver/videoapp",
        type: "post",
        async: true,
        data: {
            pId: pId
        },
        dataType: "json",
        timeout: 1000000,
        success: function(result) {
            var linkPort=result.port;
            getPortResultArrived(linkPort);
        },
        error: function(xhr, type, errorThrown) {
            if(type == 'timeout') {
                alert('请求超时:请检查网络!');
            } else {
                alert('数据加载失败!');
            }
        }
    });
}

function getPortResultArrived(linkPort){

    $(".loading").after("<video id='videoSurveillance' width='100%' controls='' autoplay='' style='background: #FFFFFF;'>"+
                            "<source src='' type='video/ogg'>"+
                            "您的浏览器不支持视频播放，请使用谷歌浏览器"+
                        "</video>");

    var videoLink=url+linkPort;
    //缓冲视频
    $("#modalVideoSurveillance").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    $(".loading").show();
    $(".progress .progress-bar").progressbar({
        display_text: 'center',
        done:function(){
            $(".loading").hide();
            $("#videoSurveillance source").attr("src",videoLink);
            $("#videoSurveillance").load();
        }
    });

}




//关闭界面触发请求
window.onbeforeunload = function(event) {
    logout();
}

function logout() {
    $.ajax({
        url: url+'spjkserver/main',
        /*url: 'http://58.96.169.201:88/spjkserver/main' , */
    });
}

