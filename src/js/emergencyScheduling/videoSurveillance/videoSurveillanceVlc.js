/**
 * Created by again on 2016/10/11.
 */
//点击查看视频
$(".video-list .thumbnail").on("click", function () {
    var pId=$(this).attr("data-id");
    var address=$(this).find(".fn-center").html();
    $(".modal-title").html(address);

    //填充表单rtspPlayAddr rtmpPlayAddr
    var sourceVideoIp = "192.168.13.62";
    var appId="app"+ new Date().getTime();
    var rtspPlayAddr = "rtsp://admin:lyyd8888@192.168.13.62/PSIA/streaming/channels/"+pId;
    var rtmpPlayAddr = "rtmp://"+rtmpVideoIp+"/live/";

    $("#sourceVideoIp").val(sourceVideoIp);
    $("#rtspPlayAddr").val(rtspPlayAddr);
    $("#rtmpPlayAddr").val(rtmpPlayAddr);
    $("#appId").val(appId);
    pushVideoApp(pId);
});


function pushVideoApp(){
    PostForm("/cwp/front/sh/ipg!execute", "uid=ipg007", "post", pushVideoAppSuccess, '',"frmMain");
}

//请求视频流回调
function pushVideoAppSuccess(result){
    // var url = $("#rtmpPlayAddr").val()+$("#appId").val();
    //播放rtmp外网地址
    var url ="rtmp://"+public_rtmpVideoIp+"/live/"+$("#appId").val();
    if(result.returnCode == 0){
        newVideo(url);
    }else{
        alert("获取视频数据失败");
    }
}

//关闭modal触发视频请求
$('#modalVideoSurveillance').on('hide.bs.modal', function () {
    //清理播放器，重置播放组件
    //if(window.myPlayer!=undefined){
    //    videojs('my_video').dispose();
    //    $("#objVlc").html(outVideoHtml);
    //}
    videojs('my_video').dispose();
    //清空进度条
    $(".loading .progress-bar").css("width","0");
    //关闭视频
    PostForm("/cwp/front/sh/ipg!execute", "uid=ipg008", "post", deleteVideoSuccess, '',"frmMain");
});


//关闭视频回调
function deleteVideoSuccess(result){
    if(result.returnCode == 0){
        // alert("视频流关闭成功");
    }else{
        // alert("视频流关闭失败");
    }
}

function newVideo(url){
    //缓冲视频
    $("#modalVideoSurveillance").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    //显示模态框
    $("#videoTip").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal');
    $(".loading-box").show();
    $("#iframeLoading").show();
    $(".progress .progress-bar").progressbar({
        display_text: 'center',
        done:function(){
            $(".loading-box").hide();
            $("#iframeLoading").hide();
        }
    });

    // if(!isInsalledIEVLC()&&!isInsalledFFVLC()){
    //     $(".downloadedBtn").show();
    //     $(".videoText").show();
    // }else{
    //     $(".downloadedBtn").css("display","none");
    //     $(".videoText").css("display","none");
    //
    //     //////判断IE
    //     var videoHtml  = "<object type='application/x-vlc-plugin' pluginspage='http://www.videolan.org/' id='vlc' events='true' width='100%' height='410px' style='margin: 0;padding: 0;display: block;'>"+
    //         "<param name='mrl' value="+url+">"+
    //         "<param name='volume' value='50'/>"+
    //         "<param name='autoplay' value='true'/>"+
    //         "<param name='loop' value='false' />"+
    //         "<param name='fullscreen' value='false'/>"+
    //         "<param name='controls' value='false'/>"+
    //         "</object>";
    //     $("#objVlc").html(videoHtml);
    // }

    var myPlayer = videojs('my_video');
    videojs('my_video').ready(function(){
        window.myPlayer = this;
        // myPlayer.src("rtmp://live.hkstv.hk.lxdns.com/live/hks");
        myPlayer.src(url);
        myPlayer.play();
    });

}


//判断是否有插件
function isInsalledIEVLC(){
    var vlcObj = null;
    var vlcInstalled= false;
    try {
        vlcObj = new ActiveXObject("VideoLAN.Vlcplugin.2");
        if( vlcObj != null ){
            vlcInstalled = true
        }
    } catch (e) {
        vlcInstalled= false;
    }
    return vlcInstalled;
}

function isInsalledFFVLC(){
    var numPlugins=navigator.plugins.length;
    for  (i=0;i<numPlugins;i++)
    {
        plugin=navigator.plugins[i];
        if(plugin.name.indexOf("VideoLAN") > -1 || plugin.name.indexOf("VLC") > -1)
        {
            return true;
        }
    }
    return false;
}
