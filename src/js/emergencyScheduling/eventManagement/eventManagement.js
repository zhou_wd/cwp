﻿var modalHtml="";
$(function() {
    warningDictionaryType();
    queryList(1);
    eventValidtor();
    modalHtml=$("#modalMaintenanceDetails").html();
});


//搜索安防预警
$(document).on("click","#btnSearchAlarm",function(){
    isFirst=true;
    queryList(1);
});

var eventProcessStatus=1;
$("#myTabs li").on("click", function () {
    isFirst=true;
    eventProcessStatus=$(this).index()+1;
    $(".bg-date").val("");
    queryList(1);

});



//请求维修管理列表
function queryList(pageIndex){
    var diName=$('#diName').val();
    var eventDescribe=$('#eventDescribe').val();
    var startApplyTime = $("#startApplyTime").val();
    var endApplyTime = $("#endApplyTime").val();
    if("undefined" == typeof startApplyTime){
        startApplyTime="";
    }
    if("undefined" == typeof endApplyTime){
        endApplyTime="";
    }
    if("undefined" == typeof eventDescribe){
        eventDescribe="";
    }
    if(diName==null){
        var diName="";
    }
    var loaderAlarmTab = new LoaderTableNew("warningEventList","/cwp/front/sh/warningEvent!execute","uid=c001&dictValue="+diName+"&eventProcessStatus="+eventProcessStatus+"&eventType=2&startApplyTime="+startApplyTime+"&endApplyTime="+endApplyTime+"&eventDescribe="+eventDescribe,pageIndex,10,WarningEventResultArrived,"",faultPagination);
    loaderAlarmTab.Display();
}



//获取维修管理数据并初始化html
function WarningEventResultArrived(result){
    template.helper('eventFromSystemFormat', function (inp) {
        if (inp == "1") {
            return '系统';
        } else  {
            return '手动';
        }
    });
    template.helper('eventProcessStatusFormat', function (inp) {
        if (inp == "1") {
            return "未处理";
        } else if (inp == "2") {
            return "处理中";
        } else if (inp == "3") {
            return "已处理";
        }
    });
    var outHtml = template("dataTmpl", result);
    $("#warningEventList tbody").html(outHtml);
}


//当前页，加载容器
//function paginationCallback(page_index){
//    if(!isPostBack) queryList(page_index+1);
//}

function faultPagination(page_index) {
    if (!isFirst) queryList(page_index + 1);
}
//-----------------列表结束-------------------------//

//搜索 设备分类
function warningDictionaryType(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_4&dictKey=intelligentType&dictdataKey='security','building_services'", "", WarningDictionarytResultArrived, '');
}

function WarningDictionarytResultArrived(result){
    var sel = $("#diName");
    var option = ""
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        if(result.beans[i].dictdataValue=="1004001") {
            option = $("<option>").text(result.beans[i].deviceName).val(result.beans[i].dictdataValue);
            sel.append(option);
        }
    }
}



//-----------------新增开始--------------------------//
//获取设备类别
function addWarningDictionaryType(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c002_4&dictKey=intelligentType&dictdataKey='security','building_services'", "", AddWarningDictionarytResultArrived, '');
}
//获取设备类别回调函数
function AddWarningDictionarytResultArrived(result){
    var sel = $("#addWaringType");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        if(result.beans[i].dictdataValue=="1004001") {
            option = $("<option>").text(result.beans[i].deviceName).val(result.beans[i].dictdataValue);
            sel.append(option);
        }
    }
}


//物业管理-新增
////查找所有楼栋 chenqiuxu
function buildingQuary(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c004_2", "", buildingResultArrived, '');
}
//获取所有楼栋并拼接html
function buildingResultArrived(result){
    var sel = $("#buildingNo");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
        sel.append(option);
    }
}


//物业管理-新增
////查找所有楼栋 chenqiuxu
function buildingQuary(){
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c004_2", "", buildingResultArrived, '');
}
//获取所有楼栋并拼接html
function buildingResultArrived(result){
    var sel = $("#buildingNo");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
        sel.append(option);
    }
}

//根据楼，显示层，显示房间，三级联动
function selectbuildChild(){
    var parentId=$("#buildingNo").val();
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c005&parentId="+parentId, "", bParentResultArrived, '');
}
//显示层
function bParentResultArrived(result){
    var sel = $("#buildingChild");
    var option = "";
    sel.empty();
    option = $("<option>").text("请选择").val("");
    sel.append(option);
    for ( var i = 0; i <   result.beans.length; i++) {
        option = $("<option>").text(result.beans[i].buildingDesc).val(result.beans[i].buildingId);
        sel.append(option);
    }
}
//显示设备
$(function(){
    //给selelct 绑定change事件
    $("#buildingChild").change(function(){
        var count = $(this).val();
        var deviceType = $("#addWaringType").val();
        var buildingId = $("#buildingChild").val();
        if(count != null && deviceType != null){
            PostForm("/cwp/front/sh/device!execute", "uid=d014&buildingId="+buildingId+"&deviceType="+deviceType, "", deviceResultArrived, '');
        }
    })
});


//显示所有设备
function deviceResultArrived(result){
    if(result.returnCode == 0){
        var sel = $("#devices");
        var option = "";
        sel.empty();
        option = $("<option>").text("请选择").val("");
        sel.append(option);
        for ( var i = 0; i <   result.beans.length; i++) {
            option = $("<option>").text(result.beans[i].devicePositionCode).val(result.beans[i].deviceId);
            sel.append(option);
        }
    }else{
        alert(result.returnMessage);
    }

}

//新增维修工单表单验证
function eventValidtor()
{
    $.formValidator.initConfig({
        formid: "frmMain", onerror: function (msg) {
            return false;
        }, onsuccess: function () {
            return true;
        }
    });
    $("#addDetailDesc").formValidator({
        onfocus: "详细描述",
        oncorrect: " "
    }).InputValidator({
        max: 30,
        onempty: "请输入详细描述",
        onerror: "账号长度不得超过30个字符"
    }).RegexValidator({
        regexp: "notempty",
        datatype: "enum",
        onerror: "不能为空"
    });

}
//添加告警事件
function saveWarningEvent() {
    //判断表单验证是否通过
    if ($.formValidator.PageIsValid()) {
        var applierId = localStorage.getItem("parkId");
        //var addWarinngType = $('#addWaringType').val();
        //var addWarningLevel = $('#addLevel').val();
        //if (addWarningLevel.length == 0) {
        //    addWarningLevel = 0;
        //}
        //var addEventPostion = $('#buildingChild').val() + $('#addDetailPostion').val();
        //var addDetailDesc = $('#addDetailDesc').val();
        //PostForm("/cwp/front/sh/warningEvent!execute", "uid=c006&warningType=" + addWarinngType + "&warningLevel=" + addWarningLevel + "&eventDetail=" + addEventPostion + "&eventDescribe=" + addDetailDesc + "&applierId=" + applierId+"&eventProgress=1&eventType=2", "", saveWarningEventResultArrived, '');
        PostForm("/cwp/front/sh/warningEvent!execute", "uid=c006&applierId=" + applierId+"&eventProgress=1&eventType=2", "", saveWarningEventResultArrived, '');
        arr = new Array();
        //关闭模态框
        $("#modalMaintenanceNew").modal("hide");
    }
}


function saveWarningEventResultArrived(result){
    switch (result.returnCode) {
        case "0":
            tipModal("icon-checkmark", "添加成功");
            isPostBack=true;
            queryList(1);
            break;
        case "2":
            tipModal("icon-cross", "添加失败");
            break;
        case "-9999":
            tipModal("icon-cross", "添加失败");
            break;
    }
}
////绑定点击事件
//新建
$("#btnMaintenanceNew").on("click", function () {
    addWarningDictionaryType();
    buildingQuary();
    $("#modalMaintenanceNew").modal({
        keyboard: true,
        backdrop: 'static'
    }).on('show.bs.modal', centerModal());
    $(window).on('resize', centerModal());
});
//-------------新增结束---------------------//



//-------------详情开始--------------------//
function detailSelect(warningEventId){
    $("#modalMaintenanceDetails").html(modalHtml);
    PostForm("/cwp/front/sh/warningEvent!execute", "uid=c003_2&warningEventId="+warningEventId, "", detailWarningById, '');

}

function detailWarningById(result) {
    //初始化模态框html
    if (result.returnCode == 0){
        var state = "";
        if (result.bean.eventProcessStatus == "1") {
            state = "未处理"
        } else if (result.bean.eventProcessStatus == "2") {
            state = "处理中"
        } else if (result.bean.eventProcessStatus == "3") {
            state = "已处理"
        }
        $('#deviceType').html(result.bean.deviceName == "" ? "门禁设备" : result.bean.deviceName);
        $('#deviceState').html(state);
        $('#devicePostion').html(result.bean.buildingName == "" ? "无" : result.bean.buildingName);

        $('#warningDescribe').html(result.bean.eventDescribe == "" ? "无" : result.bean.eventDescribe);
        $('#handleOpinion').html(result.bean.handleOpinion == "" ? "未完成" : result.bean.handleOpinion);


        //告警编号
        $("#alarmNumber").html(result.bean.alarmNumber==""?"无":result.bean.alarmNumber);
        //告警级别
        if(result.bean.warningLevel==0){
            warningLevelHtml="<i class='t-tag-del'>低</i>"
        }

        switch (result.bean.warningLevel){
            case "0":
                warningLevelHtml="<i class='t-tag-del'>低</i>";
                break;
            case "1":
                warningLevelHtml="<i class='t-tag-yellowColor'>中</i>";
                break;
            case "2":
                warningLevelHtml="<i class='t-tag-ing'>高</i>";
                break;
            case "3":
                warningLevelHtml="<i class='t-tag-red'>紧急</i>";
                break;
        }

        $("#messageLevel").html(warningLevelHtml);

        //当前责任人及角色
        var noticeNameArray=result.bean.appPush.split(',');
        if(result.bean.handName!=""){
            $(".responsibe-name").html(result.bean.handName);
            $(".responsibe-role").html(result.bean.response);
            $(".responsibe-phone span").html(result.bean.responsePhone);
        }
        else{
            $(".responsibe-name").html("暂无人接单");
        }



        //显示告警流程信息
        if (result.bean.applyTime != "") {
            var noticeNameArray=result.bean.appPush.split(',');
            var noticeName=noticeNameArray[1].split(':')[1];
            $('#noticeTime').html(result.bean.applyTime);
            $('#noticeName span').html(noticeName);
        }

        if (result.bean.responseTime != "") {
            $('#responseTime').html(result.bean.responseTime);
            $('#responseName').html(result.bean.response+"：" + result.bean.responseName);
        }

        if (result.bean.handleTime != "") {
            $('#handlerTime').html(result.bean.handleTime);
            $('#handlerName').html(result.bean.response+"：" + result.bean.handName);
        }

        if (result.bean.eventProgress == "2") {
            $('.bubble').eq(1).removeClass("comp");
            $('.com_squ').eq(1).removeClass("conp");
        } else if (result.bean.eventProgress == "3") {
            $('.bubble').eq(1).removeClass("comp");
            $('.bubble').eq(2).removeClass("comp");
            $('.bubble').eq(3).removeClass("comp");
            $('.com_squ').eq(1).removeClass("conp");
            $('.com_squ').eq(2).removeClass("conp");
            $('.com_squ').eq(3).removeClass("conp");
            $('.line .end').removeClass("conp");
            $('#completeTime').html(result.bean.handleTime);
            $('#completeResultTip').html("已完成");
            $('#completeResult').html("")

        } else if (result.bean.eventProgress == "4") {
            $('.bubble').eq(1).removeClass("comp");
            $('.bubble').eq(2).removeClass("comp");
            $('.bubble').eq(3).removeClass("comp");
            $('.com_squ').eq(1).removeClass("conp");
            $('.com_squ').eq(2).removeClass("conp");
            $('.com_squ').eq(3).removeClass("conp");
            $('.line .end').removeClass("conp");
            $('#completeTime').html(result.bean.handleTime);
            $('#completeResultTip').html("已完成");
            $('#completeResult').html("转故障")

        }

        $("#modalMaintenanceDetails").modal({
            keyboard: true,
            backdrop: 'static'
        }).on('show.bs.modal', centerModal());
        $(window).on('resize', centerModal());
    }
}

//--------------详情结束---------------------------//
